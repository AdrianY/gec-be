package com.greatuslabs.gec.be.modules.ingredients.web.graphql;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.greatuslabs.gec.be.modules.core.domain.dto.DishesIngredientInfo;
import com.greatuslabs.gec.be.modules.ingredients.domain.service.IngredientsQueryService;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class IngredientsQuery implements GraphQLQueryResolver {

    private final IngredientsQueryService ingredientsQueryService;

    public IngredientsQuery(IngredientsQueryService ingredientsQueryService) {
        this.ingredientsQueryService = ingredientsQueryService;
    }

    public List<DishesIngredientInfo> getAllDishesUsingIngredient(Long ingredientId){
        return ingredientsQueryService.getAllUsingDishesInfo(ingredientId);
    }
}
