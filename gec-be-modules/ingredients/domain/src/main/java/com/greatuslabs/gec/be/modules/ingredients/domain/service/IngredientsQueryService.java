package com.greatuslabs.gec.be.modules.ingredients.domain.service;

import com.greatuslabs.gec.be.modules.core.domain.dto.DishesIngredientInfo;

import java.util.List;

public interface IngredientsQueryService {

    List<DishesIngredientInfo> getAllUsingDishesInfo(Long ingredientId);
}
