package com.greatuslabs.gec.be.modules.ingredients.domain.service.impl;

import com.greatuslabs.be.commons.persistence.core.api.annotations.TransactionalService;
import com.greatuslabs.gec.be.modules.core.domain.dao.jpa.DishesIngredientJpaDao;
import com.greatuslabs.gec.be.modules.core.domain.dto.DishesIngredientInfo;
import com.greatuslabs.gec.be.modules.core.domain.entity.DishesIngredient;
import com.greatuslabs.gec.be.modules.core.domain.entity.Ingredient;
import com.greatuslabs.gec.be.modules.measurements.domain.service.CalculationService;
import com.greatuslabs.gec.be.modules.ingredients.domain.dao.jpa.IngredientsJpaDao;
import com.greatuslabs.gec.be.modules.ingredients.domain.service.IngredientsQueryService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@TransactionalService("ingredientsQueryService")
public class IngredientsQueryServiceImpl implements IngredientsQueryService{

    @Autowired
    private IngredientsJpaDao ingredientsJpaDao;

    @Autowired
    private DishesIngredientJpaDao dishesIngredientJpaDao;

    @Autowired
    private CalculationService calculationService;

    @Override
    public List<DishesIngredientInfo> getAllUsingDishesInfo(Long ingredientId) {

        Ingredient ingredient = ingredientsJpaDao.findOne(ingredientId);

        Set<DishesIngredient> dishes = dishesIngredientJpaDao.findAllByIngredient(ingredient);

        return dishes.stream()
                .map(d -> new DishesIngredientInfo(
                        d.getDish().getName(),
                        ingredient.getName(),
                        d.getUom().getName(),
                        d.getQuantity(),
                        calculationService.calculateIngredientTotalCostPerDish(d, ingredient)))
                .sorted(Comparator.comparing(DishesIngredientInfo::getDishName))
                .collect(Collectors.toList());
    }

}
