package com.greatuslabs.gec.be.modules.ingredients.domain.dao.jpa;

import com.greatuslabs.be.commons.persistence.jpa.api.dao.BaseJpaDao;
import com.greatuslabs.gec.be.modules.core.domain.entity.Ingredient;

public interface IngredientsJpaDao extends BaseJpaDao<Ingredient> {
}
