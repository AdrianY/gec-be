package com.greatuslabs.gec.be.modules.ingredients.infra;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.greatuslabs.gec.be.modules.ingredients.domain")
public class IngredientsModuleScanConfig {
}
