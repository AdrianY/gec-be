package com.greatuslabs.gec.be.modules.ingredients.infra;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(
        basePackages = {"com.greatuslabs.gec.be.modules.ingredients.domain.dao.jpa"}
)
public class IngredientsModuleJpaConfig {

}
