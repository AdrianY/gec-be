package com.greatuslabs.gec.be.modules.ingredients.dao;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.greatuslabs.gec.be.commons.test.annotations.GecTestExecutionListeners;
import com.greatuslabs.gec.be.commons.test.util.GecDBUnitXmlDataSetLoader;
import com.greatuslabs.gec.be.modules.core.domain.entity.Ingredient;
import com.greatuslabs.gec.be.modules.core.infra.CoreDomainModuleJpaConfig;
import com.greatuslabs.gec.be.modules.ingredients.domain.dao.jpa.IngredientsJpaDao;
import com.greatuslabs.gec.be.modules.ingredients.infra.IngredientsModuleJpaConfig;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(
        classes = {IngredientsModuleJpaConfig.class,CoreDomainModuleJpaConfig.class},
        webEnvironment = SpringBootTest.WebEnvironment.NONE
)
@EnableAutoConfiguration
@GecTestExecutionListeners
@DbUnitConfiguration(dataSetLoader = GecDBUnitXmlDataSetLoader.class)
public class IngredientsJpaDaoTest {

    @Autowired
    private IngredientsJpaDao ingredientsJpaDao;

    @Test
    @DatabaseSetup("/mappingTest.xml")
    public void testFindAll() {

        List<Ingredient> ingredients = ingredientsJpaDao.findAll();
        Assert.assertNotNull(ingredients);
        Assert.assertFalse(ingredients.isEmpty());

    }

}
