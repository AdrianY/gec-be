package com.greatuslabs.gec.be.modules.ingredients.service;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.greatuslabs.gec.be.commons.test.annotations.GecTestExecutionListeners;
import com.greatuslabs.gec.be.commons.test.util.GecDBUnitXmlDataSetLoader;
import com.greatuslabs.gec.be.modules.core.domain.dto.DishesIngredientInfo;
import com.greatuslabs.gec.be.modules.core.infra.CoreDomainModuleJpaConfig;
import com.greatuslabs.gec.be.modules.ingredients.domain.service.IngredientsQueryService;
import com.greatuslabs.gec.be.modules.ingredients.infra.IngredientsModuleJpaConfig;
import com.greatuslabs.gec.be.modules.ingredients.infra.IngredientsModuleScanConfig;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@GecTestExecutionListeners
@SpringBootTest(
        classes = {
                IngredientsModuleScanConfig.class,
                IngredientsModuleJpaConfig.class,
                CoreDomainModuleJpaConfig.class
        },
        webEnvironment = SpringBootTest.WebEnvironment.NONE
)
@EnableAutoConfiguration
@DbUnitConfiguration(dataSetLoader = GecDBUnitXmlDataSetLoader.class)
public class IngredientsQueryServiceTest {

    @Autowired
    private IngredientsQueryService ingredientsQueryService;

    @Test
    @DatabaseSetup("/mappingTest.xml")
    public void testFindIngredientsInfo () {

        List<DishesIngredientInfo> ingredientsInfo = ingredientsQueryService.getAllUsingDishesInfo(1L);

        Assert.assertNotNull(ingredientsInfo);
        Assert.assertEquals(2, ingredientsInfo.size());

        DishesIngredientInfo ingredientInfo = ingredientsInfo.get(0);
        Assert.assertEquals("Adobo", ingredientInfo.getDishName());
        Assert.assertEquals(new BigDecimal("120").setScale(4, RoundingMode.HALF_UP),ingredientInfo.getCost());

        ingredientInfo = ingredientsInfo.get(1);
        Assert.assertEquals("Sisig", ingredientInfo.getDishName());
        Assert.assertEquals(new BigDecimal("60").setScale(4, RoundingMode.HALF_UP),ingredientInfo.getCost());
    }


}
