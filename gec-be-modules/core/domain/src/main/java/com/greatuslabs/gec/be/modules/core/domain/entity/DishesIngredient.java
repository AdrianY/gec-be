package com.greatuslabs.gec.be.modules.core.domain.entity;

import com.greatuslabs.be.commons.persistence.jpa.api.entities.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "gec_dishes_ingredients")
public class DishesIngredient extends BaseEntity<DishesIngredientId> implements Serializable {

    @EmbeddedId
    private DishesIngredientId dishesIngredientId;

    @ManyToOne
    @MapsId("dishId")
    private Dish dish;

    @ManyToOne
    @MapsId("ingredientId")
    private Ingredient ingredient;

    @OneToOne
    @JoinColumn(name = "uom_id")
    private Measurement uom;

    @Column(name = "quantity")
    private BigDecimal quantity;

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    public Measurement getUom() {
        return uom;
    }

    public void setUom(Measurement uom) {
        this.uom = uom;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public DishesIngredientId getDishesIngredientId() {
        return dishesIngredientId;
    }

    public void setDishesIngredientId(DishesIngredientId dishesIngredientId) {
        this.dishesIngredientId = dishesIngredientId;
    }

    @Override
    public DishesIngredientId getId() {
        return dishesIngredientId;
    }
}
