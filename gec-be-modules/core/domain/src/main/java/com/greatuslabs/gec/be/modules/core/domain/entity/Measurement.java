package com.greatuslabs.gec.be.modules.core.domain.entity;

import com.greatuslabs.be.commons.persistence.jpa.api.entities.BaseSingularEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adrian on 6/28/2017.
 */
@Entity
@Table(name = "gec_measurements")
public class Measurement extends BaseSingularEntity implements Serializable {

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "type", nullable = false)
    private String type;

    @Column(name = "quantity")
    private BigDecimal quantity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "base_uom")
    private Measurement baseUom;

    @OneToMany(mappedBy="baseUom")
    private List<Measurement> dependentMeasurements = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setBaseUom(Measurement baseUom) {
        this.baseUom = baseUom;
    }

    public void setDependentMeasurements(List<Measurement> dependentMeasurements) {
        this.dependentMeasurements = dependentMeasurements;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

	public Measurement getBaseUom() {
		return baseUom;
	}
	
	public boolean isOfficialMeasurement() {
		return baseUom == null;
	}    
    
}
