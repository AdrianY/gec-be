package com.greatuslabs.gec.be.modules.core.domain.entity;

import com.greatuslabs.be.commons.persistence.jpa.api.entities.BaseSingularEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Collections;
import java.util.Set;

@Entity
@Table(name = "gec_dishes")
public class Dish extends BaseSingularEntity implements Serializable {

    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(mappedBy = "dish")
    private Set<DishesIngredient> ingredients = Collections.emptySet();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<DishesIngredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(Set<DishesIngredient> ingredients) {
        this.ingredients = ingredients;
    }
}
