package com.greatuslabs.gec.be.modules.core.domain.dao.jpa;

import com.greatuslabs.gec.be.modules.core.domain.entity.Dish;
import com.greatuslabs.gec.be.modules.core.domain.entity.DishesIngredient;
import com.greatuslabs.gec.be.modules.core.domain.entity.DishesIngredientId;
import com.greatuslabs.gec.be.modules.core.domain.entity.Ingredient;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface DishesIngredientJpaDao extends JpaRepository<DishesIngredient, DishesIngredientId> {

    Set<DishesIngredient> findAllByIngredient(Ingredient ingredient);

    Set<DishesIngredient> findAllByDish(Dish dish);
}
