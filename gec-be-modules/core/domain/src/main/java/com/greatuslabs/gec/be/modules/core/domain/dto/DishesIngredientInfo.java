package com.greatuslabs.gec.be.modules.core.domain.dto;

import java.math.BigDecimal;

public class DishesIngredientInfo {

    private String dishName;

    private String ingredientName;

    private String uom;

    private BigDecimal quantity;

    private BigDecimal cost;

    public DishesIngredientInfo(String dishName, String ingredientName, String uom, BigDecimal quantity, BigDecimal cost) {
        this.dishName = dishName;
        this.ingredientName = ingredientName;
        this.uom = uom;
        this.quantity = quantity;
        this.cost = cost;
    }

    public String getIngredientName() {
        return ingredientName;
    }

    public void setIngredientName(String ingredientName) {
        this.ingredientName = ingredientName;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public String getDishName() {
        return dishName;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }
}
