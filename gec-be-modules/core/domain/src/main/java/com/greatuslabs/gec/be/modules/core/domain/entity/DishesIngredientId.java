package com.greatuslabs.gec.be.modules.core.domain.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class DishesIngredientId implements Serializable {

    @Column(name="ingredient_id")
    private Long ingredientId;

    @Column(name = "dish_id")
    private Long dishId;

    public DishesIngredientId() { }

    public DishesIngredientId(Long ingredientId, Long dishId) {
        this.ingredientId = ingredientId;
        this.dishId = dishId;
    }

    public Long getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(Long ingredientId) {
        this.ingredientId = ingredientId;
    }

    public Long getDishId() {
        return dishId;
    }

    public void setDishId(Long dishId) {
        this.dishId = dishId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DishesIngredientId)) return false;

        DishesIngredientId that = (DishesIngredientId) o;

        return Objects.equals(ingredientId, that.getIngredientId()) &&
                Objects.equals(dishId, that.getDishId());
    }

    @Override
    public int hashCode() {
        int result = ingredientId.hashCode();
        result = 31 * result + dishId.hashCode();
        return result;
    }
}
