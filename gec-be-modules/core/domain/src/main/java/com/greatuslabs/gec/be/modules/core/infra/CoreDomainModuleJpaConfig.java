package com.greatuslabs.gec.be.modules.core.infra;

import com.greatuslabs.be.commons.persistence.jpa.api.infra.EntityPackagesRegistry;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(
        basePackages = {"com.greatuslabs.gec.be.modules.core.domain.dao.jpa"}
)
public class CoreDomainModuleJpaConfig  implements EntityPackagesRegistry {
    @Override
    public String[] getEntityPackagesToScan() {
        return new String[]{
                "com.greatuslabs.gec.be.modules.core.domain.entity"
        };
    }
}
