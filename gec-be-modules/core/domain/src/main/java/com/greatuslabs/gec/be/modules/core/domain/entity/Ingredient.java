package com.greatuslabs.gec.be.modules.core.domain.entity;

import com.greatuslabs.be.commons.persistence.jpa.api.entities.BaseSingularEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Set;

@Entity
@Table(name = "gec_ingredients")
public class Ingredient extends BaseSingularEntity implements Serializable{

    @Column(name = "name", nullable = false)
    private String name;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "uom_id", nullable = false)
    private Measurement uom;

    @Column(name = "cost_per_uom", nullable = false)
    private BigDecimal costPerUom;

    @OneToMany(mappedBy = "ingredient")
    private Set<DishesIngredient> dishes = Collections.emptySet();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Measurement getUom() {
        return uom;
    }

    public void setUom(Measurement uom) {
        this.uom = uom;
    }

    public BigDecimal getCostPerUom() {
        return costPerUom;
    }

    public void setCostPerUom(BigDecimal costPerUom) {
        this.costPerUom = costPerUom;
    }

    public Set<DishesIngredient> getDishes() {
        return dishes;
    }

    public void setDishes(Set<DishesIngredient> dishes) {
        this.dishes = dishes;
    }
}
