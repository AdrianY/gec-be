package com.greatuslabs.gec.be.modules.measurements.web.graphql;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.greatuslabs.gec.be.modules.measurements.domain.dto.MeasurementsInfo;
import com.greatuslabs.gec.be.modules.measurements.domain.service.MeasurementsQueryService;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MeasurementQuery implements GraphQLQueryResolver {

    private final MeasurementsQueryService measurementsQueryService;

    public MeasurementQuery(MeasurementsQueryService measurementsQueryService) {
        this.measurementsQueryService = measurementsQueryService;
    }

    public List<MeasurementsInfo> getAllRelatedMeasurements(Long id){
        return measurementsQueryService.getAllRelatedMeasurements(id);
    }

}
