package com.greatuslabs.gec.be.modules.measurements.web.rest.endpoints;

import com.greatuslabs.gec.be.modules.measurements.domain.dto.MeasurementsInfo;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

/**
 * Created by Adrian on 6/21/2017.
 */
@RestController
public class MeasurementsRestController {

    @RequestMapping(method= RequestMethod.GET, path = "/measurements/{id}")
    public ResponseEntity<MeasurementsInfo> getMeasurementDetails(@PathVariable("id") Long id) {
        MeasurementsInfo measurementsInfo =
                new MeasurementsInfo(id, "Cup", "Volume", BigDecimal.ONE);
        return new ResponseEntity<>(measurementsInfo, HttpStatus.OK);
    }
}
