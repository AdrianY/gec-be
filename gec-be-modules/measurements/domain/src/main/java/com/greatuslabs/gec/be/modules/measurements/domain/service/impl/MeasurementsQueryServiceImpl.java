package com.greatuslabs.gec.be.modules.measurements.domain.service.impl;

import com.greatuslabs.be.commons.persistence.core.api.annotations.TransactionalService;
import com.greatuslabs.gec.be.modules.core.domain.entity.Measurement;
import com.greatuslabs.gec.be.modules.measurements.domain.dao.jpa.MeasurementsJpaDao;
import com.greatuslabs.gec.be.modules.measurements.domain.properties.ConversionTable;
import com.greatuslabs.gec.be.modules.measurements.domain.dto.MeasurementsInfo;
import com.greatuslabs.gec.be.modules.measurements.domain.service.MeasurementsQueryService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@TransactionalService("measurementsQueryService")
public class MeasurementsQueryServiceImpl implements MeasurementsQueryService {


	private final ConversionTable conversionTable;

	private final MeasurementsJpaDao measurementsJpaDao;

	public MeasurementsQueryServiceImpl(ConversionTable conversionTable, MeasurementsJpaDao measurementsJpaDao) {
		this.conversionTable = conversionTable;
		this.measurementsJpaDao = measurementsJpaDao;
	}

	@Override
	public List<MeasurementsInfo> getAllRelatedMeasurements(long id) {

		//get the selected measurement
		Measurement selectedMeasurement = measurementsJpaDao.findOne(id);

		if(Objects.isNull(selectedMeasurement))
			return null;
		
		//list all measurement with the same volume type
		List<Measurement> measurements = measurementsJpaDao.findByType(selectedMeasurement.getType());

		//Filter out selected measurement
		return measurements.stream()
				.filter(m -> !m.getName().equals(selectedMeasurement.getName()))
				.map(m -> getMeasurementConversion(selectedMeasurement, m))
				.collect(Collectors.toList());
	}

	@Override
	public MeasurementsInfo getMeasurementConversion(Measurement source, Measurement target) {
		//convert the baseQty to standard unit (ml or gram)
		BigDecimal baseQty = source.isOfficialMeasurement()
				? getConvertedValue(source)
				: calculateOfficialMeasurementValue(source, calculateMultiplier(source));

		BigDecimal officialMeasurementValue = calculateOfficialMeasurementValue(target, calculateMultiplier(target));
		return new MeasurementsInfo(target.getName(), baseQty.divide(officialMeasurementValue, 4, RoundingMode.HALF_UP));
	}

	/*
	 * Calculate the total value of it's official measurement
	 * eg. when cup = 250ml, bottle = 2cups, pitcher = 2 bottle
	 * expected return for pitcher = (2 * 2 * 250) = 1000ml
	 */
	private BigDecimal calculateMultiplier(Measurement measurement) {
		if (!measurement.isOfficialMeasurement()) {
			return  measurement.getQuantity().multiply(
					calculateMultiplier(measurement.getBaseUom())).setScale(4, RoundingMode.HALF_UP);
		}
		return BigDecimal.ONE.setScale(4, RoundingMode.HALF_UP);
	}
	
	//Convert first every custom measurement to an official measurement to be able to convert to each other.	 
	private BigDecimal calculateOfficialMeasurementValue(Measurement measurement, BigDecimal multiplier) {		
		measurement = getOfficialBaseMeasurement(measurement);
		return getConvertedValue(measurement)
				.multiply(multiplier)
				.setScale(4, RoundingMode.HALF_UP);
	}

	//traverse until it gets the top/official base measurement, this is assuming that all official measurements have no more base measurement other than itself 	
	private Measurement getOfficialBaseMeasurement(Measurement measurement) {
		if (!measurement.isOfficialMeasurement()) {
			return  getOfficialBaseMeasurement(measurement.getBaseUom());
		}
		return measurement;
	}
	
	private BigDecimal getConvertedValue(Measurement measurement) {
		if(measurement != null) {
			 return conversionTable.getTable()
					 .get(measurement.getType())
					 .get(measurement.getName()).setScale(4, RoundingMode.HALF_UP);	
		}
		return null;		
	}
	
}
