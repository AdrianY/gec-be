package com.greatuslabs.gec.be.modules.measurements.infra;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by Adrian on 6/27/2017.
 */
@Configuration
@EnableJpaRepositories(
        basePackages = "com.greatuslabs.gec.be.modules.measurements.domain.dao.jpa"
)
public class MeasurementsModuleJpaConfig {

}
