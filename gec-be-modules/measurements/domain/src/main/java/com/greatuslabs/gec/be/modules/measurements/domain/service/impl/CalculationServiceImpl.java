package com.greatuslabs.gec.be.modules.measurements.domain.service.impl;

import com.greatuslabs.be.commons.persistence.core.api.annotations.TransactionalService;
import com.greatuslabs.gec.be.modules.core.domain.dto.DishesIngredientInfo;
import com.greatuslabs.gec.be.modules.core.domain.entity.DishesIngredient;
import com.greatuslabs.gec.be.modules.core.domain.entity.Ingredient;
import com.greatuslabs.gec.be.modules.measurements.domain.dto.MeasurementsInfo;
import com.greatuslabs.gec.be.modules.measurements.domain.service.CalculationService;
import com.greatuslabs.gec.be.modules.measurements.domain.service.MeasurementsQueryService;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@TransactionalService("calculationService")
public class CalculationServiceImpl implements CalculationService {

    @Autowired
    private MeasurementsQueryService measurementsQueryService;

    public BigDecimal calculateIngredientTotalCostPerDish(DishesIngredient dishesIngredient, Ingredient ingredient) {
        MeasurementsInfo measurementsInfo = measurementsQueryService.getMeasurementConversion(dishesIngredient.getUom(), ingredient.getUom());
        return dishesIngredient.getQuantity().multiply(
                ingredient.getCostPerUom().multiply(measurementsInfo.getQuantity())).setScale(4, RoundingMode.HALF_UP);
    }

    public BigDecimal calculateDishTotalCost(List<DishesIngredientInfo> dishesIngredientInfo) {
        return dishesIngredientInfo.stream()
                .map(DishesIngredientInfo::getCost)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
