package com.greatuslabs.gec.be.modules.measurements.domain.dto;

import java.math.BigDecimal;

/**
 * Created by Adrian on 6/21/2017.
 */
public class MeasurementsInfo {
    private Long id;
    private String name;
    private String type;
    private BigDecimal quantity;
    
    public MeasurementsInfo(String name, BigDecimal quantity) {
    	this.name = name;
    	this.quantity = quantity;
    }

    public MeasurementsInfo(Long id, String name, String type, BigDecimal quantity) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

	public BigDecimal getQuantity() {
		return quantity;
	}
        
}
