package com.greatuslabs.gec.be.modules.measurements.infra;

import com.greatuslabs.gec.be.modules.measurements.domain.properties.ConversionTable;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

/**
 * Created by Adrian on 6/27/2017.
 */
@Configuration
@ComponentScan("com.greatuslabs.gec.be.modules.measurements.domain")
@PropertySource(value = "measurementConversion.properties")
public class MeasurementsModuleScanConfig {
    @Bean
    @ConfigurationProperties(prefix = "measurement")
    public ConversionTable conversionTable() {
        return new ConversionTable();
    }

}
