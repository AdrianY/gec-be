package com.greatuslabs.gec.be.modules.measurements.domain.dao.jpa;

import java.util.List;

import com.greatuslabs.be.commons.persistence.jpa.api.dao.BaseJpaDao;
import com.greatuslabs.gec.be.modules.core.domain.entity.Measurement;
import org.springframework.data.jpa.repository.JpaRepository;



/**
 * Created by Adrian on 6/28/2017.
 */
public interface MeasurementsJpaDao extends BaseJpaDao<Measurement> {
    List<Measurement> findAllByBaseUom(Measurement baseUom);

    Measurement findByDependentMeasurementsIn(Measurement measurement);
    
    List<Measurement> findByType(String type);
}
