package com.greatuslabs.gec.be.modules.measurements.domain.service.impl;

import com.greatuslabs.be.commons.persistence.core.api.annotations.TransactionalService;
import com.greatuslabs.gec.be.modules.measurements.domain.dto.MeasurementsCO;
import com.greatuslabs.gec.be.modules.measurements.domain.dto.MeasurementsInfo;
import com.greatuslabs.gec.be.modules.measurements.domain.service.MaintenanceService;


@TransactionalService("measurementsMaintenanceService")
public class MeasurementsMaintenanceServiceImpl
        implements MaintenanceService<Long, MeasurementsCO, MeasurementsInfo> {

    @Override
    public MeasurementsInfo create(MeasurementsCO measurementsCO) {
        return null;
    }

    @Override
    public MeasurementsInfo update(Long aLong, MeasurementsCO measurementsCO) {
        return null;
    }
}
