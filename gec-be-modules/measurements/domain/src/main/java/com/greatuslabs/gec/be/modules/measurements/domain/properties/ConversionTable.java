package com.greatuslabs.gec.be.modules.measurements.domain.properties;


import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class ConversionTable {

    private Map<String, Map<String, BigDecimal>> table = new HashMap<>();

    public Map<String, Map<String, BigDecimal>> getTable() {
        return table;
    }
}
