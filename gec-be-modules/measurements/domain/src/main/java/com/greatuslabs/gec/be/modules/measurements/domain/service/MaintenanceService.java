package com.greatuslabs.gec.be.modules.measurements.domain.service;

import java.io.Serializable;

public interface MaintenanceService<ID extends Serializable, COMMAND_OBJECT, QUERY_OBJECT> {
    QUERY_OBJECT create(COMMAND_OBJECT commandObject);

    QUERY_OBJECT update(ID id, COMMAND_OBJECT commandObject);
}
