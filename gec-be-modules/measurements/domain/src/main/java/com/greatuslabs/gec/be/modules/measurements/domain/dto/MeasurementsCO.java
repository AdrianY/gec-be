package com.greatuslabs.gec.be.modules.measurements.domain.dto;

import javax.validation.constraints.NotNull;

public class MeasurementsCO {

    @NotNull
    private String name;
    private String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
