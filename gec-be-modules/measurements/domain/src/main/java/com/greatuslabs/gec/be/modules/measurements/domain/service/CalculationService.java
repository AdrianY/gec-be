package com.greatuslabs.gec.be.modules.measurements.domain.service;

import com.greatuslabs.gec.be.modules.core.domain.dto.DishesIngredientInfo;
import com.greatuslabs.gec.be.modules.core.domain.entity.DishesIngredient;
import com.greatuslabs.gec.be.modules.core.domain.entity.Ingredient;

import java.math.BigDecimal;
import java.util.List;

public interface CalculationService {

    BigDecimal calculateIngredientTotalCostPerDish(DishesIngredient dishesIngredient, Ingredient ingredient);

    BigDecimal calculateDishTotalCost(List<DishesIngredientInfo> dishesIngredientInfo);

}
