package com.greatuslabs.gec.be.modules.measurements.domain.service;

import java.util.List;

import com.greatuslabs.gec.be.modules.core.domain.entity.Measurement;
import com.greatuslabs.gec.be.modules.measurements.domain.dto.MeasurementsInfo;

public interface MeasurementsQueryService {
	
	List<MeasurementsInfo> getAllRelatedMeasurements(long id);

	/**
	 * Converts measurement from one to another
	 */
	MeasurementsInfo getMeasurementConversion(Measurement source, Measurement target);
}
