package com.greatuslabs.gec.be.modules.measurements.service;

import com.greatuslabs.gec.be.modules.measurements.domain.dto.MeasurementsCO;
import com.greatuslabs.gec.be.modules.measurements.domain.dto.MeasurementsInfo;
import com.greatuslabs.gec.be.modules.measurements.domain.service.MaintenanceService;
import com.greatuslabs.gec.be.modules.measurements.infra.MeasurementsModuleJpaConfig;
import com.greatuslabs.gec.be.modules.measurements.infra.MeasurementsModuleScanConfig;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(
        classes = {
                MeasurementsModuleJpaConfig.class,
                MeasurementsModuleScanConfig.class
        },
        webEnvironment = SpringBootTest.WebEnvironment.NONE
)
@EnableAutoConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Ignore
public class MeasurementsMaintenanceServiceTest {

    @Autowired
    private MaintenanceService<Long,MeasurementsCO, MeasurementsInfo> measurementsMaintenanceService;

    @Rule
    public ExpectedException exceptions = ExpectedException.none();

}
