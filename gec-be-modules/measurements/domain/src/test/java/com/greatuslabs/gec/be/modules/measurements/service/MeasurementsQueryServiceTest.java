package com.greatuslabs.gec.be.modules.measurements.service;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.greatuslabs.gec.be.commons.test.annotations.GecTestExecutionListeners;
import com.greatuslabs.gec.be.commons.test.util.GecDBUnitXmlDataSetLoader;
import com.greatuslabs.gec.be.modules.core.infra.CoreDomainModuleJpaConfig;
import com.greatuslabs.gec.be.modules.measurements.domain.dto.MeasurementsInfo;
import com.greatuslabs.gec.be.modules.measurements.domain.service.MeasurementsQueryService;
import com.greatuslabs.gec.be.modules.measurements.infra.MeasurementsModuleJpaConfig;
import com.greatuslabs.gec.be.modules.measurements.infra.MeasurementsModuleScanConfig;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@GecTestExecutionListeners
@SpringBootTest(
        classes = {
                MeasurementsModuleJpaConfig.class,
                CoreDomainModuleJpaConfig.class,
                MeasurementsModuleScanConfig.class
        },
        webEnvironment = SpringBootTest.WebEnvironment.NONE
)
@EnableAutoConfiguration
@DbUnitConfiguration(dataSetLoader = GecDBUnitXmlDataSetLoader.class)
public class MeasurementsQueryServiceTest {

    @Autowired
    private MeasurementsQueryService measurementsQueryService;
    
    /*
     * Given Data: 
     * Cup - 250 ml 
     * Bottle - 2 Cups
     * Pitcher - 1 Liter
     */
    @Test
    @DatabaseSetup("/services/MeasurementsQueryServiceTest/conversionTest.xml")
    public void testVolumeConversionForCup(){     
    	
    	//Find the different conversion for Cup
    	List<MeasurementsInfo> measurementsInfo = measurementsQueryService.getAllRelatedMeasurements(3L);
    	Assert.assertNotNull(measurementsInfo);
    	Assert.assertFalse(measurementsInfo.isEmpty());
    	
    	MeasurementsInfo measurementInfo = measurementsInfo.get(0);
    	Assert.assertEquals("Millilitre", measurementInfo.getName());
    	Assert.assertEquals(new BigDecimal("250").setScale(4, RoundingMode.HALF_UP), measurementInfo.getQuantity());
    	
    	measurementInfo = measurementsInfo.get(1);    	
    	Assert.assertEquals("Litre", measurementInfo.getName());
    	Assert.assertEquals(new BigDecimal("0.25").setScale(4, RoundingMode.HALF_UP), measurementInfo.getQuantity());
    	
    	measurementInfo = measurementsInfo.get(2);    	
    	Assert.assertEquals("Bottle", measurementInfo.getName());
    	Assert.assertEquals(new BigDecimal("0.5").setScale(4, RoundingMode.HALF_UP), measurementInfo.getQuantity());
    	
    	measurementInfo = measurementsInfo.get(3);
    	Assert.assertEquals("Pitcher", measurementInfo.getName());
    	Assert.assertEquals(new BigDecimal("0.25").setScale(4, RoundingMode.HALF_UP), measurementInfo.getQuantity());    	    	    
    }
    
    @Test
    @DatabaseSetup("/services/MeasurementsQueryServiceTest/conversionTest.xml")
    public void testVolumeConversionForPitcher(){     
    	
    	//Find the different conversion for Cup
    	List<MeasurementsInfo> measurementsInfo = measurementsQueryService.getAllRelatedMeasurements(5L);
    	Assert.assertNotNull(measurementsInfo);
    	Assert.assertFalse(measurementsInfo.isEmpty());
    	
    	MeasurementsInfo measurementInfo = measurementsInfo.get(0);
    	Assert.assertEquals("Millilitre", measurementInfo.getName());
    	Assert.assertEquals(new BigDecimal("1000").setScale(4, RoundingMode.HALF_UP), measurementInfo.getQuantity());
    	
    	measurementInfo = measurementsInfo.get(1);    	
    	Assert.assertEquals("Litre", measurementInfo.getName());
    	Assert.assertEquals(new BigDecimal("1").setScale(4, RoundingMode.HALF_UP), measurementInfo.getQuantity());
    	
    	measurementInfo = measurementsInfo.get(2);    	
    	Assert.assertEquals("Cup", measurementInfo.getName());
    	Assert.assertEquals(new BigDecimal("4").setScale(4, RoundingMode.HALF_UP), measurementInfo.getQuantity());
    	
    	measurementInfo = measurementsInfo.get(3);
    	Assert.assertEquals("Bottle", measurementInfo.getName());
    	Assert.assertEquals(new BigDecimal("2").setScale(4, RoundingMode.HALF_UP), measurementInfo.getQuantity());    	    	    
    }
    
    @Test
    @DatabaseSetup("/services/MeasurementsQueryServiceTest/conversionTest.xml")
    public void testVolumeConversionForLitre(){     
    	
    	//Find the different conversion for Cup
    	List<MeasurementsInfo> measurementsInfo = measurementsQueryService.getAllRelatedMeasurements(2L);
    	Assert.assertNotNull(measurementsInfo);
    	Assert.assertFalse(measurementsInfo.isEmpty());
    	
    	MeasurementsInfo measurementInfo = measurementsInfo.get(0);
    	Assert.assertEquals("Millilitre", measurementInfo.getName());
    	Assert.assertEquals(new BigDecimal("1000").setScale(4, RoundingMode.HALF_UP), measurementInfo.getQuantity());    	    	
    	
    	measurementInfo = measurementsInfo.get(1);    	
    	Assert.assertEquals("Cup", measurementInfo.getName());
    	Assert.assertEquals(new BigDecimal("4").setScale(4, RoundingMode.HALF_UP), measurementInfo.getQuantity());
    	
    	measurementInfo = measurementsInfo.get(2);
    	Assert.assertEquals("Bottle", measurementInfo.getName());
    	Assert.assertEquals(new BigDecimal("2").setScale(4, RoundingMode.HALF_UP), measurementInfo.getQuantity());    	    	    
    	
    	measurementInfo = measurementsInfo.get(3);
    	Assert.assertEquals("Pitcher", measurementInfo.getName());
    	Assert.assertEquals(new BigDecimal("1").setScale(4, RoundingMode.HALF_UP), measurementInfo.getQuantity());
    }
    
    @Test
    @DatabaseSetup("/services/MeasurementsQueryServiceTest/conversionTest.xml")
    public void testVolumeConversionForMillilitre(){     
    	
    	//Find the different conversion for Cup
    	List<MeasurementsInfo> measurementsInfo = measurementsQueryService.getAllRelatedMeasurements(1L);
    	Assert.assertNotNull(measurementsInfo);
    	Assert.assertFalse(measurementsInfo.isEmpty());
    	
    	MeasurementsInfo measurementInfo = measurementsInfo.get(0);
    	Assert.assertEquals("Litre", measurementInfo.getName());
    	Assert.assertEquals(new BigDecimal("0.001").setScale(4, RoundingMode.HALF_UP), measurementInfo.getQuantity());    	    	
    	
    	measurementInfo = measurementsInfo.get(1);    	
    	Assert.assertEquals("Cup", measurementInfo.getName());
    	Assert.assertEquals(new BigDecimal("0.004").setScale(4, RoundingMode.HALF_UP), measurementInfo.getQuantity());
    	
    	measurementInfo = measurementsInfo.get(2);
    	Assert.assertEquals("Bottle", measurementInfo.getName());
    	Assert.assertEquals(new BigDecimal("0.002").setScale(4, RoundingMode.HALF_UP), measurementInfo.getQuantity());    	    	    
    	
    	measurementInfo = measurementsInfo.get(3);
    	Assert.assertEquals("Pitcher", measurementInfo.getName());
    	Assert.assertEquals(new BigDecimal("0.001").setScale(4, RoundingMode.HALF_UP), measurementInfo.getQuantity());
    }
    
    @Test
    @DatabaseSetup("/services/MeasurementsQueryServiceTest/conversionTest.xml")
    public void testWeightConversionForGram(){     
    	
    	//Find the different conversion for Cup
    	List<MeasurementsInfo> measurementsInfo = measurementsQueryService.getAllRelatedMeasurements(6L);
    	Assert.assertNotNull(measurementsInfo);
    	Assert.assertFalse(measurementsInfo.isEmpty());
    	
    	MeasurementsInfo measurementInfo = measurementsInfo.get(0);
    	Assert.assertEquals("Kilogram", measurementInfo.getName());
    	Assert.assertEquals(new BigDecimal("0.001").setScale(4, RoundingMode.HALF_UP), measurementInfo.getQuantity());    	    	
    	
    	measurementInfo = measurementsInfo.get(1);    	
    	Assert.assertEquals("Cup", measurementInfo.getName());
    	Assert.assertEquals(new BigDecimal("0.005").setScale(4, RoundingMode.HALF_UP), measurementInfo.getQuantity());
    	
    	measurementInfo = measurementsInfo.get(2);
    	Assert.assertEquals("Bag", measurementInfo.getName());
    	Assert.assertEquals(new BigDecimal("0.001").setScale(4, RoundingMode.HALF_UP), measurementInfo.getQuantity());    	    	        	    	
    }
    
    @Test
    @DatabaseSetup("/services/MeasurementsQueryServiceTest/conversionTest.xml")
    public void testWeightConversionForCup(){     
    	
    	//Find the different conversion for Cup
    	List<MeasurementsInfo> measurementsInfo = measurementsQueryService.getAllRelatedMeasurements(8L);
    	Assert.assertNotNull(measurementsInfo);
    	Assert.assertFalse(measurementsInfo.isEmpty());
    	
    	MeasurementsInfo measurementInfo = measurementsInfo.get(0);
    	Assert.assertEquals("Gram", measurementInfo.getName());
    	Assert.assertEquals(new BigDecimal("200").setScale(4, RoundingMode.HALF_UP), measurementInfo.getQuantity());    	    	
    	
    	measurementInfo = measurementsInfo.get(1);    	
    	Assert.assertEquals("Kilogram", measurementInfo.getName());
    	Assert.assertEquals(new BigDecimal("0.2").setScale(4, RoundingMode.HALF_UP), measurementInfo.getQuantity());
    	
    	measurementInfo = measurementsInfo.get(2);
    	Assert.assertEquals("Bag", measurementInfo.getName());
    	Assert.assertEquals(new BigDecimal("0.2").setScale(4, RoundingMode.HALF_UP), measurementInfo.getQuantity());    	    	        	    	
    }
    
    @Test
    @DatabaseSetup("/services/MeasurementsQueryServiceTest/conversionTest.xml")
    public void testWeightConversionForBag(){     
    	
    	//Find the different conversion for Cup
    	List<MeasurementsInfo> measurementsInfo = measurementsQueryService.getAllRelatedMeasurements(9L);
    	Assert.assertNotNull(measurementsInfo);
    	Assert.assertFalse(measurementsInfo.isEmpty());
    	
    	MeasurementsInfo measurementInfo = measurementsInfo.get(0);
    	Assert.assertEquals("Gram", measurementInfo.getName());
    	Assert.assertEquals(new BigDecimal("1000").setScale(4, RoundingMode.HALF_UP), measurementInfo.getQuantity());    	    	
    	
    	measurementInfo = measurementsInfo.get(1);    	
    	Assert.assertEquals("Kilogram", measurementInfo.getName());
    	Assert.assertEquals(new BigDecimal("1").setScale(4, RoundingMode.HALF_UP), measurementInfo.getQuantity());
    	
    	measurementInfo = measurementsInfo.get(2);
    	Assert.assertEquals("Cup", measurementInfo.getName());
    	Assert.assertEquals(new BigDecimal("5").setScale(4, RoundingMode.HALF_UP), measurementInfo.getQuantity());    	    	        	    	
    }
           
}
