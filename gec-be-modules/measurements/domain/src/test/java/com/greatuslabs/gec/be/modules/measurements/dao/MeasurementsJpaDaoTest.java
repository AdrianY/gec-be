package com.greatuslabs.gec.be.modules.measurements.dao;


import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.greatuslabs.gec.be.commons.test.annotations.GecTestExecutionListeners;
import com.greatuslabs.gec.be.commons.test.util.GecDBUnitXmlDataSetLoader;
import com.greatuslabs.gec.be.modules.core.domain.entity.Measurement;
import com.greatuslabs.gec.be.modules.core.infra.CoreDomainModuleJpaConfig;
import com.greatuslabs.gec.be.modules.measurements.domain.dao.jpa.MeasurementsJpaDao;
import com.greatuslabs.gec.be.modules.measurements.infra.MeasurementsModuleJpaConfig;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Adrian on 6/27/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@GecTestExecutionListeners
@SpringBootTest(
        classes = {MeasurementsModuleJpaConfig.class,CoreDomainModuleJpaConfig.class},
        webEnvironment = SpringBootTest.WebEnvironment.NONE
)
@EnableAutoConfiguration
@DbUnitConfiguration(dataSetLoader = GecDBUnitXmlDataSetLoader.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MeasurementsJpaDaoTest {

    @Autowired
    private MeasurementsJpaDao measurementsJpaDao;

    @Test
    @DatabaseSetup("/dao/MeasurementsJpaDaoTest/dbSetup_firstTest.xml")
    public void testQuery1_findAll(){

        assertEquals(1, measurementsJpaDao.count());

        Iterable<Measurement> records = measurementsJpaDao.findAll();
        assertNotNull(records);

        Iterator<Measurement> iterator = records.iterator();

        assertTrue(iterator.hasNext());
        Measurement measurement = iterator.next();

        assertEquals("Cup", measurement.getName());
        assertEquals("Volume", measurement.getType());
        assertEquals(new BigDecimal("1.00"), measurement.getQuantity());
    }

    @Test
    @DatabaseSetup("/dao/MeasurementsJpaDaoTest/dbSetup_secondTest.xml")
    public void testQuery2_findAllDependentMeasurements(){

        Measurement baseUOM = measurementsJpaDao.findOne(1L);
        List<Measurement> dependentMeasurements =
                measurementsJpaDao.findAllByBaseUom(baseUOM);

        assertNotNull(dependentMeasurements);
        assertFalse(dependentMeasurements.isEmpty());
        assertEquals(2, dependentMeasurements.size());

        Measurement dependentMeasurement = dependentMeasurements.get(0);

        assertEquals("Cup", dependentMeasurement.getName());
        assertEquals("Volume", dependentMeasurement.getType());
        assertEquals(new BigDecimal("450.00"), dependentMeasurement.getQuantity());
    }

    @Test
    @DatabaseSetup("/dao/MeasurementsJpaDaoTest/dbSetup_thirdTest.xml")
    public void testQuery3_findBaseUOM(){

        Measurement measurement = measurementsJpaDao.findOne(2L);
        Measurement baseUOM = measurementsJpaDao.findByDependentMeasurementsIn(measurement);
        assertNotNull(baseUOM);
        assertEquals("Millimetre", baseUOM.getName());
        assertEquals("Volume", baseUOM.getType());
        assertNull(baseUOM.getQuantity());
    }
}
