package com.greatuslabs.gec.be.modules.dishes.service;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.greatuslabs.gec.be.commons.test.annotations.GecTestExecutionListeners;
import com.greatuslabs.gec.be.commons.test.util.GecDBUnitXmlDataSetLoader;
import com.greatuslabs.gec.be.modules.core.domain.dto.DishesIngredientInfo;
import com.greatuslabs.gec.be.modules.core.infra.CoreDomainModuleJpaConfig;
import com.greatuslabs.gec.be.modules.dishes.infra.DishesModuleJpaConfig;
import com.greatuslabs.gec.be.modules.dishes.infra.DishesModuleScanConfig;
import com.greatuslabs.gec.be.modules.dishes.domain.service.DishesQueryService;
import com.greatuslabs.gec.be.modules.measurements.infra.MeasurementsModuleJpaConfig;
import com.greatuslabs.gec.be.modules.measurements.infra.MeasurementsModuleScanConfig;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
@GecTestExecutionListeners
@ContextConfiguration(classes = {DishesModuleJpaConfig.class, MeasurementsModuleJpaConfig.class, CoreDomainModuleJpaConfig.class})
@SpringBootTest(classes = {DishesModuleScanConfig.class, MeasurementsModuleScanConfig.class})
@DbUnitConfiguration(dataSetLoader = GecDBUnitXmlDataSetLoader.class)
public class DishesQueryServiceTest {

    @Autowired
    private DishesQueryService dishesQueryService;

    @Test
    @DatabaseSetup("/mappingTest.xml")
    public void testFindDishesInfo () {

        List<DishesIngredientInfo> dishesInfo = dishesQueryService.getAllIngredients(1L);

        Assert.assertNotNull(dishesInfo);
        Assert.assertEquals(4, dishesInfo.size());

        DishesIngredientInfo dishInfo = dishesInfo.get(0);
        Assert.assertEquals("Adobo", dishInfo.getDishName());
        Assert.assertEquals("Chicken", dishInfo.getIngredientName());
        Assert.assertEquals(new BigDecimal("300").setScale(4, RoundingMode.HALF_UP), dishInfo.getCost());

        dishInfo = dishesInfo.get(1);
        Assert.assertEquals("Garlic", dishInfo.getIngredientName());
        Assert.assertEquals(new BigDecimal("45").setScale(4, RoundingMode.HALF_UP), dishInfo.getCost());

        dishInfo = dishesInfo.get(2);
        Assert.assertEquals("Soy Sauce", dishInfo.getIngredientName());
        Assert.assertEquals(new BigDecimal("120").setScale(4, RoundingMode.HALF_UP), dishInfo.getCost());

        dishInfo = dishesInfo.get(3);
        Assert.assertEquals("Vinegar", dishInfo.getIngredientName());
        Assert.assertEquals(new BigDecimal("120").setScale(4, RoundingMode.HALF_UP), dishInfo.getCost());
    }


}
