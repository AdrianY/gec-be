package com.greatuslabs.gec.be.modules.dishes.dao;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.greatuslabs.gec.be.commons.test.annotations.GecTestExecutionListeners;
import com.greatuslabs.gec.be.commons.test.util.GecDBUnitXmlDataSetLoader;
import com.greatuslabs.gec.be.modules.core.domain.entity.Dish;
import com.greatuslabs.gec.be.modules.dishes.domain.dao.jpa.DishesJpaDao;
import com.greatuslabs.gec.be.modules.dishes.infra.DishesModuleJpaConfig;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
@GecTestExecutionListeners
@ContextConfiguration(classes = DishesModuleJpaConfig.class)
@DbUnitConfiguration(dataSetLoader = GecDBUnitXmlDataSetLoader.class)
public class DishesJpaDaoTest {

    @Autowired
    private DishesJpaDao dishesJpaDao;

    @Test
    @DatabaseSetup("/mappingTest.xml")
    public void testFindAll() {
        List<Dish> dish = dishesJpaDao.findAll();
        Assert.assertNotNull(dish);
        Assert.assertFalse(dish.isEmpty());
    }

}
