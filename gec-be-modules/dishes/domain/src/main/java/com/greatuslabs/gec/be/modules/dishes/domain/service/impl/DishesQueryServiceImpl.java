package com.greatuslabs.gec.be.modules.dishes.domain.service.impl;

import com.greatuslabs.be.commons.persistence.core.api.annotations.TransactionalService;
import com.greatuslabs.gec.be.modules.core.domain.dao.jpa.DishesIngredientJpaDao;
import com.greatuslabs.gec.be.modules.core.domain.dto.DishesIngredientInfo;
import com.greatuslabs.gec.be.modules.core.domain.entity.Dish;
import com.greatuslabs.gec.be.modules.core.domain.entity.DishesIngredient;
import com.greatuslabs.gec.be.modules.dishes.domain.dao.jpa.DishesJpaDao;
import com.greatuslabs.gec.be.modules.dishes.domain.service.DishesQueryService;
import com.greatuslabs.gec.be.modules.measurements.domain.service.CalculationService;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@TransactionalService("dishesQueryService")
public class DishesQueryServiceImpl implements DishesQueryService{

    private final DishesJpaDao dishesJpaDao;

    private final DishesIngredientJpaDao dishesIngredientJpaDao;

    private final CalculationService calculationService;

    public DishesQueryServiceImpl(
            DishesJpaDao dishesJpaDao,
            DishesIngredientJpaDao dishesIngredientJpaDao,
            CalculationService calculationService
    ) {
        this.dishesJpaDao = dishesJpaDao;
        this.dishesIngredientJpaDao = dishesIngredientJpaDao;
        this.calculationService = calculationService;
    }

    @Override
    public List<DishesIngredientInfo> getAllIngredients(Long dishId) {

        Dish dish = dishesJpaDao.findOne(dishId);

        if(Objects.isNull(dish))
            throw new RuntimeException("Dish id "+dishId+" does not exist in db");

        Set<DishesIngredient> ingredients = dishesIngredientJpaDao.findAllByDish(dish);

        return ingredients.stream()
                .map(i -> new DishesIngredientInfo(
                        dish.getName(),
                        i.getIngredient().getName(),
                        i.getUom().getName(),
                        i.getQuantity(),
                        calculationService.calculateIngredientTotalCostPerDish(i, i.getIngredient())))
                .sorted(Comparator.comparing(DishesIngredientInfo::getIngredientName))
                .collect(Collectors.toList());
    }

}
