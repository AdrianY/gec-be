package com.greatuslabs.gec.be.modules.dishes.infra;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"com.greatuslabs.gec.be.modules.dishes.domain",
        "com.greatuslabs.gec.be.modules.core.domain.entity"})
public class DishesModuleScanConfig {
}
