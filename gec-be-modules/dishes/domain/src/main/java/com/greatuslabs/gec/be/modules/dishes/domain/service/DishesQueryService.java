package com.greatuslabs.gec.be.modules.dishes.domain.service;

import com.greatuslabs.gec.be.modules.core.domain.dto.DishesIngredientInfo;

import java.util.List;

public interface DishesQueryService {

    List<DishesIngredientInfo> getAllIngredients(Long dishId);

}
