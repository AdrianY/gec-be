package com.greatuslabs.gec.be.modules.dishes.domain.dao.jpa;

import com.greatuslabs.be.commons.persistence.jpa.api.dao.BaseJpaDao;
import com.greatuslabs.gec.be.modules.core.domain.entity.Dish;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DishesJpaDao extends BaseJpaDao<Dish> {
}
