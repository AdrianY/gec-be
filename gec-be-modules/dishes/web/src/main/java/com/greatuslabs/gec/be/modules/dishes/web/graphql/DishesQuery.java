package com.greatuslabs.gec.be.modules.dishes.web.graphql;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.greatuslabs.gec.be.modules.core.domain.dto.DishesIngredientInfo;
import com.greatuslabs.gec.be.modules.dishes.domain.service.DishesQueryService;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DishesQuery implements GraphQLQueryResolver {

    private final DishesQueryService dishesQueryService;

    public DishesQuery(DishesQueryService dishesQueryService) {
        this.dishesQueryService = dishesQueryService;
    }

    public List<DishesIngredientInfo> getAllIngredients(Long dishId){
        return dishesQueryService.getAllIngredients(dishId);
    }
}
