package com.greatuslabs.gec.be.modules.security.infra.config;

import com.greatuslabs.be.commons.security.web.hmac.api.references.RestHmacReferences;
import com.greatuslabs.commons.be.security.web.core.api.infra.dto.SecurityUrlMatchersRegister;
import org.springframework.stereotype.Component;

@Component
public class GraphQLSecurityUrlMatchersRegister implements SecurityUrlMatchersRegister {
    @Override
    public String getTargetSecurityConfigKey() {
        return RestHmacReferences.SECURITY_CONFIG_KEY;
    }

    @Override
    public String[] getUrlMatchersToScan() {
        return new String[]{
            "/graphiql",
            "/graphql"
        };
    }
}
