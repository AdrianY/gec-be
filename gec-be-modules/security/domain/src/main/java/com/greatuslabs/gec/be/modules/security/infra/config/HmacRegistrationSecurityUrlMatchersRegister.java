package com.greatuslabs.gec.be.modules.security.infra.config;

import com.greatuslabs.be.commons.security.web.basic.api.references.WebBasicSecurityReferences;
import com.greatuslabs.commons.be.security.web.core.api.infra.dto.SecurityUrlMatchersRegister;
import org.springframework.stereotype.Component;

@Component
public class HmacRegistrationSecurityUrlMatchersRegister implements SecurityUrlMatchersRegister {
    @Override
    public String getTargetSecurityConfigKey() {
        return WebBasicSecurityReferences.SECURITY_CONFIG_KEY;
    }

    @Override
    public String[] getUrlMatchersToScan() {
        return new String[]{
                "/registered-user/**"
        };
    }
}
