package com.greatuslabs.gec.be.modules.security.infra.config;

import com.greatuslabs.be.commons.persistence.jpa.api.infra.EntityPackagesRegistry;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(
        basePackages = {"com.greatuslabs.gec.be.modules.security.domain.dao.jpa"}
)
public class SecurityModuleJpaConfig implements EntityPackagesRegistry {
    @Override
    public String[] getEntityPackagesToScan() {
        return new String[]{
                "com.greatuslabs.gec.be.modules.security.domain.entities"
        };
    }
}