package com.greatuslabs.gec.be.modules.security.domain.entities;

import com.greatuslabs.be.commons.persistence.jpa.api.entities.BaseSingularEntity;
import com.greatuslabs.be.commons.security.core.api.entities.UserAccount;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "user_account")
public class User extends BaseSingularEntity implements UserAccount {

    @Column(name = "user_name")
    private String username;

    @Column(name = "password")
    private String password;

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getPassword() {
        return password;
    }


}
