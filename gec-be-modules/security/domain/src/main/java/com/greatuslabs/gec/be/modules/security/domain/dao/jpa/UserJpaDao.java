package com.greatuslabs.gec.be.modules.security.domain.dao.jpa;

import com.greatuslabs.be.commons.persistence.jpa.api.dao.BaseJpaDao;
import com.greatuslabs.be.commons.security.core.api.dao.UserAccountDao;
import com.greatuslabs.gec.be.modules.security.domain.entities.User;

public interface UserJpaDao extends BaseJpaDao<User>, UserAccountDao<User> {
    User findByUsername(String username);
}
