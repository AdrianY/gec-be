GEC
===

This is a Spring based project for an application that can be used
by restaurants to compute the cost of a Dish. This project is composed
of several modules grouped in to two:

## Commons

This module contains various sub modules that provide either functionality, tools, 
or infrastructure to assist the development of this project. The long term
goal for this module is it to be used on other projects. The aim is to make it an 
application development framework that builds on top of Spring framework.

## Modules

This module contains the various business services that makes this Dish cost 
calculator App work. The design is modular and is aimed to be inter connected via 
spring integration. Business services will be exposed via GraphQL.

*WIP*