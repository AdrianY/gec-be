GEC Local deployment
====================

To specify properties provide the following in separate application.yml file:

<pre><code>
server:
  port: 7600
  contextPath: /gec

management.security.enabled: false

logging:
  file: /Users/ayago/test-data/gec/gec.log
  path: /Users/ayago/test-data/gec

graphiql:
  mapping: /graphiql
  endpoint: /gec/graphql
  enabled: true

#H2 and hibernate
spring:
  datasource:
    platform: h2
    initialize: true
  jpa.hibernate.ddl-auto: verify
  h2.console:
    enabled: true
    path: /h2

#Datasource
gec.datasource:
  url: jdbc:h2:file:~/test
  username: sa
  password:
  driver-class-name: org.h2.Driver

#security
gl.security:
  enabled: false
  rest.hmac.registration:
    secret-key-length: 40 #minimum of 40
</code></pre>

Note: to enable loading of data set <code>spring.datasource.initialize</code> to true

To specify api swagger config specify the following in a separate ym file

<pre><code>
gl:
  web:
    rest:
      documentation:
        exclude-actuator-paths: true
        api-doc-details:
          title: "GEC API"
          description: "Description of GEC REST API Endpoints"
          contact-details:
            name: "Dev"
            url: "some-devs.com"
</code></pre>

