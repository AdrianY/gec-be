SET REFERENTIAL_INTEGRITY FALSE;
DROP TABLE IF EXISTS user_account;
DROP TABLE IF EXISTS gec_measurements;
DROP TABLE IF EXISTS gec_ingredients;
DROP TABLE IF EXISTS gec_dishes_ingredients;
DROP TABLE IF EXISTS gec_dishes;
SET REFERENTIAL_INTEGRITY TRUE;

CREATE TABLE IF NOT EXISTS user_account (
    id LONG PRIMARY KEY,
    user_name VARCHAR(50) NOT NULL,
    password VARCHAR(32) NOT NULL
);

CREATE TABLE IF NOT EXISTS gec_measurements (
    id LONG PRIMARY KEY,
    type VARCHAR(15) NOT NULL,
    name VARCHAR(100) NOT NULL,
    quantity DECIMAL NOT NULL,
    base_uom LONG NOT NULL
);

CREATE TABLE IF NOT EXISTS gec_ingredients (
    id LONG PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    cost_per_uom DECIMAL NOT NULL,
    uom_id LONG NOT NULL
);

CREATE TABLE IF NOT EXISTS gec_dishes_ingredients (
    ingredient_id LONG NOT NULL,
    dish_id LONG NOT NULL,
    quantity DECIMAL NOT NULL,
    uom_id INT NOT NULL,
    PRIMARY KEY(ingredient_id,dish_id)
);

CREATE TABLE IF NOT EXISTS gec_dishes (
    id LONG PRIMARY KEY,
    name VARCHAR(100) NOT NULL
);