package com.greatuslabs.gec.be.dist.core;

import com.greatuslabs.be.commons.security.web.basic.config.WebBasicSecurityAutoConfiguration;
import com.greatuslabs.commons.be.security.web.portal.form.config.FormLoginAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by Adrian on 6/20/2017.
 */
@SpringBootApplication(
        scanBasePackages = "com.greatuslabs.gec.be.modules.**"
)
@EnableAutoConfiguration(
        exclude = {FormLoginAutoConfiguration.class}
)
public class Runner {
    public static void main(String[] args) {
        SpringApplication.run(Runner.class, args);
    }
}