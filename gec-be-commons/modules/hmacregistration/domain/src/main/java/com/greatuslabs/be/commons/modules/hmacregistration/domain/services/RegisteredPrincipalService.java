package com.greatuslabs.be.commons.modules.hmacregistration.domain.services;

public interface RegisteredPrincipalService {

    /**
     * returns created hmac secret key
     *
     * @param principalName
     * @return
     */
    String registerPrincipal(String principalName);
}
