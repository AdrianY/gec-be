package com.greatuslabs.be.commons.modules.hmacregistration.domain.services.impl;


import com.greatuslabs.be.commons.modules.hmacregistration.domain.properties.HmacRegistrationDomainConfigProperties;
import com.greatuslabs.be.commons.modules.hmacregistration.domain.services.RegisteredPrincipalService;
import com.greatuslabs.be.commons.security.rest.hmac.utilities.HmacUtils;
import com.greatuslabs.be.commons.security.web.hmac.api.service.PrincipalSecretKeyService;

public class RegisteredPrincipalServiceImpl implements RegisteredPrincipalService {

    private final HmacRegistrationDomainConfigProperties hmacRegistrationDomainConfigProperties;
    private final PrincipalSecretKeyService principalSecretKeyService;

    public RegisteredPrincipalServiceImpl(
            HmacRegistrationDomainConfigProperties hmacRegistrationDomainConfigProperties,
            PrincipalSecretKeyService principalSecretKeyService
    ) {
        this.hmacRegistrationDomainConfigProperties = hmacRegistrationDomainConfigProperties;
        this.principalSecretKeyService = principalSecretKeyService;
    }

    @Override
    public String registerPrincipal(String principalName) {
        String generatedSecretKey =
                HmacUtils.generateEncodedKey(hmacRegistrationDomainConfigProperties.getSecretKeyLength());
        principalSecretKeyService.registerPrincipal(principalName, generatedSecretKey);
        return generatedSecretKey;
    }
}
