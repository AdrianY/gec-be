package com.greatuslabs.be.commons.modules.hmacregistration.domain.properties;

import javax.validation.constraints.Min;

public class HmacRegistrationDomainConfigProperties {

    private static final int MINIMUM_KEY_LENGTH = 40;
    private static final String SECRET_KEY_LENGTH_MIN_ERROR_MESSAGE =
            "HMAC Key Length must be at least: "+MINIMUM_KEY_LENGTH;

    @Min(value = MINIMUM_KEY_LENGTH, message = SECRET_KEY_LENGTH_MIN_ERROR_MESSAGE)
    private Integer secretKeyLength;

    public HmacRegistrationDomainConfigProperties(){
        secretKeyLength = MINIMUM_KEY_LENGTH;
    }

    public Integer getSecretKeyLength() {
        return secretKeyLength;
    }

    public void setSecretKeyLength(Integer secretKeyLength) {
        this.secretKeyLength = secretKeyLength;
    }
}
