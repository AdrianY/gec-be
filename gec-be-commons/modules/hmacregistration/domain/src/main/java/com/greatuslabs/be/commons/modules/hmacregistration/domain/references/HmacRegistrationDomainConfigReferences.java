package com.greatuslabs.be.commons.modules.hmacregistration.domain.references;

public interface HmacRegistrationDomainConfigReferences {
    String HMAC_REGISTRATION_DOMAIN_CONFIG_PREFIX = "gl.security.rest.hmac.registration";
}
