package com.greatuslabs.be.commons.modules.hmacregistration;

import com.greatuslabs.be.commons.modules.hmacregistration.domain.properties.HmacRegistrationDomainConfigProperties;
import com.greatuslabs.be.commons.modules.hmacregistration.domain.references.HmacRegistrationDomainConfigReferences;
import com.greatuslabs.be.commons.modules.hmacregistration.domain.services.RegisteredPrincipalService;
import com.greatuslabs.be.commons.modules.hmacregistration.domain.services.impl.RegisteredPrincipalServiceImpl;
import com.greatuslabs.be.commons.security.web.hmac.api.service.PrincipalSecretKeyService;
import com.greatuslabs.be.commons.security.web.hmac.core.config.RestHmacAutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

@Configuration
//@ConditionalOnBean(RestHmacAutoConfiguration.class)
//@ConditionalOnClass(RestHmacAutoConfiguration.class)
//@AutoConfigureAfter(RestHmacAutoConfiguration.class)
public class HmacRegistrationDomainAutoConfig {

    @Bean
    @Validated
    @ConfigurationProperties(HmacRegistrationDomainConfigReferences.HMAC_REGISTRATION_DOMAIN_CONFIG_PREFIX)
    public HmacRegistrationDomainConfigProperties hmacRegistrationDomainConfigProperties(){
        return new HmacRegistrationDomainConfigProperties();
    }

    @Bean
    @Transactional
    @ConditionalOnBean({HmacRegistrationDomainConfigProperties.class, PrincipalSecretKeyService.class})
    public RegisteredPrincipalService registeredPrincipalService(
            HmacRegistrationDomainConfigProperties hmacRegistrationDomainConfigProperties,
            PrincipalSecretKeyService principalSecretKeyService
    ){
        return new RegisteredPrincipalServiceImpl(
                hmacRegistrationDomainConfigProperties, principalSecretKeyService);
    }
}
