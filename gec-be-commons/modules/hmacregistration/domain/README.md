HMAC Registration
=================

Adds capability to provide HMAC users with the secret key upon registration.

## Requirement

Persistence API module
HMAC API module
HMAC Config module