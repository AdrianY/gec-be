package com.greatuslabs.be.commons.modules.hmacregistration.endpoints.rest;

import com.greatuslabs.be.commons.modules.hmacregistration.HmacRegistrationDomainAutoConfig;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
//@ConditionalOnBean(HmacRegistrationDomainAutoConfig.class)
@AutoConfigureAfter(HmacRegistrationDomainAutoConfig.class)
public class HmacRegistrationGraphQLEndpointAutoConfig {
}
