package com.greatuslabs.be.commons.modules.hmacregistration.endpoints.rest.resources;

import com.greatuslabs.be.commons.modules.hmacregistration.domain.services.RegisteredPrincipalService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RegisteredUserContoller {

    private final RegisteredPrincipalService registeredPrincipalService;

    public RegisteredUserContoller(RegisteredPrincipalService registeredPrincipalService) {
        this.registeredPrincipalService = registeredPrincipalService;
    }

    @RequestMapping(method= RequestMethod.GET, path = "/registered-user/")
    public String registerUser(){
        String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return registeredPrincipalService.registerPrincipal(username);
    }
}
