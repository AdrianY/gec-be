package com.greatuslabs.be.commons.persistence.jpa.api.entities;

import javax.persistence.*;


@MappedSuperclass
public abstract class BaseSingularEntity extends BaseEntity<Long>{

    @Id
    @Column(name = "id")
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long id;

    public Long getId(){
        return id;
    }

}
