package com.greatuslabs.be.commons.persistence.jpa.api.entities;

import org.springframework.data.domain.Persistable;

import java.io.Serializable;

/**
 * Created by Adrian on 6/28/2017.
 */
public interface PersistableData<ID extends Serializable> extends Persistable<ID> {
}
