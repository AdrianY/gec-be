package com.greatuslabs.be.commons.persistence.jpa.api.dao;

import com.greatuslabs.be.commons.persistence.jpa.api.entities.BaseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * Created by Adrian on 6/28/2017.
 */
@NoRepositoryBean
public interface BaseJpaDao<TYPE extends BaseEntity> extends JpaRepository<TYPE, Long> {
}
