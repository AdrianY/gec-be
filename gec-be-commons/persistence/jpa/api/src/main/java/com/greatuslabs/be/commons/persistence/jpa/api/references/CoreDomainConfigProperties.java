package com.greatuslabs.be.commons.persistence.jpa.api.references;

public interface CoreDomainConfigProperties {
    String PERSISTENCE_UNIT = "gec_pu";
    String TRANSACTION_MANAGER_NAME = "transactionManager";
    String ENTITY_MANAGER_FACTORY_NAME = "entityManagerFactory";
    String DATASOURCE_NAME = "dataSource";
    String DATASOURCE_CONFIG_PROPERTIES_PREFIX = "gec.datasource";
}
