package com.greatuslabs.be.commons.persistence.jpa.api.infra;

public interface EntityPackagesRegistry {
    String[] getEntityPackagesToScan();
}
