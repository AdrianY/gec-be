package com.greatuslabs.be.commons.persistence.jpa.api.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Adrian on 6/28/2017.
 */
@MappedSuperclass
public abstract class BaseEntity<ID extends Serializable> implements PersistableData<ID>{

    public abstract ID getId();

    public boolean isNew(){
        return Objects.isNull(getId());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseEntity that = (BaseEntity) o;

        return getId() != null ? getId().equals(that.getId()) : that.getId() == null;
    }

    @Override
    public int hashCode() {
        return getId() != null ? getId().hashCode() : 0;
    }
}
