package com.greatuslabs.be.commons.persistence.jpa.config;

import com.greatuslabs.be.commons.persistence.jpa.api.infra.EntityPackagesRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.List;
import java.util.stream.Stream;

import static com.greatuslabs.be.commons.persistence.jpa.api.references.CoreDomainConfigProperties.*;

@Configuration
@EnableTransactionManagement
public class CoreJpaPersistenceConfig {

    @Autowired
    private List<EntityPackagesRegistry> entityPackagesRegistryList;

    @Autowired
    private ApplicationContext applicationContext;

    @Primary
    @Bean(name = DATASOURCE_NAME)
    @ConfigurationProperties(prefix = DATASOURCE_CONFIG_PROPERTIES_PREFIX)
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = ENTITY_MANAGER_FACTORY_NAME)
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            EntityManagerFactoryBuilder builder,
            @Qualifier(DATASOURCE_NAME) DataSource dataSource) {

        String[] entityPackagesToScan = entityPackagesRegistryList.stream()
                .flatMap(r -> Stream.of(r.getEntityPackagesToScan())).toArray(String[]::new);

        return builder.dataSource(dataSource)
                .packages(entityPackagesToScan)
                .persistenceUnit(PERSISTENCE_UNIT)
                .build();
    }

    @Bean(name = TRANSACTION_MANAGER_NAME)
    public PlatformTransactionManager transactionManager(
            @Qualifier(ENTITY_MANAGER_FACTORY_NAME) EntityManagerFactory beEntityManagerFactory) {
        return new JpaTransactionManager(beEntityManagerFactory);
    }
}
