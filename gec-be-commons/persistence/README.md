Commons Persistence
===================

Designed to provide auto configuration of datasources that is usable across different module. It
should also support multi datasources application be it either SQL or NoSQL datasources.