Commons Validation
==================

Designed to provide easy configuration of validation code in the service layer by making sure
validation is coded separately but can easily be triggered via annotations. This also provides
support for back end driven validation by including validation metadata to mutatable resources
exposed via HTTP.