package com.greatuslabs.be.commons.validation.config.domain.exceptions;

public class NotExistingValidateablePropertyException extends RuntimeException{
    public NotExistingValidateablePropertyException(String message) {
        super(message);
    }
}
