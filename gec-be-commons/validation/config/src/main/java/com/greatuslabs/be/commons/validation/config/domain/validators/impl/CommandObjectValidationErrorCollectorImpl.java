package com.greatuslabs.be.commons.validation.config.domain.validators.impl;


import com.greatuslabs.be.commons.validation.api.dto.FieldError;
import com.greatuslabs.be.commons.validation.api.validators.CommandObjectValidationErrorCollector;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CommandObjectValidationErrorCollectorImpl implements CommandObjectValidationErrorCollector {

    @Override
    public <COMMAND_OBJECT> List<FieldError> collectErrors(
            COMMAND_OBJECT commandObject, Class<?> groupInterfaceClass) {
        Validator validator = getDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<COMMAND_OBJECT>> constraintViolations =
                Void.class.equals(groupInterfaceClass) ?
                        validator.validate(commandObject) :
                        validator.validate(commandObject, groupInterfaceClass);

        if(constraintViolations.isEmpty()) return new ArrayList<>();

        return constraintViolations.stream()
                .map(cV -> new FieldError(cV.getPropertyPath().toString(), cV.getMessage(), cV.getInvalidValue()))
                .collect(Collectors.toList());
    }

    private static ValidatorFactory getDefaultValidatorFactory(){
        return Validation.buildDefaultValidatorFactory();
    }
}
