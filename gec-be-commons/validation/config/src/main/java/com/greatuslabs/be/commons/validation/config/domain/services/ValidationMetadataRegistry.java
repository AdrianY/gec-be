package com.greatuslabs.be.commons.validation.config.domain.services;

import com.greatuslabs.be.commons.validation.api.dto.ValidationMetadata;

import java.util.List;

public interface ValidationMetadataRegistry {
    List<ValidationMetadata> getValidationMetadata(Class<?> queryResultObjectClass);
}
