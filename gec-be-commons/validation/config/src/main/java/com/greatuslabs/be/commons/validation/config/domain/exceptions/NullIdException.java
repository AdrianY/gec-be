package com.greatuslabs.be.commons.validation.config.domain.exceptions;

public class NullIdException extends RuntimeException{

    private final String parameterName;

    public NullIdException(String parameterName, String message) {
        super(message);
        this.parameterName = parameterName;
    }

    public String getParameterName() {
        return parameterName;
    }
}
