package com.greatuslabs.be.commons.validation.config.domain.exceptions;


import com.greatuslabs.be.commons.validation.config.domain.references.ValidatorType;

public class NoValidatorFoundException extends RuntimeException{
    private final String validatorName;

    private final ValidatorType validatorType;

    public NoValidatorFoundException(String validatorName, ValidatorType validatorType) {
        super("No validator found with getName \""+validatorName+"\"");
        this.validatorName = validatorName;
        this.validatorType = validatorType;
    }

    public String getValidatorName() {
        return validatorName;
    }

    public ValidatorType getValidatorType() {
        return validatorType;
    }
}
