package com.greatuslabs.be.commons.validation.config.domain.services;


import com.greatuslabs.be.commons.validation.api.dto.ValidationMetadata;

import java.util.List;
import java.util.Map;

public interface ValidationMetadataMappingBuilder {
    Map<Class<?>, List<ValidationMetadata>> constructRegistry();
}
