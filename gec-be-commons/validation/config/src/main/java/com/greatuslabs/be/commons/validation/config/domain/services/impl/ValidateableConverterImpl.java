package com.greatuslabs.be.commons.validation.config.domain.services.impl;

import com.greatuslabs.be.commons.validation.api.dto.Operation;
import com.greatuslabs.be.commons.validation.api.dto.ValidateableModel;
import com.greatuslabs.be.commons.validation.api.dto.ValidationMetadata;
import com.greatuslabs.be.commons.validation.api.services.ValidateableConverter;
import com.greatuslabs.be.commons.validation.config.domain.services.ValidationMetadataRegistry;
import org.apache.commons.collections4.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ValidateableConverterImpl implements ValidateableConverter {

    private final ValidationMetadataRegistry validationMetadataRegistry;

    public ValidateableConverterImpl(ValidationMetadataRegistry validationMetadataRegistry) {
        this.validationMetadataRegistry = validationMetadataRegistry;
    }

    @Override
    public <QUERY_RESULT_OBJECT> ValidateableModel<
                        QUERY_RESULT_OBJECT> toValidateable(QUERY_RESULT_OBJECT queryResultObject) {

        List<ValidationMetadata> validationMetadata =
                validationMetadataRegistry.getValidationMetadata(queryResultObject.getClass());

        Map<String, List<Operation<?>>> validations =
                CollectionUtils.isNotEmpty(validationMetadata) ?
                        validationMetadata.stream()
                            .collect(Collectors.toMap(
                                    ValidationMetadata::getFieldName, ValidationMetadata::getValidations)) :
                        new HashMap<>();

        return new ValidateableModel<>(
                queryResultObject,
                validations
        );
    }
}
