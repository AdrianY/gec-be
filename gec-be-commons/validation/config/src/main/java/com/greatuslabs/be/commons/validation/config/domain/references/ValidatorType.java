package com.greatuslabs.be.commons.validation.config.domain.references;

public enum ValidatorType {
    ID_VALIDATOR("ID");

    private String code;

    ValidatorType(String code){
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
