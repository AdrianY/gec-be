package com.greatuslabs.be.commons.validation.config.domain.aspects;

import com.greatuslabs.be.commons.validation.api.annotations.IsValidCommandObject;
import com.greatuslabs.be.commons.validation.api.dto.FieldError;
import com.greatuslabs.be.commons.validation.api.util.FieldErrorsRegistry;
import com.greatuslabs.be.commons.validation.api.validators.CommandObjectValidationErrorCollector;
import com.greatuslabs.be.commons.validation.api.validators.CommandObjectValidator;
import com.greatuslabs.be.commons.validation.config.domain.exceptions.NullCommandObjectException;
import com.greatuslabs.be.commons.validation.config.domain.services.CommandObjectValidatorRegistry;
import org.apache.commons.collections4.CollectionUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Objects;

@Aspect
public class CommandObjectValidationAspect {

    private final CommandObjectValidatorRegistry commandObjectValidatorRegistry;
    private final CommandObjectValidationErrorCollector commandObjectValidationErrorCollector;


    public CommandObjectValidationAspect(
            CommandObjectValidatorRegistry commandObjectValidatorRegistry,
            CommandObjectValidationErrorCollector commandObjectValidationErrorCollector
    ) {
        this.commandObjectValidatorRegistry = commandObjectValidatorRegistry;
        this.commandObjectValidationErrorCollector = commandObjectValidationErrorCollector;
    }

    @Before("execution(* *(.., @com.greatuslabs.be.commons.validation.api.annotations.IsValidCommandObject (*), ..))")
    public void validateCommand(JoinPoint jp) throws NoSuchMethodException {

        // Get the target method
        Method interfaceMethod = ((MethodSignature)jp.getSignature()).getMethod();
        Method implementationMethod =
                jp.getTarget().getClass().getMethod(interfaceMethod.getName(), interfaceMethod.getParameterTypes());

        // Get the annotated parameters and validate those with the @Valid annotation
        Annotation[][] annotationParameters = implementationMethod.getParameterAnnotations();
        for (int i = 0; i < annotationParameters.length; i++) {
            Annotation[] annotations = annotationParameters[i];
            for (Annotation annotation : annotations) {
                if (annotation.annotationType().equals(IsValidCommandObject.class)) {
                    IsValidCommandObject valid = (IsValidCommandObject)annotation;
                    Object commandObject = jp.getArgs()[i];
                    validateCommandObject(
                            valid,
                            commandObject
                    );
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    private <COMMAND_OBJECT> void validateCommandObject(
            IsValidCommandObject validationDetails,
            COMMAND_OBJECT commandObject
    ){

        Class<?> groupValidationClass = validationDetails.groupValidationClass();

        if(Objects.isNull(commandObject))
            throw new NullCommandObjectException("Command Object to be validated under validation class "+
                    groupValidationClass.getName()+", cannot be null");

        CommandObjectValidator<COMMAND_OBJECT> validator =
                commandObjectValidatorRegistry.retrieveValidator(
                        (Class<? extends COMMAND_OBJECT>) commandObject.getClass(),
                        groupValidationClass);

        if(null == validator){
            List<FieldError> fieldErrors =
                    commandObjectValidationErrorCollector.collectErrors(
                            commandObject, groupValidationClass);

            if(CollectionUtils.isNotEmpty(fieldErrors))
                FieldErrorsRegistry.processFieldErrors(commandObject, groupValidationClass,  fieldErrors);

        }else{
            validator.validateCommand(commandObject, validationDetails.performFullValidation());
        }

    }
}
