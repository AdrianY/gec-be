package com.greatuslabs.be.commons.validation.config.domain.services.impl;

import com.greatuslabs.be.commons.validation.api.builders.*;
import com.greatuslabs.be.commons.validation.api.dto.Operation;
import com.greatuslabs.be.commons.validation.api.dto.OperationValidationResult;
import com.greatuslabs.be.commons.validation.api.dto.ValidationMetadata;
import com.greatuslabs.be.commons.validation.config.domain.exceptions.NotExistingValidateablePropertyException;
import com.greatuslabs.be.commons.validation.config.domain.exceptions.ValidationMetadataException;
import com.greatuslabs.be.commons.validation.config.domain.services.ValidationMetadataMappingBuilder;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ValidationMappingBuilderImpl implements ValidationMapperFactory, ValidationMetadataMappingBuilder {

    private List<ClassFieldValidationMapping> validationMetadataList;

    public ValidationMappingBuilderImpl() {
        validationMetadataList = new ArrayList<>();
    }

    public ClassFieldValidationMappingBuilderImpl mapClass(Class<?> targetClass){
        return new ClassFieldValidationMappingBuilderImpl(this, targetClass);
    }

    void register(ClassFieldValidationMapping classFieldValidationMapping){
        validationMetadataList.add(classFieldValidationMapping);
    }

    public Map<Class<?>, List<ValidationMetadata>> constructRegistry(){
        Map<Class<?>, List<ValidationMetadata>> registry = validationMetadataList.stream()
                .collect(
                        Collectors.toMap(
                                v -> v.targetClass,
                                v -> v.validationMetadata
                        )
                );
        validationMetadataList = null;
        return registry;
    }

    public static class ClassFieldValidationMappingBuilderImpl
            implements ClassFieldValidationMappingBuilder, ValidationMapperRegistrar {
        private final ValidationMappingBuilderImpl parent;
        private final Class<?> targetClass;
        private List<FieldValidationMetadataBuilderImpl> fieldValidationMetadataBuilders;

        private ClassFieldValidationMappingBuilderImpl(ValidationMappingBuilderImpl parent, Class<?> targetClass) {
            this.parent = parent;
            this.targetClass = targetClass;
            this.fieldValidationMetadataBuilders = new ArrayList<>();
        }

        public ClassFieldValidationMappingBuilderImpl field(
                Consumer<FieldValidationMetadataBuilder> fieldBuilderFunction){
            FieldValidationMetadataBuilderImpl builder = new FieldValidationMetadataBuilderImpl(targetClass);
            fieldBuilderFunction.accept(builder);
            fieldValidationMetadataBuilders.add(builder);
            return this;
        }

        public void register(){
            List<ValidationMetadata> validationMetadata =
                    fieldValidationMetadataBuilders.stream()
                            .peek(b -> validateField(b.fieldName))
                            .map(FieldValidationMetadataBuilderImpl::build)
                            .collect(Collectors.toList());
            fieldValidationMetadataBuilders = null;
            parent.register(new ClassFieldValidationMapping(targetClass, validationMetadata));
        }

        private void validateField(String fieldName){

            if(StringUtils.isEmpty(fieldName))
                throw new NotExistingValidateablePropertyException("Validation metadata setup failed. " +
                        "Cannot map null or empty field for class " +
                        targetClass.getName());

            boolean fieldNameExists = getAllFields(targetClass).contains(fieldName);
            if(!fieldNameExists)
                throw new NotExistingValidateablePropertyException("Validation metadata setup failed. " +
                        "Field '"+fieldName+"' doesn't exist for class " +
                        targetClass.getName());
        }

        private static Set<String> getAllFields(final Class<?> type) {
            Set<String> fields = new HashSet<>();
            for (Field field : type.getDeclaredFields()) {
                fields.add(field.getName());
            }

            if (type.getSuperclass() != null) {
                fields.addAll(getAllFields(type.getSuperclass()));
            }
            return fields;
        }
    }

    public static final class FieldValidationMetadataBuilderImpl implements FieldValidationMetadataBuilder {
        private String fieldName;
        private ValidationOperationBuilder<?>[] fieldValidationBuilders;
        private Class<?> targetClass;

        private FieldValidationMetadataBuilderImpl(Class<?> targetClass){
            this.targetClass = targetClass;
        }

        @Override
        public void setFieldName(String fieldName) {
            this.fieldName = fieldName;
        }

        @Override
        public void validations(
                ValidationOperationBuilder<?>... validationOperationBuilders) {
            this.fieldValidationBuilders = validationOperationBuilders;
        }

        private ValidationMetadata build(){
            List<Operation<?>> constructedOperations =
                    Stream.of(fieldValidationBuilders)
                        .peek(b -> processOperationBuilderValidation(b.validate()))
                        .map(ValidationOperationBuilder::build)
                        .collect(Collectors.toList());
            fieldValidationBuilders = null;
            return new ValidationMetadata(fieldName, constructedOperations);
        }

        private void processOperationBuilderValidation(OperationValidationResult operationValidationResult){
            if(!operationValidationResult.isSuccessful())
                throw new ValidationMetadataException("Validation metadata setup failed. " +
                        "Validation for field '"+fieldName+"' for class " +
                        targetClass.getName() + " is invalid. " +
                        "Reason is: "+operationValidationResult.getReason());
        }
    }

    private static class ClassFieldValidationMapping{
        private final Class<?> targetClass;
        private final List<ValidationMetadata> validationMetadata;

        private ClassFieldValidationMapping(Class<?> targetClass, List<ValidationMetadata> validationMetadata) {
            this.targetClass = targetClass;
            this.validationMetadata = validationMetadata;
        }
    }


}
