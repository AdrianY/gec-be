package com.greatuslabs.be.commons.validation.config.domain.services.impl;

import com.greatuslabs.be.commons.validation.api.validators.CommandObjectValidator;
import com.greatuslabs.be.commons.validation.api.validators.MappedCommandObjectValidator;
import com.greatuslabs.be.commons.validation.config.domain.exceptions.ValidationFrameworkException;
import com.greatuslabs.be.commons.validation.config.domain.services.CommandObjectValidatorRegistry;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MappedCommandObjectValidatorRegistryImpl implements CommandObjectValidatorRegistry {

    private Map<Class<?>, Map<Class<?>, MappedCommandObjectValidator<?, ?>>> commandObjectValidatorGroupMap;

    public void registerValidators(List<MappedCommandObjectValidator<?, ?>> commandObjectValidators){
        commandObjectValidatorGroupMap = constructRegistryMap(commandObjectValidators);
    }

    private Map<Class<?>, Map<Class<?>, MappedCommandObjectValidator<?, ?>>> constructRegistryMap(
            List<MappedCommandObjectValidator<?, ?>> commandObjectValidators) {

        if(CollectionUtils.isEmpty(commandObjectValidators))
            throw new ValidationFrameworkException("Cannot register empty or null validators list");

        if(
                commandObjectValidators.stream().anyMatch(v -> Objects.isNull(v.getTargetCommandObjectClass())) ||
                        commandObjectValidators.stream().anyMatch(v -> Objects.isNull(v.getTargetGroupValidationClass()))
                ){
            throw new ValidationFrameworkException("Mapped validators should specify groupValidationClass and " +
                    "targetCommandObjectClass");
        }

        try {
            return commandObjectValidators.stream()
                    .collect(Collectors.groupingBy(
                            MappedCommandObjectValidator::getTargetCommandObjectClass,
                            Collectors.toMap(
                                    MappedCommandObjectValidator::getTargetGroupValidationClass,
                                    Function.identity()
                            )
                    ));
        }catch (IllegalStateException e){
            throw new ValidationFrameworkException("There are redundant command object validators");
        }

    }

    @SuppressWarnings("unchecked")
    @Override
    public <COMMAND_OBJECT, VALIDATION_CLASS> CommandObjectValidator<COMMAND_OBJECT> retrieveValidator(
            Class<? extends COMMAND_OBJECT> commandObjectClass, Class<VALIDATION_CLASS> groupValidationClass) {

        if(Objects.isNull(commandObjectValidatorGroupMap))
            return null;

        Map<Class<?>, MappedCommandObjectValidator<?, ?>> coValidatorGroup =
                commandObjectValidatorGroupMap.get(commandObjectClass);

        if(MapUtils.isEmpty(coValidatorGroup))
            return null;

        return (CommandObjectValidator<COMMAND_OBJECT>) coValidatorGroup.get(groupValidationClass);
    }

}
