package com.greatuslabs.be.commons.validation.config;

import com.greatuslabs.be.commons.validation.api.builders.ValidationMapperFactory;
import com.greatuslabs.be.commons.validation.api.services.ValidateableConverter;
import com.greatuslabs.be.commons.validation.api.validators.CommandObjectValidationErrorCollector;
import com.greatuslabs.be.commons.validation.api.validators.IdValidator;
import com.greatuslabs.be.commons.validation.api.validators.MappedCommandObjectValidator;
import com.greatuslabs.be.commons.validation.config.domain.aspects.CommandObjectValidationAspect;
import com.greatuslabs.be.commons.validation.config.domain.aspects.IdValidationAspect;
import com.greatuslabs.be.commons.validation.config.domain.services.CommandObjectValidatorRegistry;
import com.greatuslabs.be.commons.validation.config.domain.services.ValidationMetadataMappingBuilder;
import com.greatuslabs.be.commons.validation.config.domain.services.ValidationMetadataRegistry;
import com.greatuslabs.be.commons.validation.config.domain.services.impl.DefaultValidationMetadataRegistry;
import com.greatuslabs.be.commons.validation.config.domain.services.impl.MappedCommandObjectValidatorRegistryImpl;
import com.greatuslabs.be.commons.validation.config.domain.services.impl.ValidateableConverterImpl;
import com.greatuslabs.be.commons.validation.config.domain.services.impl.ValidationMappingBuilderImpl;
import com.greatuslabs.be.commons.validation.config.domain.validators.impl.CommandObjectValidationErrorCollectorImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import java.util.List;
import java.util.Map;

@Configuration
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class CoreValidationConfig {

    @Bean
    public CommandObjectValidationErrorCollector defaultObjectValidator(){
        return new CommandObjectValidationErrorCollectorImpl();
    }

    @Bean
    @ConditionalOnBean(MappedCommandObjectValidator.class)
    public CommandObjectValidatorRegistry commandObjectValidatorRegistry(
            @Autowired(required = false) List<MappedCommandObjectValidator<?,?>> mappedCommandObjectValidatorList){
        MappedCommandObjectValidatorRegistryImpl mappedCommandObjectValidatorRegistry =
                new MappedCommandObjectValidatorRegistryImpl();
        mappedCommandObjectValidatorRegistry.registerValidators(mappedCommandObjectValidatorList);
        return mappedCommandObjectValidatorRegistry;
    }

    @Bean
    @ConditionalOnBean({
            CommandObjectValidationErrorCollector.class,
            CommandObjectValidatorRegistry.class
    })
    public CommandObjectValidationAspect commandObjectValidationAspect(
            CommandObjectValidationErrorCollector commandObjectValidationErrorCollector,
            CommandObjectValidatorRegistry commandObjectValidatorRegistry
    ){
        return new CommandObjectValidationAspect(commandObjectValidatorRegistry, commandObjectValidationErrorCollector);
    }

    @Bean
    @ConditionalOnBean(IdValidator.class)
    public IdValidationAspect idValidationAspect(Map<String, IdValidator> idValidatorMap){
        return new IdValidationAspect(idValidatorMap);
    }

    @Bean
    public ValidationMapperFactory validationMapperFactory(){
        return new ValidationMappingBuilderImpl();
    }

    @Bean
    @ConditionalOnBean(ValidationMapperFactory.class)
    public ValidationMetadataMappingBuilder validationMetadataMappingBuilder(
            ValidationMapperFactory validationMapperFactory){
        return (ValidationMetadataMappingBuilder) validationMapperFactory;
    }

    @Bean
    @ConditionalOnBean(ValidationMetadataMappingBuilder.class)
    public ValidationMetadataRegistry validationMetadataRegistry(
            ValidationMetadataMappingBuilder validationMetadataMappingBuilder){
        return new DefaultValidationMetadataRegistry(validationMetadataMappingBuilder);
    }

    @Bean
    @ConditionalOnBean(ValidationMetadataRegistry.class)
    public ValidateableConverter converter(ValidationMetadataRegistry validationMetadataRegistry){
        return new ValidateableConverterImpl(validationMetadataRegistry);
    }
}
