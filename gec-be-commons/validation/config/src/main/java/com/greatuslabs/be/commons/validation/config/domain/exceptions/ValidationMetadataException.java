package com.greatuslabs.be.commons.validation.config.domain.exceptions;

public class ValidationMetadataException extends RuntimeException{

    public ValidationMetadataException(String message) {
        super(message);
    }
}
