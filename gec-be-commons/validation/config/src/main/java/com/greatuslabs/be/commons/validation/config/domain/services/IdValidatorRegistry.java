package com.greatuslabs.be.commons.validation.config.domain.services;

import com.greatuslabs.be.commons.validation.api.validators.IdValidator;

public interface IdValidatorRegistry {
    IdValidator getValidator(String validatorName);
}
