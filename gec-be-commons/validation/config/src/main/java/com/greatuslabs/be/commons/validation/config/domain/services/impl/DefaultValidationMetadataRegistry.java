package com.greatuslabs.be.commons.validation.config.domain.services.impl;

import com.greatuslabs.be.commons.validation.api.dto.ValidationMetadata;
import com.greatuslabs.be.commons.validation.config.domain.services.ValidationMetadataMappingBuilder;
import com.greatuslabs.be.commons.validation.config.domain.services.ValidationMetadataRegistry;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class DefaultValidationMetadataRegistry implements ValidationMetadataRegistry {

    private final Map<Class<?>, List<ValidationMetadata>> queryObjectValidationMetadataRegistry;

    public DefaultValidationMetadataRegistry(ValidationMetadataMappingBuilder validationMetadataMappingBuilder) {
        this.queryObjectValidationMetadataRegistry = validationMetadataMappingBuilder.constructRegistry();
    }

    @Override
    public List<ValidationMetadata> getValidationMetadata(Class<?> queryResultObjectClass) {
        List<ValidationMetadata> validationMetadata = queryObjectValidationMetadataRegistry.get(queryResultObjectClass);
        return Objects.isNull(validationMetadata) ? new ArrayList<>() : validationMetadata;
    }
}
