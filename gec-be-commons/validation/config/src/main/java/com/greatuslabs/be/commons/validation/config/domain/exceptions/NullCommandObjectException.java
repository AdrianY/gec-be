package com.greatuslabs.be.commons.validation.config.domain.exceptions;

public class NullCommandObjectException extends RuntimeException{
    public NullCommandObjectException(String message) {
        super(message);
    }
}
