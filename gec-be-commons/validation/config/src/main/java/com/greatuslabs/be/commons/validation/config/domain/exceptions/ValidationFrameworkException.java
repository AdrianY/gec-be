package com.greatuslabs.be.commons.validation.config.domain.exceptions;

public class ValidationFrameworkException extends RuntimeException{
    public ValidationFrameworkException(String message) {
        super(message);
    }
}
