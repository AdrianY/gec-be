package com.greatuslabs.be.commons.validation.config.domain.services;


import com.greatuslabs.be.commons.validation.api.validators.CommandObjectValidator;

public interface CommandObjectValidatorRegistry {
    <COMMAND_OBJECT, VALIDATION_CLASS> CommandObjectValidator<COMMAND_OBJECT> retrieveValidator(
            Class<? extends COMMAND_OBJECT> commandObjectClass, Class<VALIDATION_CLASS> groupValidationClass);
}
