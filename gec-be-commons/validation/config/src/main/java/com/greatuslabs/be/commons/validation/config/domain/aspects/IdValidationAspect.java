package com.greatuslabs.be.commons.validation.config.domain.aspects;

import com.greatuslabs.be.commons.validation.api.annotations.IsValidId;
import com.greatuslabs.be.commons.validation.api.validators.IdValidator;
import com.greatuslabs.be.commons.validation.config.domain.exceptions.NoValidatorFoundException;
import com.greatuslabs.be.commons.validation.config.domain.exceptions.NullIdException;
import com.greatuslabs.be.commons.validation.config.domain.references.ValidatorType;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Aspect
public class IdValidationAspect {

    private final Map<String, IdValidator> idValidatorRegistry;

    public IdValidationAspect(Map<String, IdValidator> idValidatorRegistry) {
        this.idValidatorRegistry = Objects.isNull(idValidatorRegistry) ? new HashMap<>() : idValidatorRegistry;
    }

    @Before("execution(* *(.., @com.greatuslabs.be.commons.validation.api.annotations.IsValidId (*), ..))")
    public void validateId(JoinPoint jp) throws NoSuchMethodException {

        // Get the target method
        Method interfaceMethod = ((MethodSignature)jp.getSignature()).getMethod();
        Object target = jp.getTarget();
        Method implementationMethod =
                target.getClass().getMethod(interfaceMethod.getName(), interfaceMethod.getParameterTypes());

        // Get the annotated parameters and validate those with the @Valid annotation
        Annotation[][] annotationParameters = implementationMethod.getParameterAnnotations();
        for (int i = 0; i < annotationParameters.length; i++) {
            Annotation[] annotations = annotationParameters[i];
            for (Annotation annotation : annotations) {
                if (annotation.annotationType().equals(IsValidId.class)) {
                    IsValidId validationDetails = (IsValidId)annotation;
                    Object id = jp.getArgs()[i];
                    validateId(id, validationDetails);
                }
            }
        }
    }

    private void validateId(Object id, IsValidId validationDetails) {

        if(null == id){
            String idName = validationDetails.idName();
            String parameterName = StringUtils.isNotEmpty(idName) ? idName : "id";
            throw new NullIdException(parameterName, parameterName+" must not be null");
        }

        String validatorName = validationDetails.validator();
        if(StringUtils.isNotEmpty(validatorName)){
            IdValidator idValidator = idValidatorRegistry.get(validatorName);

            if(null == idValidator)
                throw new NoValidatorFoundException(validatorName, ValidatorType.ID_VALIDATOR);

            idValidator.validateEntityId(id, validationDetails);
        }


    }
}
