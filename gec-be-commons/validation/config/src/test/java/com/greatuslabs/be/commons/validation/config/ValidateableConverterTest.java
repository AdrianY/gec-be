package com.greatuslabs.be.commons.validation.config;


import com.greatuslabs.be.commons.validation.api.dto.Operation;
import com.greatuslabs.be.commons.validation.api.dto.ValidateableModel;
import com.greatuslabs.be.commons.validation.api.dto.ValidationMetadata;
import com.greatuslabs.be.commons.validation.api.services.ValidateableConverter;
import com.greatuslabs.be.commons.validation.config.domain.services.ValidationMetadataRegistry;
import com.greatuslabs.be.commons.validation.config.domain.services.impl.ValidateableConverterImpl;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import com.greatuslabs.be.commons.validation.config.mocks.TestConfig;

import java.util.Arrays;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ValidateableConverterTest {

    @MockBean
    private ValidationMetadataRegistry validationMetadataRegistry;

    private ValidateableConverter validateableConverter;

    @Before
    public void init(){
        validateableConverter = new ValidateableConverterImpl(validationMetadataRegistry);
    }

    @Test
    public void test01_shouldConvertSampleQRO_whenThatQROMetadataIsRegistered(){

        when(validationMetadataRegistry.getValidationMetadata(eq(TestConfig.MockQueryResultObject.class)))
            .thenReturn(
                    Arrays.asList(
                            new ValidationMetadata(
                                    "property",
                                    Arrays.asList(
                                            mockOperation("required", "required", null),
                                            mockOperation("in", "in", Arrays.asList("one","two"))
                                    )
                            )
                    )
            );

        ValidateableModel<TestConfig.MockQueryResultObject> qro =
                validateableConverter.toValidateable(new TestConfig.MockQueryResultObject("hey"));

        assertNotNull(qro);
        assertThat(
                qro.getQueryResultObject(),
                allOf(
                        instanceOf(TestConfig.MockQueryResultObject.class),
                        hasProperty("property", equalTo("hey"))
                )
        );
        assertThat(
                qro.getValidationMetadata(),
                hasEntry(
                        equalTo("property"),
                        allOf(
                                hasItem(allOf(
                                        instanceOf(Operation.class),
                                        hasProperty("name", equalTo("required")),
                                        hasProperty("value", nullValue())
                                )),
                                hasItem(allOf(
                                        instanceOf(Operation.class),
                                        hasProperty("name", equalTo("in")),
                                        hasProperty("value",
                                                containsInAnyOrder("one", "two"))
                                ))
                        )
                )
        );
    }

    private <VALUE> Operation<VALUE> mockOperation(String name, String codeName, VALUE value){
        return new Operation<>(name, codeName, value);
    }
}
