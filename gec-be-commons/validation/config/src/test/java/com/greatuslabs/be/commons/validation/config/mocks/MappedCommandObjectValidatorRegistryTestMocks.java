package com.greatuslabs.be.commons.validation.config.mocks;

import javax.validation.constraints.NotNull;

public class MappedCommandObjectValidatorRegistryTestMocks {
    public static class CommandObjectOne{
        @NotNull(message = "cannot be null")
        private String property;

        public CommandObjectOne(String property) {
            this.property = property;
        }

        public String getProperty() {
            return property;
        }
    }

    public static class CommandObjectTwo{
        @NotNull(message = "cannot be null")
        private String property;

        public CommandObjectTwo(String property) {
            this.property = property;
        }

        public String getProperty() {
            return property;
        }
    }

    public static class Create{}

    public static class Update{}
}
