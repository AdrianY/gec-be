package com.greatuslabs.be.commons.validation.config.mocks;

public class ValidationMapperFactoryTestMocks {
    public static class QueryObjectOne{
        private String property;

        public QueryObjectOne(String property) {
            this.property = property;
        }

        public String getProperty() {
            return property;
        }
    }

    public static class QueryObjectTwo{
        private String name;
        private String address;

        public QueryObjectTwo(String name, String address) {
            this.name = name;
            this.address = address;
        }

        public String getName() {
            return name;
        }

        public String getAddress() {
            return address;
        }
    }
}
