package com.greatuslabs.be.commons.validation.config;

import com.greatuslabs.be.commons.validation.api.dto.Operation;
import com.greatuslabs.be.commons.validation.api.dto.ValidationMetadata;
import com.greatuslabs.be.commons.validation.config.domain.services.ValidationMetadataMappingBuilder;
import com.greatuslabs.be.commons.validation.config.domain.services.ValidationMetadataRegistry;
import com.greatuslabs.be.commons.validation.config.domain.services.impl.DefaultValidationMetadataRegistry;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import com.greatuslabs.be.commons.validation.config.mocks.TestConfig;

import java.util.*;

import static junit.framework.TestCase.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DefaultValidationMetadataRegistryTest {

    @MockBean
    private ValidationMetadataMappingBuilder validationMetadataMappingBuilder;

    private ValidationMetadataRegistry validationMetadataRegistry;

    @Before
    public void init(){
        Map<Class<?>, List<ValidationMetadata>> mockMap = new HashMap<>();
        mockMap.put(TestConfig.MockQueryResultObject.class, Arrays.asList(
                new ValidationMetadata(
                        "property", Arrays.asList(mockOperation("required", "required", null)))));

        when(validationMetadataMappingBuilder.constructRegistry())
                .thenReturn(mockMap);
        validationMetadataRegistry = new DefaultValidationMetadataRegistry(validationMetadataMappingBuilder);
    }

    @Test
    public void test01_shouldBeAbleToRetrieveValidationMetadata_forRegisteredClass(){
        List<ValidationMetadata> validationMetadata =
                validationMetadataRegistry.getValidationMetadata(TestConfig.MockQueryResultObject.class);

        assertNotNull(validationMetadata);
        assertFalse(validationMetadata.isEmpty());
        assertEquals(1, validationMetadata.size());
        assertThat(
                validationMetadata,
                hasItem(
                        allOf(
                                instanceOf(ValidationMetadata.class),
                                hasProperty("fieldName", equalTo("property")),
                                hasProperty(
                                        "validations",
                                        hasItem(
                                                allOf(
                                                        hasProperty("name", equalTo("required")),
                                                        hasProperty("value", nullValue())
                                                )
                                        )
                                )
                        )
                )
        );

        verify(validationMetadataMappingBuilder, times(1)).constructRegistry();
    }

    @Test
    public void test02_returnEmptyValidationMetadataList_forNoneRegisteredClass(){

        List<ValidationMetadata> validationMetadata =
                validationMetadataRegistry.getValidationMetadata(TestConfig.MockPojo.class);

        assertNotNull(validationMetadata);
        assertTrue(validationMetadata.isEmpty());

        verify(validationMetadataMappingBuilder, times(1)).constructRegistry();
    }

    private <VALUE> Operation<?> mockOperation(String name, String codeName, VALUE value){
        if(Objects.isNull(value))
            return new Operation<Void>(name, codeName, null);

        return new Operation<>(name, codeName, value);
    }
}
