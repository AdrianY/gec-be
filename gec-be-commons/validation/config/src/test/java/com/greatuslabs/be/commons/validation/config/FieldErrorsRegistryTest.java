package com.greatuslabs.be.commons.validation.config;

import com.greatuslabs.be.commons.validation.api.dto.FieldError;
import com.greatuslabs.be.commons.validation.api.exceptions.CommandObjectValidationException;
import com.greatuslabs.be.commons.validation.api.util.FieldErrorsRegistry;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.googlecode.catchexception.CatchException.*;
import static com.googlecode.catchexception.apis.CatchExceptionHamcrestMatchers.hasMessage;
import static junit.framework.TestCase.assertNull;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FieldErrorsRegistryTest {

    @Test
    public void test01_staticMethod_processFieldErrors_shouldThrowCommandValidationException_whenFieldErrorsIsNotEmpty(){

        List<FieldError> fieldErrorList =
                Arrays.asList(
                        new FieldError("field1", "must be greater than 2", 2),
                        new FieldError("field2", "must be equal to", "GL")
                );

        Runnable runnable = () -> FieldErrorsRegistry.processFieldErrors(
                1L, Object.class, fieldErrorList);

        catchException(runnable).run();

        assertThat(
                caughtException(),
                allOf(
                        not(nullValue()),
                        instanceOf(CommandObjectValidationException.class),
                        hasMessage("Command object, 1 validated under validation class "+
                                Object.class.getName()+ ", has validation errors"),
                        hasProperty(
                                "fieldErrors",
                                allOf(
                                        hasItem(
                                                allOf(
                                                        hasProperty("field", equalTo("field1")),
                                                        hasProperty("message", equalTo("must be greater than 2")),
                                                        hasProperty("rejectedValue", equalTo(2))
                                                )
                                        ),
                                        hasItem(
                                                allOf(
                                                        hasProperty("field", equalTo("field2")),
                                                        hasProperty("message", equalTo("must be equal to")),
                                                        hasProperty("rejectedValue", equalTo("GL"))
                                                )
                                        )
                                )
                        ),
                        hasProperty("groupValidationClass", equalTo(Object.class))
                )
        );

    }

    @Test
    public void test02_staticMethod_processFieldErrors_shouldNotThrowCommandValidationException_whenFieldErrorsIsEmpty(){

        Runnable runnable = () -> FieldErrorsRegistry.processFieldErrors(
                1L, Object.class, null);
        catchException(runnable).run();
        assertNull(caughtException());

        resetCaughtException();

        Runnable runnableWithEmptyFieldList = () -> FieldErrorsRegistry.processFieldErrors(
                2L, Object.class, new ArrayList<>());
        catchException(runnableWithEmptyFieldList).run();
        assertNull(caughtException());
    }

    @Test
    public void test03_staticMethod_createInstance_shouldReturnFieldErrorsRegistryInstance(){

        FieldErrorsRegistry fieldErrorsRegistry = FieldErrorsRegistry.createInstance(
                "GL", Object.class);

        assertNotNull(fieldErrorsRegistry);
    }

    @Test
    public void test04_instanceMethod_registerFieldError_shouldThrowCommandObjectValidationException(){

        FieldErrorsRegistry fieldErrorsRegistry = FieldErrorsRegistry.createInstance(
                "GL", Object.class);

        fieldErrorsRegistry.registerFieldError(new FieldError(
                "field1", "must be greater than 2", 2));

        Runnable runnable = fieldErrorsRegistry::processFieldErrors;

        catchException(runnable).run();

        assertThat(
                caughtException(),
                allOf(
                        not(nullValue()),
                        instanceOf(CommandObjectValidationException.class),
                        hasMessage("Command object, GL validated under validation class "+
                                Object.class.getName()+ ", has validation errors"),
                        hasProperty(
                                "fieldErrors",
                                hasItem(
                                        allOf(
                                                hasProperty("field", equalTo("field1")),
                                                hasProperty("message", equalTo("must be greater than 2")),
                                                hasProperty("rejectedValue", equalTo(2))
                                        )
                                )
                        ),
                        hasProperty("groupValidationClass", equalTo(Object.class))
                )
        );


    }

    @Test
    public void test05_instanceMethod_registerFieldErrors_shouldThrowCommandObjectValidationException(){

        FieldErrorsRegistry fieldErrorsRegistry = FieldErrorsRegistry.createInstance(
                "GL", Object.class);

        fieldErrorsRegistry.registerFieldErrors(
                new FieldError("field1", "must be greater than 2", 2),
                new FieldError("field2", "must be equal to", "Empowerment")
        );

        Runnable runnable = fieldErrorsRegistry::processFieldErrors;

        catchException(runnable).run();

        assertThat(
                caughtException(),
                allOf(
                        not(nullValue()),
                        instanceOf(CommandObjectValidationException.class),
                        hasMessage("Command object, GL validated under validation class "+
                                Object.class.getName()+ ", has validation errors"),
                        hasProperty(
                                "fieldErrors",
                                allOf(
                                        hasItem(
                                                allOf(
                                                        hasProperty("field", equalTo("field2")),
                                                        hasProperty("message", equalTo("must be equal to")),
                                                        hasProperty("rejectedValue", equalTo("Empowerment"))
                                                )
                                        ),
                                        hasItem(
                                                allOf(
                                                        hasProperty("field", equalTo("field1")),
                                                        hasProperty("message", equalTo("must be greater than 2")),
                                                        hasProperty("rejectedValue", equalTo(2))
                                                )
                                        )
                                )
                        ),
                        hasProperty("groupValidationClass", equalTo(Object.class))
                )
        );


    }
}
