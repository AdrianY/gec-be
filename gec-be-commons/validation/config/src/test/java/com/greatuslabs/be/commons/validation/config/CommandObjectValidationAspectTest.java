package com.greatuslabs.be.commons.validation.config;

import com.greatuslabs.be.commons.validation.api.dto.FieldError;
import com.greatuslabs.be.commons.validation.api.exceptions.CommandObjectValidationException;
import com.greatuslabs.be.commons.validation.api.validators.CommandObjectValidationErrorCollector;
import com.greatuslabs.be.commons.validation.api.validators.CommandObjectValidator;
import com.greatuslabs.be.commons.validation.config.domain.aspects.CommandObjectValidationAspect;
import com.greatuslabs.be.commons.validation.config.domain.exceptions.NullCommandObjectException;
import com.greatuslabs.be.commons.validation.config.domain.services.CommandObjectValidatorRegistry;
import org.aspectj.lang.JoinPoint;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Matchers;
import org.springframework.aop.aspectj.annotation.AspectJProxyFactory;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.googlecode.catchexception.CatchException.*;
import static com.googlecode.catchexception.apis.CatchExceptionHamcrestMatchers.hasMessage;
import static com.googlecode.catchexception.apis.CatchExceptionHamcrestMatchers.hasMessageThat;
import static com.greatuslabs.be.commons.validation.config.mocks.CommandObjectValidationAspectTestMocks.*;
import static com.greatuslabs.be.commons.validation.config.util.RegexMatcher.matchesPattern;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CommandObjectValidationAspectTest {

    @MockBean
    private CommandObjectValidatorRegistry commandObjectValidatorRegistry;

    @MockBean
    private CommandObjectValidationErrorCollector commandObjectValidationErrorCollector;

    @MockBean
    private CommandObjectValidator<CommandObject> commandObjectValidator;

    private CommandObjectValidationAspect aspectUnderTest;

    private Component mockComponent;

    @Captor
    private ArgumentCaptor<CommandObject> mockCommandObjectArgumentCaptor;

    @Captor
    private ArgumentCaptor<List<FieldError>> mockFieldErrorsArgumentCaptor;

    @Captor
    private ArgumentCaptor<Class<?>> groupValidationClassArgCaptor;

    @Before
    public void init(){
        Component rawMockComponent =
                new Component();
        AspectJProxyFactory proxyFactory = new AspectJProxyFactory(rawMockComponent);
        CommandObjectValidationAspect rawCommandObjectValidationAspect =
                new CommandObjectValidationAspect(
                        commandObjectValidatorRegistry, commandObjectValidationErrorCollector);
        aspectUnderTest = spy(rawCommandObjectValidationAspect);
        proxyFactory.addAspect(aspectUnderTest);

        mockComponent = proxyFactory.getProxy();
    }

    @Test
    public void test01_shouldThrowCommandObjectValidationException_whenCommandObjectHasInvalidValue() throws Exception {
        assertCommandObjectValidatorAspect_WillThrowExceptionFromDefaultValidator_WhenValidatorGroupClassIsNotSpecifiedAndCOHasProblems(() ->
                mockComponent.consume(new CommandObject(null)));
    }

    @Test
    public void test02_shouldThrowCommandObjectValidationException_whenNoneFirstParameterIsValidCOHasInvalidValue() throws Exception {
        assertCommandObjectValidatorAspect_WillThrowExceptionFromDefaultValidator_WhenValidatorGroupClassIsNotSpecifiedAndCOHasProblems(() ->
                mockComponent.consume("Sample Id", new CommandObject(null))
        );
    }

    @Test
    public void test03_shouldThrowCommandObjectValidationException_whenFirstParameterIsValidCOHasInvalidValue() throws Exception {
        assertCommandObjectValidatorAspect_WillThrowExceptionFromDefaultValidator_WhenValidatorGroupClassIsNotSpecifiedAndCOHasProblems(() ->
                mockComponent.consume(new CommandObject(null), "Sample Id")
        );
    }

    @Test
    public void test04_invokeCustomValidator_whenValidatorIsAvailable() throws Exception{

        assertCommandObjectValidatorAspect_WillCallCustomValidator_WhenValidatorGroupClassIsSpecified(() ->
                mockComponent.consumeWithValidator(
                        new CommandObject("Hey"))
        );
    }

    @Test
    public void test05_invokeCustomValidator_forFirstParameterCO_whenValidatorIsAvailable() throws Exception{

        assertCommandObjectValidatorAspect_WillCallCustomValidator_WhenValidatorGroupClassIsSpecified(() ->
                mockComponent.consumeWithValidator(
                        new CommandObject("Hey"), "Sample Id")
        );
    }

    @Test
    public void test06_invokeCustomValidator_forNoneFirstParameterCO_whenValidatorIsAvailable() throws Exception{

        assertCommandObjectValidatorAspect_WillCallCustomValidator_WhenValidatorGroupClassIsSpecified(() ->
                mockComponent.consumeWithValidator(
                        "Sample Id", new CommandObject("Hey"))
        );
    }

    @Test
    public void test07_shouldThrowNullCommandObjectException_forNullCO() throws Exception{
        resetCaughtException();
        Runnable runnable = () ->  mockComponent.consumeWithValidator( null);
        catchException(runnable).run();
        assertThat(caughtException(),
                allOf(
                        not(nullValue()),
                        instanceOf(NullCommandObjectException.class),
                        hasMessage("Command Object to be validated under validation class "+
                                IdValidationClass.class.getName()+
                                ", cannot be null")
                )
        );
    }

    private void assertCommandObjectValidatorAspect_WillThrowExceptionFromDefaultValidator_WhenValidatorGroupClassIsNotSpecifiedAndCOHasProblems(
            Runnable testTrigger) throws Exception {

        when(commandObjectValidatorRegistry
                .retrieveValidator(
                        eq(CommandObject.class),
                        eq(Void.class)
                )
        ).thenReturn(null);

        when(commandObjectValidationErrorCollector.collectErrors(
                Matchers.isA(CommandObject.class),
                eq(Void.class)
        )).thenReturn(newArrayList(new FieldError("property", "cannot be null", null)));

        catchException(testTrigger).run();

        assertThat(caughtException(),
                allOf(
                        instanceOf(CommandObjectValidationException.class),
                        hasMessageThat(matchesPattern("Command object, [\\.\\$A-Za-z\\@\\$0-9]{1,} " +
                                "validated under validation class " +
                                Void.class.getName() + ", has validation errors")),
                        hasProperty("groupValidationClass", equalTo(Void.class)),
                        hasProperty(
                                "fieldErrors",
                                hasItem(allOf(
                                        hasProperty("field", equalTo("property")),
                                        hasProperty("message", equalTo("cannot be null")),
                                        hasProperty("rejectedValue", nullValue())
                                )))
                )
        );
    }

    private void assertCommandObjectValidatorAspect_WillCallCustomValidator_WhenValidatorGroupClassIsSpecified(
            Runnable testTrigger) throws NoSuchMethodException {

        when(commandObjectValidatorRegistry
                .retrieveValidator(
                        eq(CommandObject.class),
                        eq(IdValidationClass.class)
                )
        ).thenReturn(commandObjectValidator);

        testTrigger.run();

        verify(commandObjectValidationErrorCollector, never())
                .collectErrors(Matchers.any(CommandObject.class), eq(Void.class));

        verify(aspectUnderTest, times(1))
                .validateCommand(Matchers.any(JoinPoint.class));

        verify(commandObjectValidator, times(1))
                .validateCommand(mockCommandObjectArgumentCaptor.capture(), eq(true));

        CommandObject commandObject = mockCommandObjectArgumentCaptor.getValue();
        assertNotNull(commandObject);
        assertEquals("Hey", commandObject.getProperty());
    }


    public static <T> List<T> newArrayList(T... elements) {
        List<T> list = new ArrayList<>();
        Collections.addAll(list, elements);
        return list;
    }
}
