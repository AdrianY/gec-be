package com.greatuslabs.be.commons.validation.config;

import com.greatuslabs.be.commons.validation.api.util.FieldErrorsRegistry;
import com.greatuslabs.be.commons.validation.api.validators.CommandObjectValidationErrorCollector;
import com.greatuslabs.be.commons.validation.api.validators.FieldByFieldCommandObjectValidator;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import static com.greatuslabs.be.commons.validation.config.mocks.FieldByFieldCommandObjectValidatorTestMocks.*;

import java.util.ArrayList;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FieldByFieldCommandObjectValidatorTest {

    @MockBean
    private CommandObjectValidationErrorCollector commandObjectValidationErrorCollector;

    private FieldByFieldCommandObjectValidator validatorUnderTest;

    @Before
    public void init(){
        when(commandObjectValidationErrorCollector.collectErrors(
                any(CommandObjectOne.class), eq(ValidationClass.class)))
                .thenReturn(new ArrayList<>());

        validatorUnderTest = spy(new FieldByFieldCommandObjectValidator(commandObjectValidationErrorCollector) {
            @Override
            protected void validateCommand(FieldErrorsRegistry fieldErrorsRegistry, Object o) {

            }

            @Override
            protected Class<?> getGroupValidationClass() {
                return ValidationClass.class;
            }
        });

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test01_shouldCallCommandObjectValidationErrorCollector_whenInFullValidationMode(){
        validatorUnderTest.validateCommand(new CommandObjectOne(null), true);

        verify(commandObjectValidationErrorCollector, times(1))
                .collectErrors(any(CommandObjectOne.class), eq(ValidationClass.class));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test02_shouldNotCallCommandObjectValidationErrorCollector_whenNotInFullValidationMode(){

        validatorUnderTest.validateCommand(new CommandObjectOne(null), false);

        verify(commandObjectValidationErrorCollector, never())
                .collectErrors(any(CommandObjectOne.class), eq(ValidationClass.class));
    }
}
