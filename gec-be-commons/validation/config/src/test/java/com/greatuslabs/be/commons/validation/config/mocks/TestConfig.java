package com.greatuslabs.be.commons.validation.config.mocks;

import com.greatuslabs.be.commons.validation.config.CoreValidationConfig;
import com.greatuslabs.be.commons.validation.api.annotations.IsValidId;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@SpringBootApplication
@Import(CoreValidationConfig.class)
@Deprecated
public class TestConfig {

    @Component("mockComponent")
    public static class MockComponent {

        public void checkId(
                @IsValidId(
                        idName = "parameterName",
                        validator = "idValidator",
                        groupValidationClass = SampleIdValidationClass.class
                ) String anotherValue
        ){

        }

        public void checkIdNoCustomValidator(
                @IsValidId(
                        idName = "parameterName",
                        groupValidationClass = SampleIdValidationClass.class
                ) String anotherValue
        ){

        }

        public void consume(
                @IsValidId(
                        idName = "parameterName",
                        validator = "idValidator",
                        groupValidationClass = SampleIdValidationClass.class
                ) String anotherValue,
                MockPojo mockPojo
        ){

        }

        public void consume(
                MockPojo mockPojo,
                @IsValidId(
                        idName = "parameterName",
                        validator = "idValidator",
                        groupValidationClass = SampleIdValidationClass.class
                ) String anotherValue
        ){

        }

    }

    public static class MockPojo {

        @NotNull(message = "cannot be null")
        private String property;

        public MockPojo(String property) {
            this.property = property;
        }

        public String getProperty() {
            return property;
        }


    }

    public static class SampleIdValidationClass{}


    public static class MockQueryResultObject{
        private String property;
        public  MockQueryResultObject(String property){
            this.property = property;
        }
        public String getProperty() {
            return property;
        }
    }

}
