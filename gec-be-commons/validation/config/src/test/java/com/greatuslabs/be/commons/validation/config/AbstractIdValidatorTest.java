package com.greatuslabs.be.commons.validation.config;

import com.greatuslabs.be.commons.validation.api.annotations.IsValidId;
import com.greatuslabs.be.commons.validation.api.exceptions.NoIdValidatorMethodException;
import com.greatuslabs.be.commons.validation.config.mocks.TestConfig;
import com.greatuslabs.be.commons.validation.api.validators.AbstractIdValidator;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static com.googlecode.catchexception.apis.CatchExceptionHamcrestMatchers.hasMessage;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AbstractIdValidatorTest {

    @Test
    public void test01_shouldThrowNoIdValidatorMethodException_whenNoMatchingValidatorIsFound(){
        IsValidId mockValidationDetails =
                mockIsValidIdAnnotation(TestConfig.SampleIdValidationClass.class);

        catchException(new AbstractIdValidator<String>() {

            @Override
            protected void registerMethods(Map<Class<?>, Consumer<String>> validatorMethodsRegistry) {

            }
        }).validateEntityId("hey", mockValidationDetails);

        assertThat(
                caughtException(),
                allOf(
                        instanceOf(NoIdValidatorMethodException.class),
                        hasProperty("validatorName", equalTo("abstractIdValidator")),
                        hasProperty("validationClass", equalTo(TestConfig.SampleIdValidationClass.class)),
                        hasMessage("No id com.greatuslabs.be.commons.validation handler found in " +
                                "IdValidator: abstractIdValidator for com.greatuslabs.be.commons.validation group class "+
                                TestConfig.SampleIdValidationClass.class.getName())
                )
        );
    }

    @SuppressWarnings("Convert2Lambda")
    @Test
    public void test02_shouldInvokeRegisteredValidationMethod_whenMatchingValidatorIsFound(){

        Consumer<String> validationMethod = new Consumer<String>() {
            @Override
            public void accept(String s) {

            }
        };
        Consumer<String> spiedValidationMethod = spy(validationMethod);

        Map<Class<?>, Consumer<String>> methodRegistry = new HashMap<>();
        methodRegistry.put(TestConfig.SampleIdValidationClass.class, spiedValidationMethod);

        Map<Class<?>, Consumer<String>> spiedMethodRegistry = spy(methodRegistry);

        IsValidId mockValidationDetails =
                mockIsValidIdAnnotation(TestConfig.SampleIdValidationClass.class);

        AbstractIdValidator<String> abstractIdValidator = new AbstractIdValidator<String>(spiedMethodRegistry) {

            @Override
            protected void registerMethods(Map<Class<?>, Consumer<String>> validatorMethodsRegistry) {

            }
        };

        //trigger test
        abstractIdValidator.validateEntityId("hey", mockValidationDetails);


        //perform assertions
        verify(spiedMethodRegistry, times(1))
                .get(eq(TestConfig.SampleIdValidationClass.class));

        verify(spiedValidationMethod, times(1))
                .accept(eq("hey"));
    }


    private static IsValidId mockIsValidIdAnnotation(Class<?> groupValidationClass){
        return new IsValidId(){

            @Override
            public Class<? extends Annotation> annotationType() {
                return IsValidId.class;
            }

            @Override
            public Class<?> groupValidationClass() {
                return groupValidationClass;
            }

            @Override
            public String validator() {
                return "abstractIdValidator";
            }

            @Override
            public String idName() {
                return "dish id";
            }
        };
    }
}
