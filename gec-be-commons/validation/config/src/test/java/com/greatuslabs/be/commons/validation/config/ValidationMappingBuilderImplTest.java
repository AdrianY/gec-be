package com.greatuslabs.be.commons.validation.config;

import com.greatuslabs.be.commons.validation.api.builders.ValidationOperationBuilder;
import com.greatuslabs.be.commons.validation.api.dto.Operation;
import com.greatuslabs.be.commons.validation.api.dto.OperationValidationResult;
import com.greatuslabs.be.commons.validation.api.dto.ValidationMetadata;
import com.greatuslabs.be.commons.validation.api.util.CoreValidationOperationBuilders;
import com.greatuslabs.be.commons.validation.config.domain.exceptions.NotExistingValidateablePropertyException;
import com.greatuslabs.be.commons.validation.config.domain.exceptions.ValidationMetadataException;
import com.greatuslabs.be.commons.validation.config.domain.services.impl.ValidationMappingBuilderImpl;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Map;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static com.googlecode.catchexception.apis.CatchExceptionHamcrestMatchers.hasMessage;
import static com.greatuslabs.be.commons.validation.config.mocks.ValidationMapperFactoryTestMocks.QueryObjectOne;
import static com.greatuslabs.be.commons.validation.config.mocks.ValidationMapperFactoryTestMocks.QueryObjectTwo;
import static junit.framework.TestCase.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ValidationMappingBuilderImplTest {

    private ValidationMappingBuilderImpl validationMapperFactoryUnderTest;

    @Before
    public void init(){
        validationMapperFactoryUnderTest = new ValidationMappingBuilderImpl();
    }

    @Test
    public void test01_shouldBeAbleToConstructValidationMetadata(){

        validationMapperFactoryUnderTest.mapClass(QueryObjectTwo.class)
                .field(b -> {
                    b.setFieldName("name");
                    b.validations(
                            CoreValidationOperationBuilders.notEqualTo("hey"),
                            CoreValidationOperationBuilders.equalTo("hey")
                    );
                })
                .field(b -> {
                    b.setFieldName("address");
                    b.validations(
                            CoreValidationOperationBuilders.equalTo("yup")
                    );
                })
                .register();


        Map<Class<?>, List<ValidationMetadata>> validationRegistryMap =
                validationMapperFactoryUnderTest.constructRegistry();

        assertNotNull(validationRegistryMap);
        assertFalse(validationRegistryMap.isEmpty());
        assertEquals(1, validationRegistryMap.size());
        assertThat(
                validationRegistryMap,
                hasEntry(
                        equalTo(QueryObjectTwo.class),
                        hasItems(
                                allOf(
                                        instanceOf(ValidationMetadata.class),
                                        hasProperty("fieldName", equalTo("name")),
                                        hasProperty(
                                                "validations",
                                                hasItems(
                                                        allOf(
                                                                instanceOf(Operation.class),
                                                                hasProperty("name", equalTo("equal")),
                                                                hasProperty("value", equalTo("hey")),
                                                                hasProperty("codeName", equalTo("=="))
                                                        ),
                                                        allOf(
                                                                instanceOf(Operation.class),
                                                                hasProperty("name", equalTo("not equal")),
                                                                hasProperty("value", equalTo("hey")),
                                                                hasProperty("codeName", equalTo("!="))
                                                        )
                                                )
                                        )
                                ),
                                allOf(
                                        instanceOf(ValidationMetadata.class),
                                        hasProperty("fieldName", equalTo("address")),
                                        hasProperty(
                                                "validations",
                                                hasItem(allOf(
                                                        instanceOf(Operation.class),
                                                        hasProperty("name", equalTo("equal")),
                                                        hasProperty("value", equalTo("yup")),
                                                        hasProperty("codeName", equalTo("=="))
                                                ))
                                        )
                                )
                        )
                )
        );

    }

    @Test
    public void test02_shouldThrowExceptionWhenFieldIsNotExistingInTargetClass(){

        Runnable runnable = () -> validationMapperFactoryUnderTest.mapClass(QueryObjectOne.class)
                .field(b -> {
                    b.setFieldName("name");
                    b.validations(
                            CoreValidationOperationBuilders.notEqualTo("hey"),
                            CoreValidationOperationBuilders.equalTo("hey")
                    );
                })
                .register();

        catchException(runnable).run();

        assertThat(
                caughtException(),
                allOf(
                        instanceOf(NotExistingValidateablePropertyException.class),
                        hasMessage("Validation metadata setup failed. " +
                                "Field 'name' doesn't exist for class " +
                                QueryObjectOne.class.getName())
                )
        );
    }

    @Test
    public void test03_shouldThrowExceptionWhenFieldNameIsNull(){
        Runnable runnable = () -> validationMapperFactoryUnderTest.mapClass(QueryObjectOne.class)
                .field(b -> b.validations(
                        CoreValidationOperationBuilders.notEqualTo("hey"),
                        CoreValidationOperationBuilders.equalTo("hey")
                ))
                .register();

        catchException(runnable).run();

        assertThat(
                caughtException(),
                allOf(
                        instanceOf(NotExistingValidateablePropertyException.class),
                        hasMessage("Validation metadata setup failed. " +
                                "Cannot map null or empty field for class " +
                                QueryObjectOne.class.getName())
                )
        );
    }

    @Test
    public void test04_shouldThrowExceptionWhenFieldNameIsEmpty(){
        Runnable runnable = () -> validationMapperFactoryUnderTest.mapClass(QueryObjectOne.class)
                .field(b -> {
                    b.setFieldName("");
                    b.validations(
                            CoreValidationOperationBuilders.notEqualTo("hey"),
                            CoreValidationOperationBuilders.equalTo("hey")
                    );
                })
                .register();

        catchException(runnable).run();

        assertThat(
                caughtException(),
                allOf(
                        instanceOf(NotExistingValidateablePropertyException.class),
                        hasMessage("Validation metadata setup failed. " +
                                "Cannot map null or empty field for class " +
                                QueryObjectOne.class.getName())
                )
        );
    }

    @Test
    public void test05_shouldThrowValidationMetadataException_whenValidationIsInvalid(){

        OperationValidationResult operationValidationResult = mock(OperationValidationResult.class);
        when(operationValidationResult.getReason()).thenReturn("Is Invalid");
        when(operationValidationResult.isSuccessful()).thenReturn(false);

        ValidationOperationBuilder validationOperationBuilder = mock(ValidationOperationBuilder.class);
        when(validationOperationBuilder.build()).thenReturn(new Operation<>("","",""));
        when(validationOperationBuilder.validate()).thenReturn(operationValidationResult);

        Runnable runnable = () -> validationMapperFactoryUnderTest.mapClass(QueryObjectOne.class)
                .field(b -> {
                    b.setFieldName("property");
                    b.validations(validationOperationBuilder);
                })
                .register();

        catchException(runnable).run();

        assertThat(
                caughtException(),
                allOf(
                        instanceOf(ValidationMetadataException.class),
                        hasMessage("Validation metadata setup failed. " +
                                "Validation for field 'property' for class " +
                                QueryObjectOne.class.getName() + " is invalid. Reason is: Is Invalid")
                )
        );

        verify(validationOperationBuilder, never()).build();
        verify(validationOperationBuilder, times(1)).validate();
    }


}
