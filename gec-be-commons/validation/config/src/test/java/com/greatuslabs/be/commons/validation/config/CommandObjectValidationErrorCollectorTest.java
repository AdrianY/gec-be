package com.greatuslabs.be.commons.validation.config;


import com.greatuslabs.be.commons.validation.api.dto.FieldError;
import com.greatuslabs.be.commons.validation.api.validators.CommandObjectValidationErrorCollector;
import com.greatuslabs.be.commons.validation.config.domain.validators.impl.CommandObjectValidationErrorCollectorImpl;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.greatuslabs.be.commons.validation.config.mocks.TestConfig;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CommandObjectValidationErrorCollectorTest {

    private CommandObjectValidationErrorCollector commandObjectValidationErrorCollector;

    @Before
    public void init(){
        commandObjectValidationErrorCollector = new CommandObjectValidationErrorCollectorImpl();
    }

    @Test
    public void test1_shouldValidatePojo_asPerJSR303(){
        List<FieldError> fieldErrors =
                commandObjectValidationErrorCollector.collectErrors(new TestConfig.MockPojo(null), Void.class);

        assertNotNull(fieldErrors);
        assertEquals(1, fieldErrors.size());
        FieldError fieldError = fieldErrors.get(0);
        assertEquals("property", fieldError.getField());
        assertEquals("cannot be null", fieldError.getMessage());
        assertNull(fieldError.getRejectedValue());
    }
}
