package com.greatuslabs.be.commons.validation.config;

import com.greatuslabs.be.commons.validation.api.util.FieldErrorsRegistry;
import com.greatuslabs.be.commons.validation.api.validators.CommandObjectValidator;
import com.greatuslabs.be.commons.validation.api.validators.MappedCommandObjectValidator;
import com.greatuslabs.be.commons.validation.config.domain.exceptions.ValidationFrameworkException;
import com.greatuslabs.be.commons.validation.config.domain.services.CommandObjectValidatorRegistry;
import com.greatuslabs.be.commons.validation.config.domain.services.impl.MappedCommandObjectValidatorRegistryImpl;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.test.context.junit4.SpringRunner;
import com.greatuslabs.be.commons.validation.config.mocks.MappedCommandObjectValidatorRegistryTestMocks;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static com.googlecode.catchexception.apis.CatchExceptionHamcrestMatchers.hasMessage;
import static com.greatuslabs.be.commons.validation.config.mocks.MappedCommandObjectValidatorRegistryTestMocks.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MappedCommandObjectValidatorRegistryTest {

    private MappedCommandObjectValidator<CommandObjectOne,Create> commandObjectValidatorOne;

    private MappedCommandObjectValidator<MappedCommandObjectValidatorRegistryTestMocks.CommandObjectTwo, MappedCommandObjectValidatorRegistryTestMocks.Create> commandObjectValidatorTwo;

    private MappedCommandObjectValidator<MappedCommandObjectValidatorRegistryTestMocks.CommandObjectTwo, MappedCommandObjectValidatorRegistryTestMocks.Update> commandObjectValidatorThree;

    private CommandObjectValidatorRegistry commandObjectValidatorRegistry;

    @Before
    public void init(){
        commandObjectValidatorOne = mockMappedCommandObjectValidator(MappedCommandObjectValidatorRegistryTestMocks.CommandObjectOne.class, MappedCommandObjectValidatorRegistryTestMocks.Create.class);
        commandObjectValidatorTwo = mockMappedCommandObjectValidator(MappedCommandObjectValidatorRegistryTestMocks.CommandObjectTwo.class, MappedCommandObjectValidatorRegistryTestMocks.Create.class);
        commandObjectValidatorThree = mockMappedCommandObjectValidator(MappedCommandObjectValidatorRegistryTestMocks.CommandObjectTwo.class, MappedCommandObjectValidatorRegistryTestMocks.Update.class);
    }

    @Test
    public void test01_shouldBeAbleToReturnAppropriateCOValidator_givenCOClassAndGroupValidationClass(){
        commandObjectValidatorRegistry =
                createValidatorRegistryUnderTest(
                        commandObjectValidatorOne, commandObjectValidatorTwo, commandObjectValidatorThree);

        CommandObjectValidator<CommandObjectOne> coValidatorOne =
                commandObjectValidatorRegistry.retrieveValidator(MappedCommandObjectValidatorRegistryTestMocks.CommandObjectOne.class, MappedCommandObjectValidatorRegistryTestMocks.Create.class);

        assertThat(coValidatorOne, equalTo(commandObjectValidatorOne));

        CommandObjectValidator<MappedCommandObjectValidatorRegistryTestMocks.CommandObjectTwo> coValidatorTwo =
                commandObjectValidatorRegistry.retrieveValidator(MappedCommandObjectValidatorRegistryTestMocks.CommandObjectTwo.class, MappedCommandObjectValidatorRegistryTestMocks.Create.class);

        assertThat(coValidatorTwo, equalTo(commandObjectValidatorTwo));

        CommandObjectValidator<MappedCommandObjectValidatorRegistryTestMocks.CommandObjectTwo> coValidatorThree =
                commandObjectValidatorRegistry.retrieveValidator(MappedCommandObjectValidatorRegistryTestMocks.CommandObjectTwo.class, MappedCommandObjectValidatorRegistryTestMocks.Update.class);

        assertThat(coValidatorThree, equalTo(commandObjectValidatorThree));
    }

    @Test
    public void test02_shouldThrowValidationFrameworkException_whenThereAreDuplicatesInValidators(){
        commandObjectValidatorRegistry =
                createValidatorRegistryUnderTest(
                        commandObjectValidatorOne, commandObjectValidatorOne, commandObjectValidatorThree);

        assertThat(
                caughtException(),
                allOf(
                        instanceOf(ValidationFrameworkException.class),
                        hasMessage("There are redundant command object validators")
                )
        );
    }

    @Test
    public void test03_shouldThrowValidationFrameworkException_whenMappedValidatorDoesNotSpecifyTargetCO(){
        commandObjectValidatorRegistry =
                createValidatorRegistryUnderTest(
                        commandObjectValidatorOne,
                        mockMappedCommandObjectValidator(null, MappedCommandObjectValidatorRegistryTestMocks.Create.class),
                        commandObjectValidatorThree
                );

        assertThat(
                caughtException(),
                allOf(
                        instanceOf(ValidationFrameworkException.class),
                        hasMessage("Mapped validators should specify groupValidationClass and " +
                                "targetCommandObjectClass")
                )
        );

    }

    @Test
    public void test04_shouldThrowValidationFrameworkException_whenMappedValidatorDoesNotSpecifyGroupValidationClass(){


        commandObjectValidatorRegistry =
                createValidatorRegistryUnderTest(
                        commandObjectValidatorOne,
                        mockMappedCommandObjectValidator(MappedCommandObjectValidatorRegistryTestMocks.CommandObjectOne.class, null),
                        commandObjectValidatorThree
                );

        assertThat(
                caughtException(),
                allOf(
                        instanceOf(ValidationFrameworkException.class),
                        hasMessage("Mapped validators should specify groupValidationClass and " +
                                "targetCommandObjectClass")
                )
        );
    }

    @Test
    public void test05_shouldThrowValidationFrameworkException_whenValidatorsSetIsEmpty(){


        commandObjectValidatorRegistry =
                createValidatorRegistryUnderTest();

        assertThat(
                caughtException(),
                allOf(
                        instanceOf(ValidationFrameworkException.class),
                        hasMessage("Cannot register empty or null validators list")
                )
        );
    }

    private static MappedCommandObjectValidatorRegistryImpl createValidatorRegistryUnderTest(
            MappedCommandObjectValidator<?,?>... validators){
        MappedCommandObjectValidatorRegistryImpl commandObjectValidatorRegistryImpl =
                new MappedCommandObjectValidatorRegistryImpl();
        catchException(commandObjectValidatorRegistryImpl)
                .registerValidators(Stream.of(validators).collect(Collectors.toList()));
        return commandObjectValidatorRegistryImpl;
    }

    private static <CO, GVC> MappedCommandObjectValidator<CO, GVC> mockMappedCommandObjectValidator(
            Class<CO> coClass, Class<GVC> gvcClass){
        return new MappedCommandObjectValidator<CO, GVC>(null) {
            @Override
            public Class<CO> getTargetCommandObjectClass() {
                return coClass;
            }

            @Override
            public Class<GVC> getTargetGroupValidationClass() {
                return gvcClass;
            }

            @Override
            protected void validateCommand(FieldErrorsRegistry fieldErrorsRegistry, CO co) {

            }
        };
    }
}
