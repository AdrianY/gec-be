package com.greatuslabs.be.commons.validation.config.mocks;

import com.greatuslabs.be.commons.validation.api.annotations.IsValidCommandObject;
import com.greatuslabs.be.commons.validation.api.annotations.IsValidId;

import javax.validation.constraints.NotNull;

public class CommandObjectValidationAspectTestMocks {

    public static class Component {

        public void checkId(
                @IsValidId(
                        idName = "parameterName",
                        validator = "idValidator",
                        groupValidationClass = IdValidationClass.class
                ) String anotherValue
        ){

        }

        public void checkIdNoCustomValidator(
                @IsValidId(
                        idName = "parameterName",
                        groupValidationClass = IdValidationClass.class
                ) String anotherValue
        ){

        }


        public void consume(
                @IsValidCommandObject CommandObject mockPojo ){

        }

        public void consume(
                @IsValidId(
                        idName = "parameterName",
                        validator = "idValidator",
                        groupValidationClass = IdValidationClass.class
                ) String anotherValue,
                @IsValidCommandObject CommandObject mockPojo
        ){

        }

        public void consume(
                @IsValidCommandObject CommandObject mockPojo,
                @IsValidId(
                        idName = "parameterName",
                        validator = "idValidator",
                        groupValidationClass = IdValidationClass.class
                ) String anotherValue
        ){

        }

        public void consumeWithValidator(
                @IsValidCommandObject(
                        groupValidationClass = IdValidationClass.class
                ) CommandObject mockPojo ){

        }

        public void consumeWithValidator(
                @IsValidId(
                        idName = "parameterName",
                        validator = "idValidator",
                        groupValidationClass = IdValidationClass.class
                ) String anotherValue,
                @IsValidCommandObject(
                        groupValidationClass = IdValidationClass.class
                ) CommandObject mockPojo
        ){

        }

        public void consumeWithValidator(
                @IsValidCommandObject(
                        groupValidationClass = IdValidationClass.class
                ) CommandObject mockPojo,
                @IsValidId(
                        idName = "parameterName",
                        validator = "idValidator",
                        groupValidationClass = IdValidationClass.class
                ) String anotherValue
        ){

        }

    }


    public static class IdValidationClass{}

    public static class CommandObject{
        @NotNull(message = "cannot be null")
        private String property;

        public CommandObject(String property) {
            this.property = property;
        }

        public String getProperty() {
            return property;
        }
    }
}
