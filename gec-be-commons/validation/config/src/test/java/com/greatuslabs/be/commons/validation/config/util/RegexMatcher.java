package com.greatuslabs.be.commons.validation.config.util;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;

public class RegexMatcher extends BaseMatcher<String> implements Matcher<String> {
    private final String regex;

    private RegexMatcher(String regex){
        this.regex = regex;
    }

    public boolean matches(Object o){
        return ((String)o).matches(regex);

    }

    public void describeTo(Description description){
        description.appendText("matches regex=").appendText(regex);
    }

    public static RegexMatcher matchesPattern(String regex){
        return new RegexMatcher(regex);
    }
}