package com.greatuslabs.be.commons.validation.config;

import com.greatuslabs.be.commons.validation.api.dto.Operation;
import com.greatuslabs.be.commons.validation.api.dto.OperationValidationResult;
import com.greatuslabs.be.commons.validation.api.builders.ValidationOperationBuilder;
import com.greatuslabs.be.commons.validation.api.util.CoreValidationOperationBuilders;
import org.apache.commons.lang3.StringUtils;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CoreValidationOperationBuildersTest {

    @Test
    public void test01_equalToShouldProduceAppropriateBuilder_whenValueIsValid(){
        ValidationOperationBuilder<Integer> validationOperationBuilder =
                CoreValidationOperationBuilders.equalTo(1);
        Class<Integer> expectedType = Integer.class;
        Operation<Integer> expectedBean = new Operation<>("equal", "==", 1);

        assertSuccessfulBuilderValidationResults(validationOperationBuilder, expectedType, expectedBean);
    }

    @Test
    public void test02_equalToShouldProductAppropriateBuilder_whenValueIsInvalid(){
        assertErroneousBuilderValidationResult(
                CoreValidationOperationBuilders.equalTo(null),
                "Invalid value for operation equal. Value should not be null"
        );
    }

    @Test
    public void test03_notEqualToShouldProduceAppropriateBuilder_whenValueIsValid(){
        ValidationOperationBuilder<Double> validationOperationBuilder =
                CoreValidationOperationBuilders.notEqualTo(1.0);
        Class<Double> expectedType = Double.class;
        Operation<Double> expectedBean = new Operation<>("not equal", "!=", 1.0);

        assertSuccessfulBuilderValidationResults(validationOperationBuilder, expectedType, expectedBean);
    }

    @Test
    public void test04_notEqualToShouldReturnErroneousResult_whenValueIsInvalid(){
        assertErroneousBuilderValidationResult(
                CoreValidationOperationBuilders.notEqualTo(null),
                "Invalid value for operation not equal. Value should not be null"
        );
    }

    @Test
    public void test05_greaterThanShouldProduceAppropriateBuilder_whenValueIsValid(){
        ValidationOperationBuilder<BigDecimal> validationOperationBuilder =
                CoreValidationOperationBuilders.greaterThan(BigDecimal.ZERO);
        Class<BigDecimal> expectedType = BigDecimal.class;
        Operation<BigDecimal> expectedBean = new Operation<>("greater than", ">", BigDecimal.ZERO);

        assertSuccessfulBuilderValidationResults(validationOperationBuilder, expectedType, expectedBean);
    }

    @Test
    public void test06_greaterThanShouldReturnErroneousResult_whenValueIsInvalid(){
        assertErroneousBuilderValidationResult(
                CoreValidationOperationBuilders.greaterThan(null),
                "Invalid value for operation greater than. Value should not be null"
        );
    }

    @Test
    public void test07_lessThanShouldProduceAppropriateBuilder_whenValueIsValid(){
        ValidationOperationBuilder<BigInteger> validationOperationBuilder =
                CoreValidationOperationBuilders.lessThan(BigInteger.ZERO);
        Class<BigInteger> expectedType = BigInteger.class;
        Operation<BigInteger> expectedBean = new Operation<>("less than", "<", BigInteger.ZERO);

        assertSuccessfulBuilderValidationResults(validationOperationBuilder, expectedType, expectedBean);
    }

    @Test
    public void test08_lessThanShouldReturnErroneousResult_whenValueIsInvalid(){
        assertErroneousBuilderValidationResult(
                CoreValidationOperationBuilders.lessThan(null),
                "Invalid value for operation less than. Value should not be null"
        );
    }

    @Test
    public void test09_greaterThanOrEqualShouldProduceAppropriateBuilder_whenValueIsValid(){
        ValidationOperationBuilder<BigInteger> validationOperationBuilder =
                CoreValidationOperationBuilders.greaterThanOrEqual(BigInteger.ZERO);
        Class<BigInteger> expectedType = BigInteger.class;
        Operation<BigInteger> expectedBean = new Operation<>(
                "greater than or equal", ">=", BigInteger.ZERO);

        assertSuccessfulBuilderValidationResults(validationOperationBuilder, expectedType, expectedBean);
    }

    @Test
    public void test10_greaterThanOrEqualShouldReturnErroneousResult_whenValueIsInvalid(){
        assertErroneousBuilderValidationResult(
                CoreValidationOperationBuilders.greaterThanOrEqual(null),
                "Invalid value for operation greater than or equal. Value should not be null"
        );
    }

    @Test
    public void test11_greaterThanOrEqualShouldProduceAppropriateBuilder_whenValueIsValid(){
        ValidationOperationBuilder<BigInteger> validationOperationBuilder =
                CoreValidationOperationBuilders.greaterThanOrEqual(BigInteger.ZERO);
        Class<BigInteger> expectedType = BigInteger.class;
        Operation<BigInteger> expectedBean = new Operation<>(
                "greater than or equal", ">=", BigInteger.ZERO);

        assertSuccessfulBuilderValidationResults(validationOperationBuilder, expectedType, expectedBean);
    }

    @Test
    public void test12_greaterThanOrEqualShouldReturnErroneousResult_whenValueIsInvalid(){
        assertErroneousBuilderValidationResult(
                CoreValidationOperationBuilders.greaterThanOrEqual(null),
                "Invalid value for operation greater than or equal. Value should not be null"
        );
    }

    @Test
    public void test13_lessThanOrEqualShouldProduceAppropriateBuilder_whenValueIsValid(){
        ValidationOperationBuilder<BigInteger> validationOperationBuilder =
                CoreValidationOperationBuilders.lessThanOrEqual(BigInteger.ZERO);
        Class<BigInteger> expectedType = BigInteger.class;
        Operation<BigInteger> expectedBean = new Operation<>(
                "less than or equal", "<=", BigInteger.ZERO);

        assertSuccessfulBuilderValidationResults(validationOperationBuilder, expectedType, expectedBean);
    }

    @Test
    public void test14_lessThanOrEqualShouldReturnErroneousResult_whenValueIsInvalid(){
        assertErroneousBuilderValidationResult(
                CoreValidationOperationBuilders.lessThanOrEqual(null),
                "Invalid value for operation less than or equal. Value should not be null"
        );
    }

    @Test
    public void test15_requiredShouldProduceAppropriateBuilder_whenValueIsValid(){
        ValidationOperationBuilder<Void> validationOperationBuilder =
                CoreValidationOperationBuilders.required();
        Class<Void> expectedType = Void.class;
        Operation<Void> expectedBean = new Operation<>(
                "required", "required", null);

        assertSuccessfulBuilderValidationResults(validationOperationBuilder, expectedType, expectedBean);
    }

    @Test
    public void test16_validShouldProduceAppropriateBuilder_whenValueIsValid(){
        ValidationOperationBuilder<Void> validationOperationBuilder =
                CoreValidationOperationBuilders.valid();
        Class<Void> expectedType = Void.class;
        Operation<Void> expectedBean = new Operation<>(
                "valid", "valid", null);

        assertSuccessfulBuilderValidationResults(validationOperationBuilder, expectedType, expectedBean);
    }

    @Test
    public void test17_inShouldProduceAppropriateBuilder_whenValueIsValid(){
        ValidationOperationBuilder<Short[]> validationOperationBuilder =
                CoreValidationOperationBuilders.in((short)1,(short)2,(short)3);
        Class<Short> expectedType = Short.class;
        Operation<Short[]> expectedBean = new Operation<>(
                "in", "in", new Short[]{(short)1,(short)2,(short)3});

        assertSuccessfulBuilderValidationResults(validationOperationBuilder, expectedType, expectedBean);
    }

    @Test
    public void test18_inShouldReturnErroneousResult_whenValueIsInvalid(){
        Short[] values = new Short[0];
        assertErroneousBuilderValidationResult(
                CoreValidationOperationBuilders.in(values),
                "Invalid value for operation in. Collection value should not be empty"
        );
    }

    @Test
    public void test19_notInShouldProduceAppropriateBuilder_whenValueIsValid(){
        ValidationOperationBuilder<Short[]> validationOperationBuilder =
                CoreValidationOperationBuilders.notIn((short)1,(short)2,(short)3);
        Class<Short> expectedType = Short.class;
        Operation<Short[]> expectedBean = new Operation<>(
                "not in", "not in", new Short[]{(short)1,(short)2,(short)3});

        assertSuccessfulBuilderValidationResults(validationOperationBuilder, expectedType, expectedBean);
    }

    @Test
    public void test20_notInShouldReturnErroneousResult_whenValueIsInvalid(){
        assertErroneousBuilderValidationResult(
                CoreValidationOperationBuilders.notIn(),
                "Invalid value for operation not in. Collection value should not be empty"
        );
    }

    @Test
    public void test21_betweenShouldProduceAppropriateBuilder_whenValueIsValid(){
        ValidationOperationBuilder<Map<String, Byte>> validationOperationBuilder =
                CoreValidationOperationBuilders.between((byte)1,(byte)2);
        Class<Byte> expectedType = Byte.class;
        
        Map<String, Byte> operationValue = new HashMap<>();
        operationValue.put("lowerBound", (byte)1);
        operationValue.put("upperBound", (byte)2);
        Operation<Map<String, Byte>> expectedBean = new Operation<>(
                "between", "between", operationValue);

        assertSuccessfulBuilderValidationResults(validationOperationBuilder, expectedType, expectedBean);
    }

    @Test
    public void test22_betweenShouldReturnErroneousResult_whenValueIsInvalid(){
        assertErroneousBuilderValidationResult(
                CoreValidationOperationBuilders.between(null, (byte)2),
                "Invalid values for bounded operation between. Upper and lower bound should not be null"
        );

        assertErroneousBuilderValidationResult(
                CoreValidationOperationBuilders.between((byte)1, null),
                "Invalid values for bounded operation between. Upper and lower bound should not be null"
        );

        assertErroneousBuilderValidationResult(
                CoreValidationOperationBuilders.between((byte)2, (byte)1),
                "Invalid values for bounded operation between. " +
                        "Upper bound should be greater than lower bound"
        );

        assertErroneousBuilderValidationResult(
                CoreValidationOperationBuilders.between((byte)2, (byte)2),
                "Invalid values for bounded operation between. " +
                        "Upper bound should be greater than lower bound"
        );
    }

    @Test
    public void test23_notBetweenShouldProduceAppropriateBuilder_whenValueIsValid(){
        ValidationOperationBuilder<Map<String, Byte>> validationOperationBuilder =
                CoreValidationOperationBuilders.notBetween((byte)1,(byte)2);
        Class<Byte> expectedType = Byte.class;

        Map<String, Byte> operationValue = new HashMap<>();
        operationValue.put("lowerBound", (byte)1);
        operationValue.put("upperBound", (byte)2);
        Operation<Map<String, Byte>> expectedBean = new Operation<>(
                "not between", "not between", operationValue);

        assertSuccessfulBuilderValidationResults(validationOperationBuilder, expectedType, expectedBean);
    }

    @Test
    public void test24_notBetweenShouldReturnErroneousResult_whenValueIsInvalid(){
        assertErroneousBuilderValidationResult(
                CoreValidationOperationBuilders.notBetween(null, (byte)2),
                "Invalid values for bounded operation not between. " +
                        "Upper and lower bound should not be null"
        );

        assertErroneousBuilderValidationResult(
                CoreValidationOperationBuilders.notBetween((byte)1, null),
                "Invalid values for bounded operation not between. " +
                        "Upper and lower bound should not be null"
        );

        assertErroneousBuilderValidationResult(
                CoreValidationOperationBuilders.notBetween((byte)2, (byte)1),
                "Invalid values for bounded operation not between. " +
                        "Upper bound should be greater than lower bound"
        );

        assertErroneousBuilderValidationResult(
                CoreValidationOperationBuilders.notBetween((byte)2, (byte)2),
                "Invalid values for bounded operation not between. " +
                        "Upper bound should be greater than lower bound"
        );
    }

    @Test
    public void test25_likePatternShouldProduceAppropriateBuilder_whenValueIsValid(){
        ValidationOperationBuilder<String> validationOperationBuilder =
                CoreValidationOperationBuilders.likePattern("[A-Z]{1,}");
        Class<String> expectedType = String.class;
        Operation<String> expectedBean = new Operation<>(
                "like pattern", "pattern", "[A-Z]{1,}");

        assertSuccessfulBuilderValidationResults(validationOperationBuilder, expectedType, expectedBean);
    }

    @Test
    public void test26_likePatternShouldReturnErroneousResult_whenValueIsInvalid(){
        assertErroneousBuilderValidationResult(
                CoreValidationOperationBuilders.likePattern(null),
                "Invalid value for operation like pattern. Pattern null is invalid. Details are: null"
        );

        assertErroneousBuilderValidationResultWithMessagePattern(
                CoreValidationOperationBuilders.likePattern("****"),
                "Invalid value for operation like pattern. Pattern **** is invalid. Details are: "
        );
    }

    @Test
    public void test27_notLikePatternShouldProduceAppropriateBuilder_whenValueIsValid(){
        ValidationOperationBuilder<String> validationOperationBuilder =
                CoreValidationOperationBuilders.notLikePattern("[A-Z]{1,}");
        Class<String> expectedType = String.class;
        Operation<String> expectedBean = new Operation<>(
                "not like pattern", "unlike pattern", "[A-Z]{1,}");

        assertSuccessfulBuilderValidationResults(validationOperationBuilder, expectedType, expectedBean);
    }

    @Test
    public void test28_notLikePatternShouldReturnErroneousResult_whenValueIsInvalid(){
        assertErroneousBuilderValidationResult(
                CoreValidationOperationBuilders.notLikePattern(null),
                "Invalid value for operation not like pattern. Pattern null is invalid. Details are: null"
        );

        assertErroneousBuilderValidationResultWithMessagePattern(
                CoreValidationOperationBuilders.notLikePattern("****"),
                "Invalid value for operation not like pattern. Pattern **** is invalid. Details are: "
        );
    }


    private static void assertSuccessfulBuilderValidationResults(
            ValidationOperationBuilder<?> validationOperationBuilder,
            Class<?> expected,
            Operation<?> expectedBean
    ) {
        OperationValidationResult validationResult = validationOperationBuilder.validate();
        assertTrue(validationResult.isSuccessful());

        assertEquals(expected, validationOperationBuilder.getValueType());

        assertThat(
                validationOperationBuilder.build(),
                samePropertyValuesAs(expectedBean)
        );
    }

    private static void assertErroneousBuilderValidationResult(
            ValidationOperationBuilder<?> validationOperationBuilder,
            String expectedMessage
    ) {
        OperationValidationResult validationResult = validationOperationBuilder.validate();
        assertFalse(validationResult.isSuccessful());

        String validationResultReason = validationResult.getReason();
        assertTrue(StringUtils.isNotEmpty(validationResultReason));
        assertEquals(
                expectedMessage,
                validationResultReason
        );
    }

    private static void assertErroneousBuilderValidationResultWithMessagePattern(
            ValidationOperationBuilder<?> validationOperationBuilder,
            String messagePattern
    ) {
        OperationValidationResult validationResult = validationOperationBuilder.validate();
        assertFalse(validationResult.isSuccessful());

        String validationResultReason = validationResult.getReason();
        assertTrue(StringUtils.isNotEmpty(validationResultReason));
        assertThat(
                validationResultReason,
                startsWith(messagePattern)
        );
    }


}
