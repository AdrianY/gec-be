package com.greatuslabs.be.commons.validation.config;

import com.greatuslabs.be.commons.validation.api.annotations.IsValidId;
import com.greatuslabs.be.commons.validation.api.validators.IdValidator;
import com.greatuslabs.be.commons.validation.config.domain.aspects.IdValidationAspect;
import com.greatuslabs.be.commons.validation.config.domain.exceptions.NoValidatorFoundException;
import com.greatuslabs.be.commons.validation.config.domain.exceptions.NullIdException;
import com.greatuslabs.be.commons.validation.config.domain.references.ValidatorType;
import org.aspectj.lang.JoinPoint;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Matchers;
import org.springframework.aop.aspectj.annotation.AspectJProxyFactory;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import com.greatuslabs.be.commons.validation.config.mocks.TestConfig;

import java.util.Map;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class IdValidationAspectTest {

    @MockBean
    private Map<String, IdValidator> idValidatorRegistry;

    private TestConfig.MockComponent mockComponent;

    @Captor
    private ArgumentCaptor<IsValidId> isValidIdArgumentCaptor;

    @MockBean
    private IdValidator idValidator;

    @MockBean
    private IdValidationAspect idValidationAspect;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void init(){
        TestConfig.MockComponent rawMockComponent = new TestConfig.MockComponent();
        AspectJProxyFactory proxyFactory = new AspectJProxyFactory(rawMockComponent);
        IdValidationAspect rawIdValidationAspect = new IdValidationAspect(idValidatorRegistry);
        idValidationAspect = spy(rawIdValidationAspect);
        proxyFactory.addAspect(idValidationAspect);

        mockComponent = proxyFactory.getProxy();
    }

    @Test
    public void test01_shouldThrowParameterValidationException_whenFirstParameterIsExistingIdIsNull(){
        assertDefaultValidationForNullIds();
        mockComponent.consume(null, new TestConfig.MockPojo("Sample Property"));
    }

    @Test
    public void test02_shouldThrowParameterValidationException_whenNoneFirstParameterIsExistingIdIsNull(){
        assertDefaultValidationForNullIds();
        mockComponent.consume(new TestConfig.MockPojo("Sample Property"),null);
    }

    @Test
    public void test03_shouldThrowParameterValidationException_whenIsExistingIdIsNull(){
        assertDefaultValidationForNullIds();
        mockComponent.checkId(null);
    }

    private void assertDefaultValidationForNullIds() {
        thrown.expect(NullIdException.class);
        String parameterName = "parameterName";
        thrown.expectMessage(parameterName+" must not be null");
        thrown.expect(hasProperty("parameterName", is(parameterName)));
    }

    @Test
    public void test04_shouldInvokeCustomValidator_whenIdValueIsNotNull() throws NoSuchMethodException {

        when(idValidatorRegistry.get(eq("idValidator")))
                .thenReturn(idValidator);

        final String mockId = "2L";

        mockComponent.checkId(mockId);

        verify(idValidationAspect, times(1)).validateId(Matchers.any(JoinPoint.class));

        verify(idValidator, times(1))
                .validateEntityId(eq("2L"), isValidIdArgumentCaptor.capture());

        IsValidId idValidationDetails = isValidIdArgumentCaptor.getValue();

        assertEquals("idValidator", idValidationDetails.validator());
        assertEquals(TestConfig.SampleIdValidationClass.class, idValidationDetails.groupValidationClass());
    }

    @Test
    public void test05_shouldThrowNoValidatorFoundException_whenThereIsNoMatchingValidator() throws NoSuchMethodException {

        final String validatorName = "idValidator";
        final ValidatorType validatorType = ValidatorType.ID_VALIDATOR;
        when(idValidatorRegistry.get(eq(validatorName))).thenReturn(null);

        final String mockId = "2L";

        thrown.expect(NoValidatorFoundException.class);
        thrown.expectMessage("No validator found with getName \""+validatorName+"\"");
        thrown.expect(hasProperty("validatorName", is(validatorName)));
        thrown.expect(hasProperty("validatorType", is(validatorType)));

        mockComponent.checkId(mockId);

        verify(idValidationAspect, times(1)).validateId(Matchers.any(JoinPoint.class));
    }

    @Test
    public void test06_shouldNotThrowNoValidatorFoundException_whenThereIsValidatorNameIsEmpty() throws NoSuchMethodException {

        final String mockId = "2L";

        mockComponent.checkIdNoCustomValidator(mockId);

        verify(idValidatorRegistry, never()).get(anyString());
        verify(idValidationAspect, times(1)).validateId(Matchers.any(JoinPoint.class));

    }


}
