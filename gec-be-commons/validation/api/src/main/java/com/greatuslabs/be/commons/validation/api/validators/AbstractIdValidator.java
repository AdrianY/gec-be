package com.greatuslabs.be.commons.validation.api.validators;


import com.greatuslabs.be.commons.validation.api.annotations.IsValidId;
import com.greatuslabs.be.commons.validation.api.exceptions.NoIdValidatorMethodException;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;

public abstract class AbstractIdValidator<ID_TYPE> implements IdValidator{

    private final Map<Class<?>, Consumer<ID_TYPE>> validatorMethodsRegistry;

    public AbstractIdValidator() {
        this(new HashMap<>());
    }

    public AbstractIdValidator(Map<Class<?>, Consumer<ID_TYPE>> validatorMethodsRegistry) {
        this.validatorMethodsRegistry = validatorMethodsRegistry;
        registerMethods(validatorMethodsRegistry);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <ID> void validateEntityId(ID id, IsValidId validationDetails) {
        Consumer<ID> idValidator =
                (Consumer<ID>) validatorMethodsRegistry.get(validationDetails.groupValidationClass());
        if(Objects.isNull(idValidator))
            throw new NoIdValidatorMethodException(
                    buildErrorMessage(validationDetails),
                    validationDetails.validator(),
                    validationDetails.groupValidationClass()
            );
        idValidator.accept(id);
    }

    protected abstract void registerMethods(Map<Class<?>, Consumer<ID_TYPE>> validatorMethodsRegistry);

    private static String buildErrorMessage(IsValidId validationDetails){
        return "No id com.greatuslabs.be.commons.validation handler found in " +
                "IdValidator: "+validationDetails.validator()+" for com.greatuslabs.be.commons.validation group class "+
                validationDetails.groupValidationClass().getName();
    }
}
