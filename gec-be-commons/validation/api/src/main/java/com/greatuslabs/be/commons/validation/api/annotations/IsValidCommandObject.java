package com.greatuslabs.be.commons.validation.api.annotations;


import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.PARAMETER})
@Documented
public @interface IsValidCommandObject {
    Class<?> groupValidationClass() default Void.class;

    boolean performFullValidation() default true;
}
