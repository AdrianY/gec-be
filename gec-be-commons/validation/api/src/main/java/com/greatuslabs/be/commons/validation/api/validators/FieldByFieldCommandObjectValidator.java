package com.greatuslabs.be.commons.validation.api.validators;

import com.greatuslabs.be.commons.validation.api.dto.FieldError;
import com.greatuslabs.be.commons.validation.api.util.FieldErrorsRegistry;

import java.util.List;

public abstract class FieldByFieldCommandObjectValidator<COMMAND_OBJECT>
        implements CommandObjectValidator<COMMAND_OBJECT>{

    private final CommandObjectValidationErrorCollector commandObjectValidationErrorCollector;

    protected FieldByFieldCommandObjectValidator(
            CommandObjectValidationErrorCollector commandObjectValidationErrorCollector) {
        this.commandObjectValidationErrorCollector = commandObjectValidationErrorCollector;
    }

    @Override
    public void validateCommand(COMMAND_OBJECT commandObject, boolean fullValidation) {
        Class<?> groupValidationClass = getGroupValidationClass();
        FieldErrorsRegistry fieldErrorsRegistry =
                FieldErrorsRegistry.createInstance(commandObject, groupValidationClass);

        if(fullValidation){
            List<FieldError> fieldErrors =
                    commandObjectValidationErrorCollector.collectErrors(commandObject, groupValidationClass);
            fieldErrorsRegistry.registerFieldErrors(fieldErrors);
        }

        validateCommand(fieldErrorsRegistry, commandObject);
    }

    protected abstract void validateCommand(FieldErrorsRegistry fieldErrorsRegistry, COMMAND_OBJECT commandObject);

    protected abstract Class<?> getGroupValidationClass();
}
