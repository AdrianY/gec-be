package com.greatuslabs.be.commons.validation.api.exceptions;

import com.greatuslabs.be.commons.validation.api.dto.FieldError;
import java.util.List;

public class CommandObjectValidationException extends RuntimeException{

    private final Class<?> groupValidationClass;
    private final List<FieldError> fieldErrors;

    public CommandObjectValidationException(
            Class<?> groupValidationClass,
            String errorMessage,
            List<FieldError> fieldErrors
    ) {
        super(errorMessage);
        this.groupValidationClass = groupValidationClass;
        this.fieldErrors = fieldErrors;
    }

    public List<FieldError> getFieldErrors() {
        return fieldErrors;
    }

    public Class<?> getGroupValidationClass() {
        return groupValidationClass;
    }
}
