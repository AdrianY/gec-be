package com.greatuslabs.be.commons.validation.api.dto;

import java.util.List;
import java.util.Map;

public class ValidateableModel<QUERY_RESULT_OBJECT> {
    private final QUERY_RESULT_OBJECT queryResultObject;
    private final Map<String, List<Operation<?>>> validationMetadata;

    public ValidateableModel(QUERY_RESULT_OBJECT queryResultObject, Map<String, List<Operation<?>>> validationMetadata) {
        this.queryResultObject = queryResultObject;
        this.validationMetadata = validationMetadata;
    }

    public QUERY_RESULT_OBJECT getQueryResultObject() {
        return queryResultObject;
    }

    public Map<String, List<Operation<?>>> getValidationMetadata() {
        return validationMetadata;
    }
}
