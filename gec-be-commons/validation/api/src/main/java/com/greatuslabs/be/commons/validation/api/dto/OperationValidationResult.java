package com.greatuslabs.be.commons.validation.api.dto;

public class OperationValidationResult {

    private final boolean successful;
    private String reason;

    private OperationValidationResult(boolean successful) {
        this.successful = successful;
    }

    private OperationValidationResult(boolean successful, String reason) {
        this.successful = successful;
        this.reason = reason;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public String getReason() {
        return reason;
    }

    public static OperationValidationResult successfulResult(){
        return new OperationValidationResult(true);
    }

    public static OperationValidationResult erroneousResult(String message){
        return new OperationValidationResult(false, message);
    }

}
