package com.greatuslabs.be.commons.validation.api.exceptions;

public class NoIdValidatorMethodException extends RuntimeException{
    private final String validatorName;
    private final Class<?> validationClass;

    public NoIdValidatorMethodException(String message, String validatorName, Class<?> validationClass) {
        super(message);
        this.validatorName = validatorName;
        this.validationClass = validationClass;
    }

    public String getValidatorName() {
        return validatorName;
    }

    public Class<?> getValidationClass() {
        return validationClass;
    }
}
