package com.greatuslabs.be.commons.validation.api.builders;

import java.util.function.Consumer;

public interface ClassFieldValidationMappingBuilder {
    ClassFieldValidationMappingBuilder field(Consumer<FieldValidationMetadataBuilder> fieldBuilderFunction);
}
