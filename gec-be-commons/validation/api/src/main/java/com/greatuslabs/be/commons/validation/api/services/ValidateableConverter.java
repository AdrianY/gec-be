package com.greatuslabs.be.commons.validation.api.services;


import com.greatuslabs.be.commons.validation.api.dto.ValidateableModel;

public interface ValidateableConverter {
    <QUERY_RESULT_OBJECT> ValidateableModel<QUERY_RESULT_OBJECT> toValidateable(QUERY_RESULT_OBJECT queryResultObject);
}
