package com.greatuslabs.be.commons.validation.api.builders;

public interface ValidationMapperFactory {
    ClassFieldValidationMappingBuilder mapClass(Class<?> targetClass);
}
