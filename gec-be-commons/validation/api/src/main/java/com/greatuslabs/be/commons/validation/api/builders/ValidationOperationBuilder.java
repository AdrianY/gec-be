package com.greatuslabs.be.commons.validation.api.builders;


import com.greatuslabs.be.commons.validation.api.dto.Operation;
import com.greatuslabs.be.commons.validation.api.dto.OperationValidationResult;

@FunctionalInterface
public interface ValidationOperationBuilder<TYPE>{

    Operation<TYPE> build();

    default OperationValidationResult validate(){
        return OperationValidationResult.successfulResult();
    }

    default Class<?> getValueType(){
        return Void.class;
    }

}
