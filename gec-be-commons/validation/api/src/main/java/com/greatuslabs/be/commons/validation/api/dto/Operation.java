package com.greatuslabs.be.commons.validation.api.dto;

public class Operation<T>{

    private final String name;
    private final String codeName;
    private final T value;

    public Operation(String name, String codeName, T value) {
        this.name = name;
        this.codeName = codeName;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getCodeName() {
        return codeName;
    }

    public T getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Operation{" +
                "name='" + name + '\'' +
                ", codeName='" + codeName + '\'' +
                ", value=" + value +
                '}';
    }
}
