package com.greatuslabs.be.commons.validation.api.util;

import com.greatuslabs.be.commons.validation.api.builders.ValidationOperationBuilder;
import com.greatuslabs.be.commons.validation.api.dto.Operation;
import com.greatuslabs.be.commons.validation.api.dto.OperationValidationResult;
import org.apache.commons.lang3.ArrayUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;

public final class CoreValidationOperationBuilders {
    private CoreValidationOperationBuilders(){}

    public static <VALUE> ValidationOperationBuilder<VALUE> equalTo(VALUE value){
        return new DefaultValidationOperationBuilder<>("equal", "==", value);
    }

    public static <VALUE> ValidationOperationBuilder<VALUE> notEqualTo(VALUE value){
        return new DefaultValidationOperationBuilder<>("not equal", "!=", value);
    }

    public static <VALUE extends Number> ValidationOperationBuilder<VALUE> greaterThan(VALUE value){
        return new DefaultValidationOperationBuilder<>("greater than", ">", value);
    }

    public static <VALUE extends Number> ValidationOperationBuilder<VALUE> lessThan(VALUE value){
        return new DefaultValidationOperationBuilder<>("less than", "<", value);
    }

    public static <VALUE extends Number> ValidationOperationBuilder<VALUE> greaterThanOrEqual(VALUE value){
        return new DefaultValidationOperationBuilder<>("greater than or equal", ">=", value);
    }

    public static <VALUE extends Number> ValidationOperationBuilder<VALUE> lessThanOrEqual(VALUE value){
        return new DefaultValidationOperationBuilder<>("less than or equal", "<=", value);
    }

    public static ValidationOperationBuilder<Void> required(){
        return () -> new Operation<>("required", "required", null);
    }

    public static ValidationOperationBuilder<Void> valid(){
        return () -> new Operation<>("valid", "valid", null);
    }

    @SafeVarargs
    public static <VALUE> ValidationOperationBuilder<VALUE[]> in(VALUE... values){
        return new CollectionEqualityValidationOperationBuilder<>("in", "in", values);
    }

    @SafeVarargs
    public static <VALUE> ValidationOperationBuilder<VALUE[]> notIn(VALUE... values){
        return new CollectionEqualityValidationOperationBuilder<>("not in", "not in", values);
    }

    public static <VALUE extends Comparable<VALUE>> ValidationOperationBuilder<Map<String, VALUE>> between(
            VALUE lowerBound, VALUE upperBound){
        return new BoundedValueValidationOperationBuilder<>(
                "between", "between", lowerBound, upperBound);
    }

    public static <VALUE extends Comparable<VALUE>> ValidationOperationBuilder<Map<String, VALUE>> notBetween(
            VALUE lowerBound, VALUE upperBound){
        return new BoundedValueValidationOperationBuilder<>(
                "not between", "not between", lowerBound, upperBound);
    }

    public static ValidationOperationBuilder<String> likePattern(String value){
        return new PatternValidationOperationBuilder("like pattern", "pattern", value);
    }

    public static ValidationOperationBuilder<String> notLikePattern(String value){
        return new PatternValidationOperationBuilder("not like pattern", "unlike pattern", value);
    }

    public static class DefaultValidationOperationBuilder<VALUE>
            implements ValidationOperationBuilder<VALUE>{

        final String name;
        final String codeName;
        final VALUE value;

        DefaultValidationOperationBuilder(String name, String codeName, VALUE value) {
            this.name = name;
            this.codeName = codeName;
            this.value = value;
        }

        @Override
        public Operation<VALUE> build() {
            return new Operation<>(name, codeName, value);
        }

        @Override
        public OperationValidationResult validate() {
            if(Objects.nonNull(value))
                return OperationValidationResult.successfulResult();
            return OperationValidationResult.erroneousResult("Invalid value for operation "+
                    name+". Value should not be null");
        }

        public Class<?> getValueType(){
            return value.getClass();
        }
    }

    public static class CollectionEqualityValidationOperationBuilder<VALUE>
            extends DefaultValidationOperationBuilder<VALUE[]>{

        @SafeVarargs
        CollectionEqualityValidationOperationBuilder(String name, String codeName, VALUE... value) {
            super(name, codeName, value);
        }

        @Override
        public OperationValidationResult validate() {
            if(ArrayUtils.isEmpty(value))
                return OperationValidationResult.erroneousResult(
                        "Invalid value for operation "+name+". Collection value should not be empty");
            return OperationValidationResult.successfulResult();
        }

        @Override
        public Class<?> getValueType() {
            return value.getClass().getComponentType();
        }
    }

    public static class BoundedValueValidationOperationBuilder<VALUE extends Comparable<VALUE>>
            implements ValidationOperationBuilder<Map<String, VALUE>>{

        final VALUE lowerBound;
        final VALUE upperBound;
        final String name;
        final String codeName;

        BoundedValueValidationOperationBuilder(
                String name, String codeName, VALUE lowerBound, VALUE upperBound) {
            this.lowerBound = lowerBound;
            this.upperBound = upperBound;
            this.name = name;
            this.codeName = codeName;
        }

        @Override
        public Operation<Map<String, VALUE>> build() {
            Map<String, VALUE> values = new HashMap<>();
            values.put("lowerBound", lowerBound);
            values.put("upperBound", upperBound);
            return new Operation<>(name, codeName, values);
        }

        @Override
        public OperationValidationResult validate() {
            if(Objects.isNull(lowerBound) || Objects.isNull(upperBound))
                return OperationValidationResult.erroneousResult("Invalid values for bounded operation "+
                        name+". Upper and lower bound should not be null");

            if(0 <= lowerBound.compareTo(upperBound))
                return OperationValidationResult.erroneousResult("Invalid values for bounded operation "+
                        name+". Upper bound should be greater than lower bound");

            return OperationValidationResult.successfulResult();
        }

        @Override
        public Class<?> getValueType() {
            return lowerBound.getClass();
        }

    }

    public static class PatternValidationOperationBuilder
            extends DefaultValidationOperationBuilder<String>{

        PatternValidationOperationBuilder(String name, String codeName, String pattern) {
            super(name, codeName, pattern);
        }

        @Override
        @SuppressWarnings("ResultOfMethodCallIgnored")
        public OperationValidationResult validate() {
            try {
                Pattern.compile(value);
            } catch (Exception e) {
                return OperationValidationResult.erroneousResult(
                        "Invalid value for operation "+name+
                                ". Pattern "+value+" is invalid. Details are: "+e.getMessage());
            }

            return OperationValidationResult.successfulResult();
        }

        @Override
        public Class<?> getValueType() {
            return String.class;
        }

    }

}
