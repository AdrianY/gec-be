package com.greatuslabs.be.commons.validation.api.validators;

public abstract class MappedCommandObjectValidator<COMMAND_OBJECT, TARGET_VALIDATION_CLASS>
        extends FieldByFieldCommandObjectValidator<COMMAND_OBJECT> {

    protected MappedCommandObjectValidator(
            CommandObjectValidationErrorCollector commandObjectValidationErrorCollector) {
        super(commandObjectValidationErrorCollector);
    }

    protected Class<?> getGroupValidationClass(){
        return getTargetGroupValidationClass();
    }

    public abstract Class<COMMAND_OBJECT> getTargetCommandObjectClass();

    public abstract Class<TARGET_VALIDATION_CLASS> getTargetGroupValidationClass();
}
