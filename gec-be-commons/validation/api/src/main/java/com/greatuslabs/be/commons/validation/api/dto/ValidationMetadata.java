package com.greatuslabs.be.commons.validation.api.dto;

import java.util.List;

public class ValidationMetadata {

    private final String fieldName;
    private final List<Operation<?>> validations;

    public ValidationMetadata(String fieldName, List<Operation<?>> validations) {
        this.fieldName = fieldName;
        this.validations = validations;
    }

    public String getFieldName() {
        return fieldName;
    }

    public List<Operation<?>> getValidations() {
        return validations;
    }

    @Override
    public String toString() {
        return "ValidationMetadata{" +
                "fieldName='" + fieldName + '\'' +
                ", validations=" + validations +
                '}';
    }
}
