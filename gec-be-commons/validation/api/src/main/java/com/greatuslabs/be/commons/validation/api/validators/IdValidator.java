package com.greatuslabs.be.commons.validation.api.validators;


import com.greatuslabs.be.commons.validation.api.annotations.IsValidId;

public interface IdValidator {
    <ID> void validateEntityId(ID id, IsValidId validationDetails);
}
