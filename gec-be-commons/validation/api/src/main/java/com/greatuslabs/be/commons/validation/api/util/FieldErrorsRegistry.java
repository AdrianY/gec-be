package com.greatuslabs.be.commons.validation.api.util;

import com.greatuslabs.be.commons.validation.api.dto.FieldError;
import com.greatuslabs.be.commons.validation.api.exceptions.CommandObjectValidationException;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class FieldErrorsRegistry {
    private final Class<?> groupValidationClass;
    private final Object commandObject;
    private final List<FieldError> fieldErrorList;

    public static FieldErrorsRegistry createInstance(
            Object commandObject, Class<?> groupValidationClass){
        return new FieldErrorsRegistry(commandObject, groupValidationClass);
    }

    public static void processFieldErrors(
            Object commandObject, Class<?> groupValidationClass, List<FieldError> fieldErrors){
        FieldErrorsRegistry fieldErrorsRegistry =
                new FieldErrorsRegistry(commandObject, groupValidationClass, fieldErrors);
        fieldErrorsRegistry.processFieldErrors();
    }

    private FieldErrorsRegistry(Object commandObject, Class<?> groupValidationClass) {
        this.commandObject = commandObject;
        this.groupValidationClass = groupValidationClass;
        fieldErrorList = new ArrayList<>();
    }

    private FieldErrorsRegistry(Object commandObject, Class<?> groupValidationClass, List<FieldError> fieldErrors) {
        this.commandObject = commandObject;
        this.groupValidationClass = groupValidationClass;
        this.fieldErrorList = fieldErrors;
    }

    public void registerFieldErrors(FieldError... fieldErrors){
        fieldErrorList.addAll(Arrays.asList(fieldErrors));
    }

    public void registerFieldErrors(List<FieldError> fieldErrors){
        fieldErrorList.addAll(fieldErrors);
    }

    public void registerFieldError(FieldError fieldError){
        fieldErrorList.add(fieldError);
    }

    public void processFieldErrors(){
        if(CollectionUtils.isNotEmpty(fieldErrorList))
            throw new CommandObjectValidationException(
                    groupValidationClass,
                    "Command object, "+commandObject+" validated under validation class "+
                            groupValidationClass.getName()+ ", has validation errors",
                    fieldErrorList
            );
    }


}
