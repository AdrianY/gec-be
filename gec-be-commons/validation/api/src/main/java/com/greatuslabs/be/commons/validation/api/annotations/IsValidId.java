package com.greatuslabs.be.commons.validation.api.annotations;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
@Documented
public @interface IsValidId {

    Class<?> groupValidationClass() default Void.class;

    String validator() default "";

    String idName() default "";
}
