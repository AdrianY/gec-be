package com.greatuslabs.be.commons.validation.api.validators;

import com.greatuslabs.be.commons.validation.api.dto.FieldError;

import java.util.List;

public interface CommandObjectValidationErrorCollector {
    <COMMAND_OBJECT> List<FieldError> collectErrors(COMMAND_OBJECT commandObject, Class<?> groupInterfaceClass);
}
