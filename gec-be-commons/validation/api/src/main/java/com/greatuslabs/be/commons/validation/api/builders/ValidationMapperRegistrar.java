package com.greatuslabs.be.commons.validation.api.builders;

public interface ValidationMapperRegistrar {
    void register();
}
