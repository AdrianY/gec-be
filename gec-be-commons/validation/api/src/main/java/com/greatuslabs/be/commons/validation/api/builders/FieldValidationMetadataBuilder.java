package com.greatuslabs.be.commons.validation.api.builders;

public interface FieldValidationMetadataBuilder {
    void setFieldName(String fieldName);
    void validations(ValidationOperationBuilder<?>... validationBuilders);

}
