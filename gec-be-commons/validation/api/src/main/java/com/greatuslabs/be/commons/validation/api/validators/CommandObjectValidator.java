package com.greatuslabs.be.commons.validation.api.validators;

public interface CommandObjectValidator<COMMAND_OBJECT> {
    void validateCommand(COMMAND_OBJECT commandObject, boolean fullValidation);

}
