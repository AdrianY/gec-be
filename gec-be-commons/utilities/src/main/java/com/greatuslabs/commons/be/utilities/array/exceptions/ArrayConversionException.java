package com.greatuslabs.commons.be.utilities.array.exceptions;

public class ArrayConversionException extends RuntimeException{
    public ArrayConversionException(String message) {
        super(message);
    }
}
