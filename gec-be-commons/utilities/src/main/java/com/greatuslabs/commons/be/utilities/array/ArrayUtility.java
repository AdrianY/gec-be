package com.greatuslabs.commons.be.utilities.array;

import com.greatuslabs.commons.be.utilities.array.exceptions.ArrayConversionException;
import org.apache.commons.collections4.CollectionUtils;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Objects;

public final class ArrayUtility {
    private ArrayUtility(){}

    public static Object[] getArray(Object val){
        int length = Array.getLength(val);
        Object[] outputArray = new Object[length];
        for(int i = 0; i < length; ++i){
            outputArray[i] = Array.get(val, i);
        }
        return outputArray;
    }

    @SuppressWarnings("unchecked")
    public static <TYPE> TYPE[] toArray(Class<TYPE> type, Collection<TYPE> collection){
        if(CollectionUtils.isEmpty(collection))
            throw new ArrayConversionException("Collection must be specified");

        if(Objects.isNull(type))
            throw new ArrayConversionException("Type must be specified");

        TYPE[] array = (TYPE[])Array.newInstance(type, collection.size());
        return collection.toArray(array);
    }
}
