package com.greatuslabs.commons.be.utilities.date.exceptions;

public class DateUtilityException extends RuntimeException{
    public DateUtilityException() {
    }

    public DateUtilityException(String message) {
        super(message);
    }

    public DateUtilityException(String message, Throwable cause) {
        super(message, cause);
    }

    public DateUtilityException(Throwable cause) {
        super(cause);
    }
}
