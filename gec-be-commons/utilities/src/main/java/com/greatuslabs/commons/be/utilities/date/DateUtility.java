package com.greatuslabs.commons.be.utilities.date;

import com.greatuslabs.commons.be.utilities.date.exceptions.DateUtilityException;
import org.apache.commons.lang3.time.DateUtils;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.Objects;

public final class DateUtility {

    private DateUtility(){}

    public static boolean isValidDate(String dateString){
        try {
            DateUtils.parseDate(dateString);
        } catch (ParseException e) {
            return false;
        }

        return true;
    }

    public static Date parseDate(String dateString){
        try {
            return DateUtils.parseDate(dateString);
        } catch (ParseException e) {
            throw new DateUtilityException(e);
        }
    }

    public static LocalDateTime toLocalDateTime(Date date){
        if(Objects.isNull(date))
            throw new DateUtilityException("date cannot be null");

        return date.toInstant().atOffset(ZoneOffset.UTC).toLocalDateTime();
    }
}
