package com.greatuslabs.gec.be.commons.test.annotations;

import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;

/**
 * Created by Adrian on 6/28/2017.
 */
@Inherited
@Documented
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class
})
public @interface GecTransactionalTestExecutionListeners {
}
