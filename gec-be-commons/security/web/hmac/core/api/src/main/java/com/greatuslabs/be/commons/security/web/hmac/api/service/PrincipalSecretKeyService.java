package com.greatuslabs.be.commons.security.web.hmac.api.service;

public interface PrincipalSecretKeyService {

    /**
     * retrieves current secret key
     * @param principalName
     * @return
     */
    String getSecretKey(String principalName);

    /**
     * returns created secret key
     * @param principalName
     * @return
     */
    void registerPrincipal(String principalName, String generatedSecretKey);
}
