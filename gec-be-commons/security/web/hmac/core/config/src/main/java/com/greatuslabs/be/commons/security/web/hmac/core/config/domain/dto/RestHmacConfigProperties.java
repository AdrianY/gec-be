package com.greatuslabs.be.commons.security.web.hmac.core.config.domain.dto;

import javax.validation.constraints.Min;
import java.util.Objects;

public class RestHmacConfigProperties {

    private static final int DEFAULT_TIMESTAMP_EXPIRATION_IN_MINUTES = 5;
    private static final String DEFAULT_TIMESTAMP_EXPIRATION_MIN_ERROR_MESSAGE =
            "Minimum timestamp expiration must be at least: "+DEFAULT_TIMESTAMP_EXPIRATION_IN_MINUTES;

    private static final int MINIMUM_KEY_LENGTH = 40;
    private static final String SECRET_KEY_LENGTH_MIN_ERROR_MESSAGE =
            "HMAC Key Length must be at least: "+MINIMUM_KEY_LENGTH;

    @Min(value = MINIMUM_KEY_LENGTH, message = SECRET_KEY_LENGTH_MIN_ERROR_MESSAGE)
    private Integer secretKeyLength;

    @Min(value = DEFAULT_TIMESTAMP_EXPIRATION_IN_MINUTES, message = DEFAULT_TIMESTAMP_EXPIRATION_MIN_ERROR_MESSAGE)
    private Integer timestampExpirationInMinutes;

    public RestHmacConfigProperties(){
        secretKeyLength = MINIMUM_KEY_LENGTH;
        timestampExpirationInMinutes = DEFAULT_TIMESTAMP_EXPIRATION_IN_MINUTES;
    }

    public Integer getTimestampExpirationInMinutes() {
        return timestampExpirationInMinutes;
    }

    public void setTimestampExpirationInMinutes(Integer timestampExpirationInMinutes) {
        this.timestampExpirationInMinutes = timestampExpirationInMinutes;
    }

    public Integer getSecretKeyLength() {
        return secretKeyLength;
    }

    public void setSecretKeyLength(Integer secretKeyLength) {
        this.secretKeyLength = secretKeyLength;
    }
}
