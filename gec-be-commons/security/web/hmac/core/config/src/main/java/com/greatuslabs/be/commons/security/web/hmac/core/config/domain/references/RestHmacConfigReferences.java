package com.greatuslabs.be.commons.security.web.hmac.core.config.domain.references;

public interface RestHmacConfigReferences {
    String REST_HMAC_CONFIG_PREFIX = "gl.security.rest.hmac";
    String LOGIN_FILTER_PATH = "/hmacLogin";
}
