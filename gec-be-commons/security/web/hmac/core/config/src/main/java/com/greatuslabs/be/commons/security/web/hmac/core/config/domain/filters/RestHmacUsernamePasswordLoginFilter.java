package com.greatuslabs.be.commons.security.web.hmac.core.config.domain.filters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.greatuslabs.be.commons.security.rest.hmac.utilities.HmacUtils;
import com.greatuslabs.be.commons.security.web.hmac.core.config.domain.dto.RestHmacConfigProperties;
import com.greatuslabs.commons.be.security.web.core.internal.filter.AbstractUsernamePasswordLoginFilter;
import com.greatuslabs.commons.be.security.web.portal.form.internal.api.dto.AuthTokenProcessingResult;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RestHmacUsernamePasswordLoginFilter extends AbstractUsernamePasswordLoginFilter {

    private static final String HMAC_KEY_NAME = "hmacKey";

    private final RestHmacConfigProperties configProperties;

    public RestHmacUsernamePasswordLoginFilter(String loginPath, RestHmacConfigProperties configProperties) {
        super(loginPath);
        this.configProperties = configProperties;
    }

    @Override
    protected void onAuthenticationSuccess(
            HttpServletRequest request,
            HttpServletResponse response,
            UsernamePasswordAuthenticationToken authentication
    ) throws IOException {
        String generatedSecretKey = HmacUtils.generateEncodedKey(configProperties.getSecretKeyLength());
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonString = objectMapper.writeValueAsString(new AuthTokenProcessingResult<>(
                HMAC_KEY_NAME,generatedSecretKey));

        response.getWriter().write(jsonString);
        response.setStatus(HttpServletResponse.SC_OK);

        clearAuthenticationAttributes(request);
    }

}
