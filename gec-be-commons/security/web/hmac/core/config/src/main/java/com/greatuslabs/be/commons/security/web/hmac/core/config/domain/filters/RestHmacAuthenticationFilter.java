package com.greatuslabs.be.commons.security.web.hmac.core.config.domain.filters;

import com.greatuslabs.be.commons.security.rest.hmac.utilities.HmacMessageBuilder;
import com.greatuslabs.be.commons.security.web.hmac.core.config.domain.dto.RestHmacCredentials;
import com.greatuslabs.be.commons.security.web.hmac.core.config.domain.wrapper.RestHmacToken;
import com.greatuslabs.be.commons.security.web.hmac.core.config.domain.util.HmacRawCredentialsUtility;
import com.greatuslabs.commons.be.security.web.core.internal.filter.AbstractMultipleUrlAuthenticationProcessingFilter;
import com.greatuslabs.commons.be.security.web.core.utilities.CachedHttpServletRequestWrapper;
import com.greatuslabs.commons.be.security.web.core.utilities.DefaultBadCredentialExceptionFactory;
import com.greatuslabs.commons.be.security.web.core.utilities.wrapper.CachedHttpServletRequest;
import com.greatuslabs.commons.be.utilities.date.DateUtility;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RestHmacAuthenticationFilter extends AbstractMultipleUrlAuthenticationProcessingFilter {

    private static final String REQUEST_HEADER_AUTHORIZATION = "Authorization";
    private static final String REQUEST_HEADER_DATE = "Date";

    public RestHmacAuthenticationFilter(String[] urlMatchers) {
        super(urlMatchers);
    }

    @Override
    public Authentication attemptAuthentication(
            HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        // use wrapper to read multiple times the content
        CachedHttpServletRequest cachedRequest =
                CachedHttpServletRequestWrapper.wrapRequest(request);

        // Get authorization header
        String credentialsString = cachedRequest.getHeader(REQUEST_HEADER_AUTHORIZATION);
        String timestamp = cachedRequest.getHeader(REQUEST_HEADER_DATE);

        if(!HmacRawCredentialsUtility.isValidRawCredentials(credentialsString) || !DateUtility.isValidDate(timestamp))
            DefaultBadCredentialExceptionFactory.throwException();

        String messageToSign =
                HmacMessageBuilder.createMessage(b -> {
                    b.requestMethod =   cachedRequest.getMethod();
                    b.payload = cachedRequest.getPayload();
                    b.contentType = cachedRequest.getContentType();
                    b.timestamp = timestamp;
                    b.requestUri = cachedRequest.getRequestURI();
                });

        HmacRawCredentialsUtility.RestHmacRawCredentials rawCredentials =
                HmacRawCredentialsUtility.extractRawCredentials(credentialsString);

        // compose rest credential using cachedRequest data to sign and the signature
        RestHmacCredentials restCredential = new RestHmacCredentials(messageToSign, rawCredentials.signature);

        // Create an authentication token
        RestHmacToken unAuthenticatedToken =
                new RestHmacToken(rawCredentials.publicKey, restCredential, DateUtility.parseDate(timestamp));

        return getAuthenticationManager().authenticate(unAuthenticatedToken);
    }


}
