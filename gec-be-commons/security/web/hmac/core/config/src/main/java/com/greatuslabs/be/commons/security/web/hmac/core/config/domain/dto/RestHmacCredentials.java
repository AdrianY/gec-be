package com.greatuslabs.be.commons.security.web.hmac.core.config.domain.dto;

public class RestHmacCredentials {
    private final String requestData;
    private final String signature;

    public RestHmacCredentials(String requestData, String signature) {
        this.requestData = requestData;
        this.signature = signature;
    }

    public String getRequestData() {
        return requestData;
    }

    public String getSignature() {
        return signature;
    }
}
