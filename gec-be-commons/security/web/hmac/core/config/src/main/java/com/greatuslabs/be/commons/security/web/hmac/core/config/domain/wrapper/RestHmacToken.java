package com.greatuslabs.be.commons.security.web.hmac.core.config.domain.wrapper;

import com.greatuslabs.be.commons.security.web.hmac.core.config.domain.dto.RestHmacCredentials;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Date;

public class RestHmacToken extends UsernamePasswordAuthenticationToken{

    private Date timestamp;

    // this constructor creates a non-authenticated token (see super-class)
    public RestHmacToken(String principal, RestHmacCredentials credentials, Date timestamp) {
        super(principal, credentials);
        this.timestamp = timestamp;
    }

    // this constructor creates an authenticated token (see super-class)
    public RestHmacToken(
            String principal,
            RestHmacCredentials credentials,
            Date timestamp,
            Collection<? extends GrantedAuthority> authorities
    ) {
        super(principal, credentials, authorities);
        this.timestamp = timestamp;
    }

    @Override
    public String getPrincipal() {
        return (String) super.getPrincipal();
    }

    @Override
    public RestHmacCredentials getCredentials() {
        return (RestHmacCredentials) super.getCredentials();
    }

    public Date getTimestamp() {
        return timestamp;
    }
}
