package com.greatuslabs.be.commons.security.web.hmac.core.config.infra.conditions;

import com.greatuslabs.be.commons.security.web.hmac.api.references.RestHmacReferences;
import com.greatuslabs.commons.be.security.web.core.utilities.infra.SecurityUrlMatchersCondition;

public class RestHmacUrlMatchersCondition extends SecurityUrlMatchersCondition {

    private static final String URL_MATCHERS_REST_HMAC = "rest hmac url matchers";

    public RestHmacUrlMatchersCondition() {
        super(c -> {
            c.securityConfigKey = RestHmacReferences.SECURITY_CONFIG_KEY;
            c.urlMatchersGroupName = URL_MATCHERS_REST_HMAC;
        });
    }

}
