package com.greatuslabs.be.commons.security.web.hmac.core.config;

import com.greatuslabs.be.commons.security.core.config.domain.references.CoreSecurityConfigReferences;
import com.greatuslabs.be.commons.security.web.hmac.api.references.RestHmacReferences;
import com.greatuslabs.be.commons.security.web.hmac.api.service.PrincipalSecretKeyService;
import com.greatuslabs.be.commons.security.web.hmac.core.config.domain.dto.RestHmacConfigProperties;
import com.greatuslabs.be.commons.security.web.hmac.core.config.domain.filters.RestHmacAuthenticationFilter;
import com.greatuslabs.be.commons.security.web.hmac.core.config.domain.filters.RestHmacUsernamePasswordLoginFilter;
import com.greatuslabs.be.commons.security.web.hmac.core.config.domain.provider.RestHmacAuthenticationProvider;
import com.greatuslabs.be.commons.security.web.hmac.core.config.domain.services.AuthenticatedPrincipalHmacTokenService;
import com.greatuslabs.be.commons.security.web.hmac.core.config.infra.conditions.RestHmacUrlMatchersCondition;
import com.greatuslabs.commons.be.security.web.core.api.infra.dto.SecurityUrlMatchersRegister;
import com.greatuslabs.commons.be.security.web.core.config.CoreWebSecurityAutoConfiguration;
import com.greatuslabs.commons.be.security.web.core.config.infra.WebAccessAuthenticationConfigRegister;
import com.greatuslabs.commons.be.security.web.core.internal.filter.AbstractUsernamePasswordLoginFilter;
import com.greatuslabs.commons.be.security.web.core.utilities.SecurityUrlMatchersCollector;
import com.greatuslabs.commons.be.security.web.portal.form.config.FormLoginAutoConfiguration;
import com.greatuslabs.commons.be.security.web.portal.form.internal.api.infra.AuthenticatedTokenProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.validation.annotation.Validated;

import java.util.List;

import static com.greatuslabs.be.commons.security.web.hmac.core.config.domain.references.RestHmacConfigReferences.LOGIN_FILTER_PATH;
import static com.greatuslabs.be.commons.security.web.hmac.core.config.domain.references.RestHmacConfigReferences.REST_HMAC_CONFIG_PREFIX;

@Configuration
@AutoConfigureBefore(CoreWebSecurityAutoConfiguration.class)
@ConditionalOnClass({PrincipalSecretKeyService.class, CoreWebSecurityAutoConfiguration.class})
@ConditionalOnProperty(
        prefix = CoreSecurityConfigReferences.CORE_SECURITY_CONFIG_PREFIX,
        value = "enabled",
        matchIfMissing = true
)
public class RestHmacAutoConfiguration {

    @Bean
    @Validated
    @ConfigurationProperties(REST_HMAC_CONFIG_PREFIX)
    public RestHmacConfigProperties restHmacConfigProperties(){
        return new RestHmacConfigProperties();
    }

    @Bean
    @Conditional(RestHmacUrlMatchersCondition.class)
    public RestHmacAuthenticationProvider restHmacAuthenticationProvider(
            PrincipalSecretKeyService principalSecretKeyService, RestHmacConfigProperties restHmacConfigProperties){
        return new RestHmacAuthenticationProvider(principalSecretKeyService, restHmacConfigProperties);
    }

//    @Bean
//    @ConditionalOnClass(AuthenticatedTokenProcessor.class)
//    public AuthenticatedTokenProcessor authenticatedPrincipalHmacTokenService(
//            PrincipalSecretKeyService principalSecretKeyService){
//        return new AuthenticatedPrincipalHmacTokenService(principalSecretKeyService);
//    }

    @Bean
    public WebAccessAuthenticationConfigRegister restHmacSecurityConfigRegister(
            List<SecurityUrlMatchersRegister> securityUrlMatchersRegisterList,
            RestHmacAuthenticationProvider restHmacAuthenticationProvider,
            RestHmacConfigProperties restHmacConfigProperties
    ){
        return new WebAccessAuthenticationConfigRegister() {
            @Override
            public AuthenticationProvider getAuthenticationProvider() {
                return restHmacAuthenticationProvider;
            }

            @Override
            public AbstractAuthenticationProcessingFilter getAuthTokenFilter() {
                String[] urlMatchers =
                        SecurityUrlMatchersCollector.collectUrlMatchers(
                                securityUrlMatchersRegisterList, RestHmacReferences.SECURITY_CONFIG_KEY);

                return new RestHmacAuthenticationFilter(urlMatchers);
            }

            @Override
            public AbstractUsernamePasswordLoginFilter getLoginFilter() {
                return new RestHmacUsernamePasswordLoginFilter(LOGIN_FILTER_PATH, restHmacConfigProperties);
            }
        };
    }
}
