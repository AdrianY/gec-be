package com.greatuslabs.be.commons.security.web.hmac.core.config.domain.util;

import com.greatuslabs.be.commons.security.web.hmac.core.config.domain.exceptions.RestHmacAuthenticationException;
import org.apache.commons.lang3.StringUtils;

public final class HmacRawCredentialsUtility {

    private static final String CREDENTIALS_DELIMITER = ":";
    private static final String PATTERN_AUTHORIZATION_HEADER = "^[A-Za-z0-9]+:[A-Za-z0-9]+";

    private HmacRawCredentialsUtility(){}

    public static RestHmacRawCredentials extractRawCredentials(String credentialsString) throws RestHmacAuthenticationException {
        // Authorization header is in the form <public_access_key>:<signature>
        if(!isValidRawCredentials(credentialsString))
            throw new RestHmacAuthenticationException("Invalid username or password");

        String[] rawCredentials = credentialsString.split(CREDENTIALS_DELIMITER);
        return new RestHmacRawCredentials(rawCredentials[1], rawCredentials[0]);
    }

    public static boolean isValidRawCredentials(String credentialsString){
        return StringUtils.isNotEmpty(credentialsString) && credentialsString.matches(PATTERN_AUTHORIZATION_HEADER);
    }

    public static class RestHmacRawCredentials{
        public final String signature;
        public final String publicKey;

        private RestHmacRawCredentials(String signature, String publicKey) {
            this.signature = signature;
            this.publicKey = publicKey;
        }
    }
}
