package com.greatuslabs.be.commons.security.web.hmac.core.config.domain.provider;

import com.greatuslabs.be.commons.security.rest.hmac.utilities.HmacUtils;
import com.greatuslabs.be.commons.security.web.hmac.api.service.PrincipalSecretKeyService;
import com.greatuslabs.be.commons.security.web.hmac.core.config.domain.dto.RestHmacConfigProperties;
import com.greatuslabs.be.commons.security.web.hmac.core.config.domain.dto.RestHmacCredentials;
import com.greatuslabs.be.commons.security.web.hmac.core.config.domain.wrapper.RestHmacToken;
import com.greatuslabs.commons.be.security.web.core.utilities.DefaultBadCredentialExceptionFactory;
import com.greatuslabs.commons.be.utilities.date.DateUtility;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;


public class RestHmacAuthenticationProvider implements AuthenticationProvider{

    private final PrincipalSecretKeyService principalSecretKeyService;
    private final RestHmacConfigProperties restHmacConfigProperties;

    public RestHmacAuthenticationProvider(
            PrincipalSecretKeyService principalSecretKeyService, RestHmacConfigProperties restHmacConfigProperties) {
        this.principalSecretKeyService = principalSecretKeyService;
        this.restHmacConfigProperties = restHmacConfigProperties;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        RestHmacToken authToken = (RestHmacToken) authentication;

        // api key (username)
        String apiKey = authToken.getPrincipal();

        RestHmacCredentials credentials = authToken.getCredentials();

        String secret = retrieveSecretKey(apiKey);

        validateHmacSignature(secret, credentials);

        validateTimestamp(authToken.getTimestamp());

        // return new fully authenticated token, with the "authenticated" flag set to true
        RestHmacToken successfulAuthToken =
                new RestHmacToken(apiKey, credentials, authToken.getTimestamp(), new HashSet<>());

        SecurityContextHolder.getContext().setAuthentication(successfulAuthToken);
        return successfulAuthToken;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return RestHmacToken.class.equals(authentication);
    }

    private String retrieveSecretKey(String apiKey){
        String secret = principalSecretKeyService.getSecretKey(apiKey);

        if (Objects.isNull(secret))
            DefaultBadCredentialExceptionFactory.throwException();


        return secret;
    }

    private void validateHmacSignature(String apiSecret, RestHmacCredentials credentials){
        String hmac = HmacUtils.calculateHMAC(apiSecret, credentials.getRequestData());

        if (!credentials.getSignature().equals(hmac))
            DefaultBadCredentialExceptionFactory.throwException();
    }

    private void validateTimestamp(Date timestamp) {

        LocalDateTime requestLocalDateTime = DateUtility.toLocalDateTime(timestamp);

        Long timestampMinutedExpiration = restHmacConfigProperties.getTimestampExpirationInMinutes().longValue();

        LocalDateTime lastHourTime = LocalDateTime.now(ZoneOffset.UTC).minusMinutes(timestampMinutedExpiration);
        if (!requestLocalDateTime.isAfter(lastHourTime))
            DefaultBadCredentialExceptionFactory.throwException();

    }

}
