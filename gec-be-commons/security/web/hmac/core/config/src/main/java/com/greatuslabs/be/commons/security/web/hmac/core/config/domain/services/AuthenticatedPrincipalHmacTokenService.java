package com.greatuslabs.be.commons.security.web.hmac.core.config.domain.services;

import com.greatuslabs.be.commons.security.rest.hmac.utilities.HmacUtils;
import com.greatuslabs.be.commons.security.web.hmac.api.service.PrincipalSecretKeyService;
import com.greatuslabs.commons.be.security.web.portal.form.internal.api.dto.AuthTokenProcessingResult;
import com.greatuslabs.commons.be.security.web.portal.form.internal.api.infra.AuthenticatedTokenProcessor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

public class AuthenticatedPrincipalHmacTokenService implements AuthenticatedTokenProcessor {

    private final PrincipalSecretKeyService principalSecretKeyService;

    public AuthenticatedPrincipalHmacTokenService(PrincipalSecretKeyService principalSecretKeyService) {
        this.principalSecretKeyService = principalSecretKeyService;
    }

    @Override
    public AuthTokenProcessingResult<String> processToken(UsernamePasswordAuthenticationToken authentication) {
        String generatedSecretKey = HmacUtils.generateEncodedKey(40);
        return new AuthTokenProcessingResult<>(
                "hmacKey",generatedSecretKey);
    }
}
