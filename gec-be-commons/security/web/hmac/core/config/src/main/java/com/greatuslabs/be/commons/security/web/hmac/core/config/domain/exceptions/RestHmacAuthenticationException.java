package com.greatuslabs.be.commons.security.web.hmac.core.config.domain.exceptions;


import org.springframework.security.core.AuthenticationException;

public class RestHmacAuthenticationException extends AuthenticationException {
    public RestHmacAuthenticationException(String explanation) {
        super(explanation);
    }
}
