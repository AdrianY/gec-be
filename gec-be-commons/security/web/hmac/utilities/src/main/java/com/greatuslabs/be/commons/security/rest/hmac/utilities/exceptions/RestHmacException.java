package com.greatuslabs.be.commons.security.rest.hmac.utilities.exceptions;

public class RestHmacException extends RuntimeException{


    public RestHmacException(Throwable cause) {
        super(cause);
    }

}
