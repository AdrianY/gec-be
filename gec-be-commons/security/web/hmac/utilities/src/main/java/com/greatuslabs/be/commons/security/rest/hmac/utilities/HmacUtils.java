package com.greatuslabs.be.commons.security.rest.hmac.utilities;



import com.greatuslabs.be.commons.security.rest.hmac.utilities.exceptions.RestHmacException;

import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidParameterException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import static org.apache.commons.codec.digest.DigestUtils.md5Hex;

public final class HmacUtils {
    private static final String HMAC_SHA_256 = "HmacSHA256";
    private static final String CHARSET_NAME_UTF8 = "UTF-8";

    private HmacUtils(){}

    /**
     * Generates an HMAC key
     *
     * @param length The length of the key in bits
     * @return The Base64-encoded key
     */
    public static byte[] generateKey(int length) {
        try {
            KeyGenerator generator = KeyGenerator.getInstance(HMAC_SHA_256);
            generator.init(length);
            return generator.generateKey().getEncoded();
        } catch (NoSuchAlgorithmException | InvalidParameterException ex) {
            throw new RestHmacException(ex);
        }
    }

    /**
     * Generates a Base64-encoded HMAC key
     *
     * @param length The length of the key in bits
     * @return The Base64-encoded key
     */
    public static String generateEncodedKey(int length) {
        byte[] rawKey = generateKey(length);
        return encode(rawKey);
    }

    /**
     * Calculates HMAC for the data with the given the secret key
     *
     * @param secret The secret key
     * @param data   The message to be hashed
     * @return The HMAC
     */
    public static String calculateHMAC(String secret, String data) {
        try {
            SecretKeySpec signingKey = new SecretKeySpec(secret.getBytes(CHARSET_NAME_UTF8), HMAC_SHA_256);
            Mac mac = Mac.getInstance(HMAC_SHA_256);
            mac.init(signingKey);
            byte[] rawHmac = mac.doFinal(data.getBytes());
            return encode(rawHmac);
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static String md5Hash(String message) {
        return md5Hex(message);
    }

    private static String encode(byte[] value) {
        return Base64.getEncoder().encodeToString(value);
    }
}
