package com.greatuslabs.be.commons.security.rest.hmac.utilities;

import java.util.Set;
import java.util.TreeSet;
import java.util.function.Consumer;

public final class HmacMessageBuilder {
    private static final Set<String> METHOD_HAS_CONTENT = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER) {
        private static final long serialVersionUID = 1L;

        {
            add("POST");
            add("PUT");
            add("PATCH");
        }
    };
    private static final String STRING_NEXT_LINE = "\n";
    private static final String STRING_EMPTY = "";

    private HmacMessageBuilder(){}

    @SuppressWarnings("StringBufferReplaceableByString")
    public static String createMessage(Consumer<HmacMessageComponents> componentsBuilder) {
        HmacMessageComponents componentsHolder = new HmacMessageComponents();
        componentsBuilder.accept(componentsHolder);
        String requestMethod = componentsHolder.requestMethod;
        boolean hasContentBody = METHOD_HAS_CONTENT.contains(requestMethod);
        return new StringBuilder()
                .append(requestMethod).append(STRING_NEXT_LINE)
                .append(hasContentBody ? HmacUtils.md5Hash(componentsHolder.payload) + STRING_NEXT_LINE : STRING_EMPTY)
                .append(hasContentBody ? componentsHolder.contentType + STRING_NEXT_LINE : STRING_EMPTY)
                .append(componentsHolder.timestamp).append(STRING_NEXT_LINE)
                .append(componentsHolder.requestUri)
                .toString();
    }

    public static class HmacMessageComponents{
        public String payload;
        public String requestMethod;
        public String contentType;
        public String timestamp;
        public String requestUri;

        private HmacMessageComponents(){}
    }
}
