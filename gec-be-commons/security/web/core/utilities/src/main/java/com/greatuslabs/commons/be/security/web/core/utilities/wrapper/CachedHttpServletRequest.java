package com.greatuslabs.commons.be.security.web.core.utilities.wrapper;

import com.greatuslabs.commons.be.security.web.core.utilities.dto.CustomServletInputStream;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class CachedHttpServletRequest extends HttpServletRequestWrapper{

    private final String payload;

    public CachedHttpServletRequest(
            HttpServletRequest request, String payload) {
        super(request);
        this.payload = payload;
    }

    @Override
    public ServletInputStream getInputStream () {
        return new CustomServletInputStream(payload);
    }

    public String getPayload() {
        return payload;
    }
}
