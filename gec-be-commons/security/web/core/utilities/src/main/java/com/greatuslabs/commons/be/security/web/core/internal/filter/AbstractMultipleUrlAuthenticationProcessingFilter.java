package com.greatuslabs.commons.be.security.web.core.internal.filter;

import com.greatuslabs.commons.be.security.web.core.internal.exceptions.WebSecurityFilterConstructionException;
import com.greatuslabs.commons.be.utilities.array.ArrayUtility;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class AbstractMultipleUrlAuthenticationProcessingFilter
        extends AbstractAuthenticationProcessingFilter {

    public AbstractMultipleUrlAuthenticationProcessingFilter(String defaultUrlMatcher) {
        super(defaultUrlMatcher);
    }

    public AbstractMultipleUrlAuthenticationProcessingFilter(String[] urlMatchers){
        super(createRequestMatcher(urlMatchers));
    }

    private static OrRequestMatcher createRequestMatcher(String[] urlMatchers){
        if(ArrayUtils.isEmpty(urlMatchers))
            throw new WebSecurityFilterConstructionException("url matchers cannot be empty");

        List<AntPathRequestMatcher> matchers =
                Stream.of(urlMatchers).map(AntPathRequestMatcher::new).collect(Collectors.toList());

        return new OrRequestMatcher(ArrayUtility.toArray(AntPathRequestMatcher.class, matchers));
    }
}
