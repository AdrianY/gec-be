package com.greatuslabs.commons.be.security.web.core.utilities.dto;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import java.io.ByteArrayInputStream;

public class CustomServletInputStream extends ServletInputStream{

    private final ByteArrayInputStream byteArrayInputStream;

    public CustomServletInputStream(String payload){
        byteArrayInputStream = new ByteArrayInputStream(payload.getBytes());
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public boolean isReady() {
        return false;
    }

    @Override
    public void setReadListener(ReadListener readListener) {

    }

    @Override
    public int read() {
        return byteArrayInputStream.read();
    }
}
