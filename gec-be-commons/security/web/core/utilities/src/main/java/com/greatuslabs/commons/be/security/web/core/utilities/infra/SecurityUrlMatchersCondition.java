package com.greatuslabs.commons.be.security.web.core.utilities.infra;

import com.greatuslabs.commons.be.security.web.core.api.infra.dto.SecurityUrlMatchersRegister;
import com.greatuslabs.commons.be.security.web.core.utilities.SecurityUrlMatchersCollector;
import com.greatuslabs.commons.be.utilities.array.ArrayUtility;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.boot.autoconfigure.condition.ConditionMessage;
import org.springframework.boot.autoconfigure.condition.ConditionOutcome;
import org.springframework.boot.autoconfigure.condition.SpringBootCondition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * TODO: Refactor, allow caching of resolved urls
 */
public abstract class SecurityUrlMatchersCondition extends SpringBootCondition {

    private static final String URL_MATCHERS_GENERIC = "url matchers";
    private static final String CONDITION_NAME_REST_HMAC_URL_MATCHERS = "RestHmacUrlMatchers";

    private final String urlMatchersGroupName;
    private final String securityConfigKey;

    protected SecurityUrlMatchersCondition(Consumer<SecurityUrlMatchersConditionDetails> builder) {
        SecurityUrlMatchersConditionDetails conditionDetails = new SecurityUrlMatchersConditionDetails();
        builder.accept(conditionDetails);
        securityConfigKey = conditionDetails.securityConfigKey;
        urlMatchersGroupName = conditionDetails.urlMatchersGroupName;
    }

    @Override
    public ConditionOutcome getMatchOutcome(ConditionContext context, AnnotatedTypeMetadata metadata) {

        List<SecurityUrlMatchersRegister> urlMatchersRegistrarList = getSecurityUrlMatchersRegisters(context);

        ConditionMessage.Builder message = ConditionMessage.forCondition(CONDITION_NAME_REST_HMAC_URL_MATCHERS);

        if(CollectionUtils.isEmpty(urlMatchersRegistrarList))
            return ConditionOutcome.noMatch(
                    message.didNotFind(URL_MATCHERS_GENERIC).atAll());

        String[] urlMatchers =
                SecurityUrlMatchersCollector.collectUrlMatchers(
                        urlMatchersRegistrarList, securityConfigKey);

        if(ArrayUtils.isEmpty(urlMatchers))
            return ConditionOutcome.noMatch(
                    message.didNotFind(urlMatchersGroupName).atAll());



        return ConditionOutcome.match(message.found(urlMatchersGroupName)
                .items(ConditionMessage.Style.NORMAL, ArrayUtility.getArray(urlMatchers)));
    }

    @SuppressWarnings("unchecked")
    private List<SecurityUrlMatchersRegister> getSecurityUrlMatchersRegisters(ConditionContext context) {
        try{
            ConfigurableListableBeanFactory beanFactory = context.getBeanFactory();

            Map<String, SecurityUrlMatchersRegister> beansOfType =
                    beanFactory.getBeansOfType(SecurityUrlMatchersRegister.class);

            if(MapUtils.isEmpty(beansOfType))
                return null;

            return beansOfType.entrySet().stream().map(Map.Entry::getValue).collect(Collectors.toList());
        }catch(NoSuchBeanDefinitionException e){
            return null;
        }
    }

    public static class SecurityUrlMatchersConditionDetails {
        public String urlMatchersGroupName;
        public String securityConfigKey;

        private SecurityUrlMatchersConditionDetails() {
        }
    }
}
