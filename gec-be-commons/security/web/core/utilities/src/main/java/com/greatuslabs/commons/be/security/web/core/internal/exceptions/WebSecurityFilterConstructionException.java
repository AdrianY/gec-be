package com.greatuslabs.commons.be.security.web.core.internal.exceptions;

public class WebSecurityFilterConstructionException extends RuntimeException{
    public WebSecurityFilterConstructionException(String message) {
        super(message);
    }
}
