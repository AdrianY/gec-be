package com.greatuslabs.commons.be.security.web.core.utilities.exceptions;

import org.springframework.security.core.AuthenticationException;

public class WebAuthenticationException extends AuthenticationException {
    public WebAuthenticationException(String msg, Throwable t) {
        super(msg, t);
    }

    public WebAuthenticationException(String msg) {
        super(msg);
    }
}
