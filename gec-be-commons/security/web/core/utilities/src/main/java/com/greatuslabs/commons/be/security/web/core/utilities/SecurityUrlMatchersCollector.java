package com.greatuslabs.commons.be.security.web.core.utilities;

import com.greatuslabs.commons.be.security.web.core.api.infra.dto.SecurityUrlMatchersRegister;
import com.greatuslabs.commons.be.security.web.core.utilities.exceptions.SecurityUrlMatchersCollectorException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public final class SecurityUrlMatchersCollector {
    private SecurityUrlMatchersCollector(){}

    public static String[] collectUrlMatchers(
            List<SecurityUrlMatchersRegister> registers, String securityConfigKey) {
        if(CollectionUtils.isEmpty(registers))
            throw new SecurityUrlMatchersCollectorException("registers cannot be empty");

        if(StringUtils.isEmpty(securityConfigKey))
            throw new SecurityUrlMatchersCollectorException("securityConfigKey cannot be empty");

        List<String> urlMatchers = registers.stream()
                .filter(r -> securityConfigKey.equals(r.getTargetSecurityConfigKey()))
                .flatMap(r -> Arrays.stream(r.getUrlMatchersToScan()))
                .collect(Collectors.toList());

        if(CollectionUtils.isEmpty(urlMatchers)) return new String[0];

        return urlMatchers.toArray(new String[urlMatchers.size()]);
    }
}
