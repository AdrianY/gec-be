package com.greatuslabs.commons.be.security.web.core.internal.filter;

import com.greatuslabs.commons.be.security.web.core.utilities.DefaultBadCredentialExceptionFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Objects;

public abstract class AbstractUsernamePasswordLoginFilter extends AbstractAuthenticationProcessingFilter {

    protected AbstractUsernamePasswordLoginFilter(String loginPath) {
        super(new AntPathRequestMatcher(loginPath, "POST"));
        setAuthenticationSuccessHandler(this::onAuthenticationSuccess);
    }

    private void onAuthenticationSuccess(
            HttpServletRequest request,
            HttpServletResponse response,
            Authentication authentication
    ) throws IOException, ServletException{
        onAuthenticationSuccess(request, response, (UsernamePasswordAuthenticationToken) authentication);
    }

    protected abstract void onAuthenticationSuccess(
            HttpServletRequest request,
            HttpServletResponse response,
            UsernamePasswordAuthenticationToken authentication
    ) throws IOException, ServletException;

    //TODO improve this on par with UsernamePasswordAuthenticationFilter
    @Override
    public Authentication attemptAuthentication(
            HttpServletRequest request,
            HttpServletResponse response
    ) throws AuthenticationException {

        String username = request.getParameter("username");
        String password = request.getParameter("password");

        if(Objects.isNull(username) || Objects.isNull(password))
            DefaultBadCredentialExceptionFactory.throwException();

        UsernamePasswordAuthenticationToken authToken = new
                UsernamePasswordAuthenticationToken(username, password);

        return getAuthenticationManager().authenticate(authToken);
    }

    protected final void clearAuthenticationAttributes(HttpServletRequest request) {
        HttpSession session = request.getSession(false);

        if (Objects.nonNull(session))
            session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    }
}
