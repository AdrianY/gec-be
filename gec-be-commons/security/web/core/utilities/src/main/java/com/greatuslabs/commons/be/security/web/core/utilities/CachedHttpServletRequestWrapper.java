package com.greatuslabs.commons.be.security.web.core.utilities;


import com.greatuslabs.commons.be.security.web.core.utilities.exceptions.WebAuthenticationException;
import com.greatuslabs.commons.be.security.web.core.utilities.wrapper.CachedHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public final class CachedHttpServletRequestWrapper {

    private CachedHttpServletRequestWrapper(){}

    public static CachedHttpServletRequest wrapRequest(HttpServletRequest request){
        return new CachedHttpServletRequestWrapper().executeWrapping(request);
    }

    public CachedHttpServletRequest executeWrapping(HttpServletRequest request) {
        String payload = stringifyPayload(request);
        return new CachedHttpServletRequest(request, payload);
    }

    private String stringifyPayload(HttpServletRequest request) {
        // read the original payload into the payload variable
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = null;
        try {
            // read the payload into the StringBuilder
            InputStream inputStream = request.getInputStream();
            if (inputStream != null) {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                char[] charBuffer = new char[128];
                int bytesRead;
                while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                    stringBuilder.append(charBuffer, 0, bytesRead);
                }
            } else {
                // make an empty string since there is no payload
                stringBuilder.append("");
            }
        } catch (IOException ex) {
            throw new WebAuthenticationException("Error reading the request payload", ex);
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException iox) {
                    // ignore
                }
            }
        }

        return stringBuilder.toString();
    }
}
