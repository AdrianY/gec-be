package com.greatuslabs.commons.be.security.web.core.utilities;

import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.SpringSecurityMessageSource;

public final class DefaultBadCredentialExceptionFactory {

    private static final MessageSourceAccessor messages = SpringSecurityMessageSource.getAccessor();

    private static final String DEFAULT_BAD_CREDENTIALS_MESSAGE = "Bad credentials";
    private static final String MESSAGE_CODE = "AbstractUserDetailsAuthenticationProvider.badCredentials";

    private DefaultBadCredentialExceptionFactory(){}

    public static void throwException(){
        throw new BadCredentialsException(messages.getMessage(
                MESSAGE_CODE,
                DEFAULT_BAD_CREDENTIALS_MESSAGE
        ));
    }
}
