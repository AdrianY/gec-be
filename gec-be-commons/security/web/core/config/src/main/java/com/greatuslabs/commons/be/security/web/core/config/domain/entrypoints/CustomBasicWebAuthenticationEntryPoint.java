package com.greatuslabs.commons.be.security.web.core.config.domain.entrypoints;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class CustomBasicWebAuthenticationEntryPoint extends BasicAuthenticationEntryPoint{

    private static final String SECURE_REALM = "Secure realm";

    public CustomBasicWebAuthenticationEntryPoint() {
        setRealmName(SECURE_REALM);
    }

    @Override
    public void commence(
            HttpServletRequest request,
            HttpServletResponse response,
            AuthenticationException authException
    ) throws IOException {

        //HMACDigest
        response.addHeader("WWW-Authenticate", "Realm=\"" + getRealmName() + "\"");
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        PrintWriter writer = response.getWriter();
        writer.println("HTTP Status " + HttpServletResponse.SC_UNAUTHORIZED + " - " + authException.getMessage());
    }
}
