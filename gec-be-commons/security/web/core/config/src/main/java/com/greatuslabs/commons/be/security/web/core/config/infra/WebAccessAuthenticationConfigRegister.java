package com.greatuslabs.commons.be.security.web.core.config.infra;

import com.greatuslabs.commons.be.security.web.core.internal.filter.AbstractUsernamePasswordLoginFilter;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.Filter;

public interface WebAccessAuthenticationConfigRegister {

    AuthenticationProvider getAuthenticationProvider();

    AbstractAuthenticationProcessingFilter getAuthTokenFilter();

    AbstractUsernamePasswordLoginFilter getLoginFilter();

}
