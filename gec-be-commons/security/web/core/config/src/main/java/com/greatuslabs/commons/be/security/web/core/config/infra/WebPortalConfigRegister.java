package com.greatuslabs.commons.be.security.web.core.config.infra;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;

public interface WebPortalConfigRegister {
    void config(HttpSecurity http) throws Exception;
}
