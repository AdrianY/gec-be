package com.greatuslabs.commons.be.security.web.core.config.domain.services;

public interface SecurityUrlMatchersRegistry {
    String[] collectUrlMatchers(String securityConfigKey);


}
