package com.greatuslabs.commons.be.security.web.core.config.domain.exceptions;

public class SecurityUrlMatchersCollectorException extends RuntimeException{

    public SecurityUrlMatchersCollectorException() {
    }

    public SecurityUrlMatchersCollectorException(String message) {
        super(message);
    }

    public SecurityUrlMatchersCollectorException(String message, Throwable cause) {
        super(message, cause);
    }

    public SecurityUrlMatchersCollectorException(Throwable cause) {
        super(cause);
    }
}
