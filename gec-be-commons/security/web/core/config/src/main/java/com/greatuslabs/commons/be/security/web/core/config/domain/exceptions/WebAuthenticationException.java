package com.greatuslabs.commons.be.security.web.core.config.domain.exceptions;

import org.springframework.security.core.AuthenticationException;

public class WebAuthenticationException extends AuthenticationException {
    public WebAuthenticationException(String msg, Throwable t) {
        super(msg, t);
    }

    public WebAuthenticationException(String msg) {
        super(msg);
    }
}
