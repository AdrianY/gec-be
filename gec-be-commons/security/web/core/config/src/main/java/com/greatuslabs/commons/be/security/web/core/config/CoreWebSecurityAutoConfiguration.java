package com.greatuslabs.commons.be.security.web.core.config;

import com.greatuslabs.be.commons.security.core.config.CoreSecurityAutoConfiguration;
import com.greatuslabs.be.commons.security.core.config.domain.references.CoreSecurityConfigReferences;
import com.greatuslabs.be.commons.security.core.config.domain.services.CustomUserDetailsService;
import com.greatuslabs.commons.be.security.web.core.config.domain.entrypoints.CustomBasicWebAuthenticationEntryPoint;
import com.greatuslabs.commons.be.security.web.core.config.domain.handlers.SuccessfulAuthenticationHandler;
import com.greatuslabs.commons.be.security.web.core.config.domain.providers.CustomDaoAuthenticationProvider;
import com.greatuslabs.commons.be.security.web.core.config.infra.WebAccessAuthenticationConfigRegister;
import com.greatuslabs.commons.be.security.web.core.config.infra.WebPortalConfigRegister;
import com.greatuslabs.commons.be.security.web.core.internal.filter.AbstractUsernamePasswordLoginFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import java.util.List;
import java.util.Objects;
import java.util.Optional;


@ConditionalOnClass(CoreSecurityAutoConfiguration.class)
@ConditionalOnBean(CoreSecurityAutoConfiguration.class)
@ConditionalOnProperty(
        prefix = CoreSecurityConfigReferences.CORE_SECURITY_CONFIG_PREFIX,
        value = "enabled",
        matchIfMissing = true
)
@AutoConfigureAfter(CoreSecurityAutoConfiguration.class)
@EnableWebSecurity
public class CoreWebSecurityAutoConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private List<WebAccessAuthenticationConfigRegister> webAccessAuthenticationConfigRegisterList;

    @Autowired
    private CustomDaoAuthenticationProvider customDaoAuthenticationProvider;

    @Override
    public void configure(AuthenticationManagerBuilder authBuilder) {
        for(WebAccessAuthenticationConfigRegister configRegister: webAccessAuthenticationConfigRegisterList){
            Optional.ofNullable(configRegister.getAuthenticationProvider())
                    .ifPresent(authBuilder::authenticationProvider);


        }

        //TODO fix tokens by removing direct inheritance of other tokens from UsernamePasswordAuthenticationProvider
        //configure dao auth provider which supports UsernamePasswordAuthenticationProvider
        //TODO set password encoder
        authBuilder.authenticationProvider(customDaoAuthenticationProvider);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();

        for(WebAccessAuthenticationConfigRegister configRegister: webAccessAuthenticationConfigRegisterList){
            bootstrapHttpFilter(http, configRegister.getLoginFilter());
            bootstrapHttpFilter(http, configRegister.getAuthTokenFilter());
        }

        http.authorizeRequests().anyRequest().authenticated()
            .and().exceptionHandling().authenticationEntryPoint(new CustomBasicWebAuthenticationEntryPoint())
            .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    private void bootstrapHttpFilter(
            HttpSecurity http, AbstractAuthenticationProcessingFilter abstractAuthFilter) throws Exception {
        AbstractAuthenticationProcessingFilter loginFilter = Objects.requireNonNull(abstractAuthFilter);
        http.addFilterBefore(loginFilter, UsernamePasswordAuthenticationFilter.class);
        loginFilter.setAuthenticationManager(authenticationManagerBean());
    }

    public void configure(WebSecurity web) {
        web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**");
    }


    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    @ConditionalOnBean(BasicAuthenticationFilter.class)
    public FilterRegistrationBean basicAuthenticationFilterRegistration(BasicAuthenticationFilter filter) {
        FilterRegistrationBean registration = new FilterRegistrationBean(filter);
        registration.setEnabled(false);
        return registration;
    }

//    @Bean
//    public SuccessfulAuthenticationHandler successfulAuthenticationHandler(){
//        return new SuccessfulAuthenticationHandler();
//    }
//
//    @Bean
//    public SimpleUrlAuthenticationFailureHandler defaultFailureHandler(){
//        return new SimpleUrlAuthenticationFailureHandler();
//    }

    @Bean
    public CustomDaoAuthenticationProvider customDaoAuthenticationProvider(
            CustomUserDetailsService customUserDetailsService){
        return new CustomDaoAuthenticationProvider(customUserDetailsService);
    }
}
