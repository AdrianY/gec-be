package com.greatuslabs.commons.be.security.web.core.api.infra.dto;

public interface SecurityUrlMatchersRegister {
    String getTargetSecurityConfigKey();

    String[] getUrlMatchersToScan();
}
