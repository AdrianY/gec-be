package com.greatuslabs.be.commons.security.web.basic.api.references;

public interface WebBasicSecurityReferences {
    String SECURITY_CONFIG_KEY = "webBasicSecurityKey";
}
