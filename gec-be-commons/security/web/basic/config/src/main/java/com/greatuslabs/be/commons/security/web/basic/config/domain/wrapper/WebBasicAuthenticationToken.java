package com.greatuslabs.be.commons.security.web.basic.config.domain.wrapper;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class WebBasicAuthenticationToken extends UsernamePasswordAuthenticationToken {

    public WebBasicAuthenticationToken(String username, String password) {
        super(username, password);
    }

    public WebBasicAuthenticationToken(
            String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    public String getUsername(){
        return (String) super.getPrincipal();
    }

    public String getPassword(){
        return (String) super.getCredentials();
    }
}
