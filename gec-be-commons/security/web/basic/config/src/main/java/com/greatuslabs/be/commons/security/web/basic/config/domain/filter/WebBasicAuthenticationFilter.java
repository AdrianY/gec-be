package com.greatuslabs.be.commons.security.web.basic.config.domain.filter;

import com.greatuslabs.be.commons.security.web.basic.config.domain.wrapper.WebBasicAuthenticationToken;
import com.greatuslabs.commons.be.security.web.core.internal.filter.AbstractMultipleUrlAuthenticationProcessingFilter;
import com.greatuslabs.commons.be.security.web.core.utilities.DefaultBadCredentialExceptionFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

public class WebBasicAuthenticationFilter extends AbstractMultipleUrlAuthenticationProcessingFilter {

    public WebBasicAuthenticationFilter(String[] urlMatchers) {
        super(urlMatchers);
    }

    @Override
    public Authentication attemptAuthentication(
            HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {

        String username = request.getHeader("username");
        String password = request.getHeader("password");

        if(Objects.isNull(username) || Objects.isNull(password))
            DefaultBadCredentialExceptionFactory.throwException();

        return new WebBasicAuthenticationToken(username, password);
    }
}
