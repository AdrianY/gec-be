package com.greatuslabs.be.commons.security.web.basic.config.domain.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.greatuslabs.commons.be.security.web.core.internal.filter.AbstractUsernamePasswordLoginFilter;
import com.greatuslabs.commons.be.security.web.portal.form.internal.api.dto.AuthTokenProcessingResult;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class WebBasicUsernamePasswordLoginFilter extends AbstractUsernamePasswordLoginFilter {

    private static final String WEB_BASIC_KEY_NAME = "basicKey";

    public WebBasicUsernamePasswordLoginFilter(String loginPath) {
        super(loginPath);
    }

    @Override
    protected void onAuthenticationSuccess(
            HttpServletRequest request,
            HttpServletResponse response,
            UsernamePasswordAuthenticationToken authentication
    ) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonString = objectMapper.writeValueAsString(new AuthTokenProcessingResult<>(
                WEB_BASIC_KEY_NAME,"Web Basic"));

        response.getWriter().write(jsonString);
        response.setStatus(HttpServletResponse.SC_OK);

        clearAuthenticationAttributes(request);
    }

}
