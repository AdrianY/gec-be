package com.greatuslabs.be.commons.security.web.basic.config.infra.conditions;

import com.greatuslabs.be.commons.security.web.basic.api.references.WebBasicSecurityReferences;
import com.greatuslabs.commons.be.security.web.core.utilities.infra.SecurityUrlMatchersCondition;


public class BasicSecurityUrlMatchersCondition extends SecurityUrlMatchersCondition {

    private static final String URL_MATCHERS_GROUP_NAME = "web basic security url matchers";

    protected BasicSecurityUrlMatchersCondition() {
        super(c -> {
            c.securityConfigKey = WebBasicSecurityReferences.SECURITY_CONFIG_KEY;
            c.urlMatchersGroupName = URL_MATCHERS_GROUP_NAME;
        });
    }
}
