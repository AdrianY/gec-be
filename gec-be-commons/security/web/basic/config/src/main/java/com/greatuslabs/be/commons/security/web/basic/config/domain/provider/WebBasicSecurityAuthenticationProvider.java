package com.greatuslabs.be.commons.security.web.basic.config.domain.provider;

import com.greatuslabs.be.commons.security.core.api.entities.UserAccount;
import com.greatuslabs.be.commons.security.core.config.domain.services.CustomUserDetailsService;
import com.greatuslabs.be.commons.security.web.basic.config.domain.wrapper.WebBasicAuthenticationToken;
import com.greatuslabs.commons.be.security.web.core.utilities.DefaultBadCredentialExceptionFactory;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.HashSet;
import java.util.Objects;

public class WebBasicSecurityAuthenticationProvider  extends DaoAuthenticationProvider {

    private final CustomUserDetailsService customUserDetailsService;

    public WebBasicSecurityAuthenticationProvider(CustomUserDetailsService customUserDetailsService) {
        this.customUserDetailsService = customUserDetailsService;
        setUserDetailsService(customUserDetailsService);
    }

    @Override
    public Authentication authenticate(Authentication auth)
            throws AuthenticationException {

        WebBasicAuthenticationToken authToken = (WebBasicAuthenticationToken) auth;

        UserAccount user = customUserDetailsService.retrieveByUsername(authToken.getUsername());

        if (Objects.isNull(user) || !validPassword(user, authToken.getPassword()))
            DefaultBadCredentialExceptionFactory.throwException();


        WebBasicAuthenticationToken successfulAuthToken = new WebBasicAuthenticationToken(
                authToken.getUsername(),
                authToken.getPassword(),
                new HashSet<>()
        );

        SecurityContextHolder.getContext().setAuthentication(successfulAuthToken);

        return successfulAuthToken;
    }

    //TODO refactor using BCrypt
    private boolean validPassword(UserAccount user, String password) {
        return user.getPassword().equals(password);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return WebBasicAuthenticationToken.class.equals(authentication);
    }
}
