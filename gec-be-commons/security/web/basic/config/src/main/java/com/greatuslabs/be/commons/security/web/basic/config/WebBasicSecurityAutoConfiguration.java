package com.greatuslabs.be.commons.security.web.basic.config;

import com.greatuslabs.be.commons.security.core.config.domain.references.CoreSecurityConfigReferences;
import com.greatuslabs.be.commons.security.core.config.domain.services.CustomUserDetailsService;
import com.greatuslabs.be.commons.security.web.basic.api.references.WebBasicSecurityReferences;
import com.greatuslabs.be.commons.security.web.basic.config.domain.filter.WebBasicAuthenticationFilter;
import com.greatuslabs.be.commons.security.web.basic.config.domain.filter.WebBasicUsernamePasswordLoginFilter;
import com.greatuslabs.be.commons.security.web.basic.config.domain.provider.WebBasicSecurityAuthenticationProvider;
import com.greatuslabs.be.commons.security.web.basic.config.infra.conditions.BasicSecurityUrlMatchersCondition;
import com.greatuslabs.commons.be.security.web.core.api.infra.dto.SecurityUrlMatchersRegister;
import com.greatuslabs.commons.be.security.web.core.config.CoreWebSecurityAutoConfiguration;
import com.greatuslabs.commons.be.security.web.core.config.infra.WebAccessAuthenticationConfigRegister;
import com.greatuslabs.commons.be.security.web.core.internal.filter.AbstractUsernamePasswordLoginFilter;
import com.greatuslabs.commons.be.security.web.core.utilities.SecurityUrlMatchersCollector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import java.util.List;


@Configuration
@ConditionalOnClass(CoreWebSecurityAutoConfiguration.class)
@ConditionalOnProperty(
        prefix = CoreSecurityConfigReferences.CORE_SECURITY_CONFIG_PREFIX,
        value = "enabled",
        matchIfMissing = true
)
@AutoConfigureBefore(CoreWebSecurityAutoConfiguration.class)
public class WebBasicSecurityAutoConfiguration /*extends WebSecurityConfigurerAdapter */{

    @Autowired
    private WebBasicSecurityAuthenticationProvider webBasicSecurityAuthenticationProvider;

    @Autowired
    private List<SecurityUrlMatchersRegister> securityUrlMatchersRegisterList;

    @Bean
    public WebAccessAuthenticationConfigRegister webBasicSecurityConfigRegister(){
        return new WebAccessAuthenticationConfigRegister() {
            @Override
            public AuthenticationProvider getAuthenticationProvider() {
                return webBasicSecurityAuthenticationProvider;
            }

            @Override
            public AbstractAuthenticationProcessingFilter getAuthTokenFilter() {
                String[] urlMatchers =
                        SecurityUrlMatchersCollector.collectUrlMatchers(
                                securityUrlMatchersRegisterList, WebBasicSecurityReferences.SECURITY_CONFIG_KEY);

                return new WebBasicAuthenticationFilter(urlMatchers);
            }

            @Override
            public AbstractUsernamePasswordLoginFilter getLoginFilter() {
                return new WebBasicUsernamePasswordLoginFilter("/basicLogin");
            }
        };
    }

    public void finalize( ){
        System.out.println("hello");
    }

    @Bean
    @DependsOn("customUserDetailsService")
    @ConditionalOnMissingBean
    @ConditionalOnBean(CustomUserDetailsService.class)
    @Conditional(BasicSecurityUrlMatchersCondition.class)
    public WebBasicSecurityAuthenticationProvider webBasicSecurityAuthenticationProvider(
            CustomUserDetailsService customUserDetailsService){
        return new WebBasicSecurityAuthenticationProvider(customUserDetailsService);
    }
}
