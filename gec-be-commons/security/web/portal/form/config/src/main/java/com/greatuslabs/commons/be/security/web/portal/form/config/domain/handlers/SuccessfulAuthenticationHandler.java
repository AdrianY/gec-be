package com.greatuslabs.commons.be.security.web.portal.form.config.domain.handlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.greatuslabs.commons.be.security.web.portal.form.internal.api.dto.AuthTokenProcessingResult;
import com.greatuslabs.commons.be.security.web.portal.form.internal.api.infra.AuthenticatedTokenProcessor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class SuccessfulAuthenticationHandler extends SimpleUrlAuthenticationSuccessHandler {

    private final List<AuthenticatedTokenProcessor> authenticatedTokenProcessorList;

    public SuccessfulAuthenticationHandler(List<AuthenticatedTokenProcessor> authenticatedTokenProcessorList) {
        super();
        this.authenticatedTokenProcessorList = authenticatedTokenProcessorList;
    }

    @Override
    public void onAuthenticationSuccess(
            HttpServletRequest request,
            HttpServletResponse response,
            Authentication authentication) throws IOException {

        UsernamePasswordAuthenticationToken authToken = (UsernamePasswordAuthenticationToken) authentication;

        ObjectMapper objectMapper = new ObjectMapper();
        StringBuilder stringBuffer = new StringBuilder();

        //TODO instead of string append store jsonStrings on list before writing on response
        for(AuthenticatedTokenProcessor authenticatedTokenProcessor: authenticatedTokenProcessorList){
            AuthTokenProcessingResult<?> authTokenProcessingResult =
                    authenticatedTokenProcessor.processToken(authToken);

            String jsonString = objectMapper.writeValueAsString(authTokenProcessingResult);
            stringBuffer.append(jsonString);
        }

        response.getWriter().write(stringBuffer.toString());
        response.setStatus(HttpServletResponse.SC_OK);

        clearAuthenticationAttributes(request);
    }

}
