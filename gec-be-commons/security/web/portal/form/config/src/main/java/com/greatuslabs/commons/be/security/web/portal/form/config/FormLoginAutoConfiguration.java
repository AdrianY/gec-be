package com.greatuslabs.commons.be.security.web.portal.form.config;

import com.greatuslabs.commons.be.security.web.core.config.CoreWebSecurityAutoConfiguration;
import com.greatuslabs.commons.be.security.web.core.config.infra.WebPortalConfigRegister;
import com.greatuslabs.commons.be.security.web.portal.form.config.domain.handlers.SuccessfulAuthenticationHandler;
import com.greatuslabs.commons.be.security.web.portal.form.internal.api.infra.AuthenticatedTokenProcessor;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import java.util.List;

@AutoConfigureBefore(CoreWebSecurityAutoConfiguration.class)
@ConditionalOnBean(AuthenticatedTokenProcessor.class)
@Configuration
public class FormLoginAutoConfiguration {

    @Bean
    @ConditionalOnBean(AuthenticatedTokenProcessor.class)
    public WebPortalConfigRegister webPortalConfigRegister(
            List<AuthenticatedTokenProcessor> authenticatedTokenProcessorList){
        return http -> http.formLogin()
                .successHandler(new SuccessfulAuthenticationHandler(authenticatedTokenProcessorList))
                .failureHandler(new SimpleUrlAuthenticationFailureHandler());
    }
}
