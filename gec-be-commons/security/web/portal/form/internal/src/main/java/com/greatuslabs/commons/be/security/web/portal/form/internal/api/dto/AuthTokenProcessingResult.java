package com.greatuslabs.commons.be.security.web.portal.form.internal.api.dto;

public class AuthTokenProcessingResult<DETAILS> {

    public final String valueName;
    public final DETAILS details;

    public AuthTokenProcessingResult(String valueName, DETAILS details) {
        this.valueName = valueName;
        this.details = details;
    }
}
