package com.greatuslabs.commons.be.security.web.portal.form.internal.api.infra;

import com.greatuslabs.commons.be.security.web.portal.form.internal.api.dto.AuthTokenProcessingResult;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

public interface AuthenticatedTokenProcessor {
    AuthTokenProcessingResult<?> processToken(UsernamePasswordAuthenticationToken authentication);
}
