Commons Security
===================

Provide various types of security configurations that can be selected depending on the project requirement.
Currently has support for HMAC Security. Upon use, module are auto configured when detected by a spring boot app
in the classpath.