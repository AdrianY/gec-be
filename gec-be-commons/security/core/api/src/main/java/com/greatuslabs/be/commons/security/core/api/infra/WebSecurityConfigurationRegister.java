package com.greatuslabs.be.commons.security.core.api.infra;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;

public interface WebSecurityConfigurationRegister {
    void configure(HttpSecurity http) throws Exception;
}
