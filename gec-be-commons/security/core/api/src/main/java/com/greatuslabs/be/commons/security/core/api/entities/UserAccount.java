package com.greatuslabs.be.commons.security.core.api.entities;

public interface UserAccount {
    String getUsername();

    String getPassword();
}
