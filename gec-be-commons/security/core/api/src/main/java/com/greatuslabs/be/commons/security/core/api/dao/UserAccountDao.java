package com.greatuslabs.be.commons.security.core.api.dao;


import com.greatuslabs.be.commons.security.core.api.entities.UserAccount;

public interface UserAccountDao<U extends UserAccount> {
    U findByUsername(String username);
}
