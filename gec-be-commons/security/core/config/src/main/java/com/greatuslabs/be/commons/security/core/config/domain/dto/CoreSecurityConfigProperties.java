package com.greatuslabs.be.commons.security.core.config.domain.dto;

import java.util.Objects;

public class CoreSecurityConfigProperties {

    private Boolean enabled;

    public Boolean getEnabled() {
        if(Objects.isNull(enabled))
            return true;
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
