package com.greatuslabs.be.commons.security.core.config.domain.services.impl;

import com.greatuslabs.be.commons.security.core.api.dao.UserAccountDao;
import com.greatuslabs.be.commons.security.core.api.entities.UserAccount;
import com.greatuslabs.be.commons.security.core.config.domain.services.CustomUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.Objects;


public class CustomUserDetailsServiceImpl implements CustomUserDetailsService {

    private final UserAccountDao<?> userAccountDao;

    public CustomUserDetailsServiceImpl(UserAccountDao<?> userAccountDao) {
        this.userAccountDao = userAccountDao;
    }

    @Override
    public UserAccount retrieveByUsername(String username) {
        return userAccountDao.findByUsername(username);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final UserAccount userAccount = userAccountDao.findByUsername(username);
        if (Objects.isNull(userAccount))
            throw new UsernameNotFoundException("No user found with username: " + username);

        return new org.springframework.security.core.userdetails.User(
                userAccount.getUsername(), userAccount.getPassword(),
                true,
                true,
                true,
                true,
                new ArrayList<>()
        );
    }
}
