package com.greatuslabs.be.commons.security.core.config.domain.services;

import com.greatuslabs.be.commons.security.core.api.entities.UserAccount;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface CustomUserDetailsService extends UserDetailsService {
    UserAccount retrieveByUsername(String username);
}
