package com.greatuslabs.be.commons.security.core.config.domain.references;

public interface CoreSecurityConfigReferences {
    String CORE_SECURITY_CONFIG_PREFIX = "gl.security";
}
