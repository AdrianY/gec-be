package com.greatuslabs.be.commons.security.core.config;

import com.greatuslabs.be.commons.persistence.core.api.annotations.TransactionalService;
import com.greatuslabs.be.commons.security.core.api.dao.UserAccountDao;
import com.greatuslabs.be.commons.security.core.api.entities.UserAccount;
import com.greatuslabs.be.commons.security.core.api.infra.WebSecurityConfigurationRegister;
import com.greatuslabs.be.commons.security.core.config.domain.dto.CoreSecurityConfigProperties;
import com.greatuslabs.be.commons.security.core.config.domain.references.CoreSecurityConfigReferences;
import com.greatuslabs.be.commons.security.core.config.domain.services.CustomUserDetailsService;
import com.greatuslabs.be.commons.security.core.config.domain.services.impl.CustomUserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Configuration
@ConditionalOnProperty(
        prefix = CoreSecurityConfigReferences.CORE_SECURITY_CONFIG_PREFIX,
        value = "enabled",
        matchIfMissing = true
)
//@AutoConfigureOrder(Ordered.LOWEST_PRECEDENCE-3)
//@ConditionalOnBean(WebSecurityConfigurationRegister.class)
//@AutoConfigureOrder(Ordered.LOWEST_PRECEDENCE-1)
//@EnableWebSecurity
public class CoreSecurityAutoConfiguration /*extends WebSecurityConfigurerAdapter */{

//    @Autowired
//    private List<WebSecurityConfigurationRegister> webSecurityConfigurationRegisterList;
//
//    @Override
//    public void configure(HttpSecurity http) throws Exception {
//        http.csrf().disable();
//
//        for(WebSecurityConfigurationRegister configurationRegister: webSecurityConfigurationRegisterList){
//            configurationRegister.configure(http);
//        }
//
//        // setup security
//        http.authorizeRequests().anyRequest().authenticated();
//    }

    @Bean
    @Validated
    @ConfigurationProperties(CoreSecurityConfigReferences.CORE_SECURITY_CONFIG_PREFIX)
    public CoreSecurityConfigProperties coreSecurityConfigProperties(){
        return new CoreSecurityConfigProperties();
    }

    @Bean
    @TransactionalService
    public CustomUserDetailsService customUserDetailsService(UserAccountDao<? extends UserAccount> userAccountDao){
        return new CustomUserDetailsServiceImpl(userAccountDao);
    }
}
