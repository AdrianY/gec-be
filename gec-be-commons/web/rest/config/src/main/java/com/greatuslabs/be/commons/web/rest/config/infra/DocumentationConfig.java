package com.greatuslabs.be.commons.web.rest.config.infra;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.greatuslabs.be.commons.web.rest.config.domain.properties.ApiDocumentationProperties;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.ApiSelectorBuilder;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.greatuslabs.be.commons.web.rest.config.domain.references.WebRestAutoConfigReferences.API_DOCUMENTATION_PROPERTIES_PREFIX;

@Configuration
@EnableSwagger2
public class DocumentationConfig {

    @Bean
    @ConfigurationProperties(API_DOCUMENTATION_PROPERTIES_PREFIX)
    public ApiDocumentationProperties apiDocumentationProperties(){
        return new ApiDocumentationProperties();
    }

    @SuppressWarnings("Guava")
    @Bean
    public Docket api(ApiDocumentationProperties apiDocumentationProperties) {

        List<String> includedPaths = apiDocumentationProperties.getIncludedPaths();
        List<String> excludedPaths = apiDocumentationProperties.getExcludedPaths();
        ApiDocumentationProperties.ApiDocDetails apiDocDetails = apiDocumentationProperties.getApiDocDetails();
        boolean excludeActuatorPaths = apiDocumentationProperties.isExcludeActuatorPaths();

        boolean includedPathsSpecified = CollectionUtils.isNotEmpty(includedPaths);
        boolean excludedPathsSpecified = CollectionUtils.isNotEmpty(excludedPaths);

        ApiInfo apiInfo = deriveApiInfo(apiDocDetails);

        if(includedPathsSpecified && excludedPathsSpecified)
            throw new IllegalArgumentException("Excluded paths should be empty when included paths are specified");

        if(includedPathsSpecified){
            List<Predicate<String>> antPaths =
                    includedPaths.stream().map(PathSelectors::ant).collect(Collectors.toList());

            return createDocket(apiInfo, antPaths, excludeActuatorPaths);
        }
        else if(excludedPathsSpecified){
            List<Predicate<String>> regexPaths =
                    excludedPaths.stream().map(p -> Predicates.not(PathSelectors.regex(p)))
                            .collect(Collectors.toList());

            return createDocket(apiInfo, regexPaths, excludeActuatorPaths);
        }

        return createDocket(apiInfo, excludeActuatorPaths);
    }

    private static Docket createDocket(ApiInfo apiInfo, boolean excludeActuatorPaths) {
        ApiSelectorBuilder builder = new Docket(DocumentationType.SPRING_WEB)
                .select();

        if(excludeActuatorPaths)
            excludeActuatorEndpoints(builder);
        else
            builder.apis(RequestHandlerSelectors.any());

        return builder
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo);
    }

    @SuppressWarnings("Guava")
    private static Docket createDocket(
            ApiInfo apiInfo,
            List<Predicate<String>> pathFilters,
            boolean excludeActuatorPaths
    ) {
        ApiSelectorBuilder builder = new Docket(DocumentationType.SPRING_WEB)
                .select();

        if(excludeActuatorPaths)
            excludeActuatorEndpoints(builder);
        else
            builder.apis(RequestHandlerSelectors.any());

        for (Predicate<String> pathFilter: pathFilters)
            builder.paths(pathFilter);

        return builder.build().apiInfo(apiInfo);
    }

    private static ApiInfo deriveApiInfo(ApiDocumentationProperties.ApiDocDetails apiDocDetails) {

        ApiDocumentationProperties.ContactDetails contactDetails = apiDocDetails.getContactDetails();

        return new ApiInfo(
                apiDocDetails.getTitle(),
                apiDocDetails.getDescription(),
                apiDocDetails.getVersion(),
                apiDocDetails.getTermsOfService(),
                new Contact(
                        contactDetails.getName(),
                        contactDetails.getUrl(),
                        contactDetails.getEmail()
                ),
                apiDocDetails.getLicense(),
                apiDocDetails.getLicenseUrl(),
                Collections.emptyList()
        );

    }

    @SuppressWarnings("Guava")
    private static Predicate<RequestHandler> wrapToBeExcludedPackages(String packageName){
        return Predicates.not(RequestHandlerSelectors.basePackage(packageName));
    }

    private static void excludeActuatorEndpoints(ApiSelectorBuilder builder){
        builder
                .apis(wrapToBeExcludedPackages("org.springframework.boot"))
                .apis(wrapToBeExcludedPackages("eu.hinsch.spring.boot.actuator"));
    }

}
