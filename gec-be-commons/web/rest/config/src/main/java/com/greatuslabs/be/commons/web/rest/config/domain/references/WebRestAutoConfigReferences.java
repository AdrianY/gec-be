package com.greatuslabs.be.commons.web.rest.config.domain.references;

public interface WebRestAutoConfigReferences {
    String API_DOCUMENTATION_PROPERTIES_PREFIX = "gl.web.rest.documentation";
}
