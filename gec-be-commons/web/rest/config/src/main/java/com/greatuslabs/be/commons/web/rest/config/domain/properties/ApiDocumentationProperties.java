package com.greatuslabs.be.commons.web.rest.config.domain.properties;

import java.util.ArrayList;
import java.util.List;

public class ApiDocumentationProperties {

    private final List<String> includedPaths = new ArrayList<>();
    private final List<String> excludedPaths = new ArrayList<>();
    private final ApiDocDetails apiDocDetails = new ApiDocDetails();
    private boolean excludeActuatorPaths = true;

    public List<String> getIncludedPaths() {
        return includedPaths;
    }

    public List<String> getExcludedPaths() {
        return excludedPaths;
    }

    public ApiDocDetails getApiDocDetails() {
        return apiDocDetails;
    }

    public boolean isExcludeActuatorPaths() {
        return excludeActuatorPaths;
    }

    public void setExcludeActuatorPaths(boolean excludeActuatorPaths) {
        this.excludeActuatorPaths = excludeActuatorPaths;
    }

    public static class ApiDocDetails{

        private final ContactDetails contactDetails = new ContactDetails();
        private String title = "Greatus Labs REST API";
        private String description = "Documentation of Greatus Labs API.";
        private String version = "1.0.0-a1";
        private String termsOfService = "Terms of Service";
        private String license = "License of API";
        private String licenseUrl = "API license URL";

        public ContactDetails getContactDetails() {
            return contactDetails;
        }

        public String getTitle() {
            return title;
        }

        public String getDescription() {
            return description;
        }

        public String getVersion() {
            return version;
        }

        public String getTermsOfService() {
            return termsOfService;
        }

        public String getLicense() {
            return license;
        }

        public String getLicenseUrl() {
            return licenseUrl;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public void setTermsOfService(String termsOfService) {
            this.termsOfService = termsOfService;
        }

        public void setLicense(String license) {
            this.license = license;
        }

        public void setLicenseUrl(String licenseUrl) {
            this.licenseUrl = licenseUrl;
        }
    }

    public static class ContactDetails{

        private String name = "Greatus Labs";
        private String url = "http://www.greatuslabs.com/";
        private String email = "adriancyago@gmail.com";

        public String getName() {
            return name;
        }

        public String getUrl() {
            return url;
        }

        public String getEmail() {
            return email;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }
}
