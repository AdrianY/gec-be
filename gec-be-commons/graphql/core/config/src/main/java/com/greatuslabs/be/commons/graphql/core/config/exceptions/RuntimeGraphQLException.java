package com.greatuslabs.be.commons.graphql.core.config.exceptions;

import graphql.ErrorType;
import graphql.GraphQLError;
import graphql.language.SourceLocation;

import java.util.List;

public class RuntimeGraphQLException extends RuntimeException implements GraphQLError {

    private final ErrorType errorType;

    public RuntimeGraphQLException(RuntimeException ex){
        super(ex);
        this.errorType = ErrorType.DataFetchingException;
    }

    public RuntimeGraphQLException(ErrorType errorType, String message){
        super(message);
        this.errorType = errorType;
    }

    @Override
    public List<SourceLocation> getLocations() {
        return null;
    }

    @Override
    public ErrorType getErrorType() {
        return errorType;
    }
}
