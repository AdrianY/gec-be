package com.greatuslabs.be.commons.graphql.core.config.components;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.stereotype.Component;

@Component
public class DefaultQuery implements GraphQLQueryResolver {

    public String getGreetings(String name){
        return "Hello "+name;
    }
}
