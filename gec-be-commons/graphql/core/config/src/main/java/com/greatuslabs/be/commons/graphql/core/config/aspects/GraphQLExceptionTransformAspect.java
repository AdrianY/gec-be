package com.greatuslabs.be.commons.graphql.core.config.aspects;

import com.greatuslabs.be.commons.graphql.core.config.exceptions.RuntimeGraphQLException;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class GraphQLExceptionTransformAspect {

    @AfterThrowing(pointcut = "execution(* com.greatuslabs..*.modules..*.service..*.*(..))", throwing = "exception")
    public void runtimeExceptionTransformAdvice(RuntimeException exception){
        throw new RuntimeGraphQLException(exception);
    }
}
